/* 
 *  BlueRP.sql
 *	Database configuration
 */

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for gmod_admins
-- ----------------------------
DROP TABLE IF EXISTS `bluerp_players`;
CREATE TABLE `bluerp_players` (
	`SteamID` VARCHAR(32) NOT NULL,
	`Cash` INT(9) NOT NULL DEFAULT 0, 
	`Bank` INT(9) NOT NULL DEFAULT 3000, 
	`Income` INT(9) NOT NULL DEFAULT 7, 
	`Felony` INT(9) NOT NULL DEFAULT 0, 
	`Cuffed` INT(2) NOT NULL DEFAULT 0, 
	`JailTime` INT(9) NOT NULL DEFAULT 0, 
	`JailProgress` INT(9) NOT NULL DEFAULT 0, 
	`Minutes` INT(9) NOT NULL DEFAULT 0, 
	`Employment` TEXT(32) NOT NULL DEFAULT 'Unemployed'
	PRIMARY KEY (`SteamID`)
) 	ENGINE=InnoDB DEFAULT CHARSET=latin1;
