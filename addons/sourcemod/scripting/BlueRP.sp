///////////////////////////////////////////////////////////////////
////////////////////		  BlueRP			///////////////////
///////////////////			By: EasSidezZ		///////////////////
///////////////////////////////////////////////////////////////////
/*
	My original intent of creating this roleplay mod was to be a simple and compact roleplay mod for Half Life 2: Deathmatch.  
	But rather than that, I wanted to make something that the average server owner could open up and edit to make some great changes o their server mod.
	Hopefully, nobody takes this for themselves and steals it claiming it as their own. Good luck and have fun.
	-EasSidezZ
*/

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <morecolors>
#include <bluerp>

#pragma semicolon 1
#pragma compress 0

//Global Definitions:
//These are essentially things that are preset for a few reasons.
//One of which is so you don't have to search all over to edit one value, or to make it really easy overall.
#define PLUGINVERSION		"0.2.33"
#define CMDTAG 				"{cornflowerblue}[BlueRP]{default}" 
#define CONSOLETAG			"[BlueRP]"
#define NOBODY 				"Nobody"
#define NOTBUYABLE			0
#define BUYABLE				1
#define SELLABLE			2
#define WAGETIMER			45
#define HUDTIMER 			1.0
#define MAXITEMS			300
#define MAXEDICTS			2048
#define MAXENTS				4028
#define MAXDOORS			1200
#define MAXNPCS				100
#define MAXJAILS			10
#define MAXJOBS				250


//Global Scope Variables (These can be edited or called to from ANYWHERE in the plugin.)
//Explanation of Variable Conventions:
/*
	g_ = Global. If it's in here, it's a global variable and you are to prefix it as so.
	i = Integer or Number. If it's not a bool, float, string or anything else, put an i infront of it. Ex: iTime, g_iMoney, etc.
	s = String. If it's text based, this is a string.
	sz = String. If it's going to end up terminated to a NULL, put this. I don't think anyone is QUITE this assenine to care about sz or s, so I just put s.
	f = Float. If it's a decimal, you'll put this.
	b = Boolean. If it's true or false, you'll put this.
	a = Trie Arrays. You'll probably never use this, I haven't ever had to.
	h = Handles. These are dependant, and just an h will be used for any generic handles you'll use.
	hCV = ConVar Handles. You'll use these to mark something as a console variable handle.
	_h = KeyValues Handles. I personally find this optional, but to keep it classy i'll be using it inside the mod.
	other = Dependant on the first letter of the enum. I don't think anyone editing this would be majorly editing it for enumerations, so this you'll probably never use.
*/

//Explanation of Format-Class Functions:
/*
	%i = Integer. You use this to post the contents of a number.
	%d = Integer. You use this to post the contents of a number.
	%s = String. You use this in a print function to paste the contents of a String.
	%f = Float. You use this to post a floating-point (decimal) number.
	%N = Client Index. You use this with a Client Index to print their name. (Thusforth making GetClientName obsolete)
*/

//Integers
new g_iCash[MAXPLAYERS + 1]; //MAXPLAYERS is the current mod's maximum number of players + 1 for extra space. Other things like PLATFORM_MAX_PATH are hardcoded into SM as a number. (Definitons)
new g_iBank[MAXPLAYERS + 1]; //Essentially, the variable has 33 slots, much like the players in the server. Slot 1 of g_iCash would be the first player's money.
new g_iIncome[MAXPLAYERS + 1];
new g_iIncomeTimer[MAXPLAYERS + 1];
new g_iJailTime[MAXPLAYERS + 1];
new g_iJailProgress[MAXPLAYERS + 1];
new g_iMinutes[MAXPLAYERS + 1];
new g_iFelonyLevel[MAXPLAYERS + 1];
new g_iPrestige[MAXPLAYERS + 1];
new g_iNotoriety[MAXPLAYERS + 1];
new g_iCuffedBy[MAXPLAYERS + 1];
new g_iBounty[MAXPLAYERS + 1];
new g_iMenuTarget[MAXPLAYERS + 1];
new g_iUsingVendor[MAXPLAYERS + 1];
new g_iTrails[MAXPLAYERS + 1];
new g_iNPCCash[MAXPLAYERS + 1];
new g_iItem[MAXPLAYERS + 1][MAXITEMS + 1]; //The 2nd array is the ItemId and the value of this is the amount a client has. Ex: g_iItem[Client][102] = 5; Means a client has 5 of Item 102.
new g_iItemAmount[MAXENTS][MAXITEMS];
new g_iItemCost[MAXITEMS];
new g_iItemAction[MAXITEMS];
new g_iLocks[MAXDOORS];
new g_iPoliceDoor[65];
new g_iValue[MAXEDICTS];  
new g_iNPCList[MAXNPCS];
new g_iNPCListInverse[MAXENTS];
new g_iNPCLiveUpdate[15][MAXEDICTS];
new g_iAccessDoor[MAXPLAYERS + 1][MAXEDICTS];
new g_iUptime; //There's no MAXPLAYERS or Array size because it's not for a Client, and we're only storing one number in it (NOT ONE DIGIT, ONE NUMBER!)
new UserMsg:g_iFadeID;
new g_iGPSTo[MAXPLAYERS + 1][MAXPLAYERS + 1];
new g_iLaser;
new g_iHalo;
new g_iValidJails = 0;


//Strings
new String:g_sTempJob[MAXPLAYERS + 1][32];
new String:g_sPermJob[MAXPLAYERS + 1][32];
new String:g_sSteamID[MAXPLAYERS + 1][32];
new String:g_sItemName[MAXITEMS][32];
new String:g_sItemVar[MAXITEMS][255];
new String:g_sNotice[MAXDOORS][255];

//Booleans
new bool:g_bAdmin[MAXPLAYERS + 1];
new bool:g_bCuffed[MAXPLAYERS + 1];
new bool:g_bGivingItems[MAXPLAYERS + 1];
new bool:g_bOnDrugs[MAXPLAYERS + 1];
new bool:g_bPreThink[MAXPLAYERS + 1] = false;
new bool:g_bKnows[MAXPLAYERS + 1] = false;
new bool:g_bDoorLocked[MAXDOORS];
new bool:g_bHasJail;
new bool:g_bHasExit;

//Floats
new Float:g_fLastPressedE[MAXPLAYERS + 1];
new Float:g_fLastPressedShift[MAXPLAYERS + 1];
new Float:g_fMenuTime[MAXPLAYERS + 1];
new Float:g_fSpeed[MAXPLAYERS + 1];
new Float:g_fPoliceScannerTime[MAXPLAYERS + 1];
new Float:g_fPoliceJammerTime[MAXPLAYERS + 1];
new Float:g_fJailLoc[MAXJAILS + 1][3];
new Float:g_fExitLoc[3];
new Float:NULLZONE[3] = {-1.0, -1.0, -1.0};
new Float:g_fNPCRobOrigin[MAXPLAYERS + 1][3];
new Float:g_fRobCooldown[3][MAXNPCS];


//Handles
new Handle:g_hDB = INVALID_HANDLE;
new Handle:g_hCV_DatabaseType = INVALID_HANDLE;
new Handle:g_hCV_RebelTeam = INVALID_HANDLE;
new Handle:g_hCV_CombineTeam = INVALID_HANDLE;
new Handle:g_hCV_Cuffable = INVALID_HANDLE;
new Handle:g_hCV_CrimeForBounty = INVALID_HANDLE;
new Handle:g_hCV_CrimeMenuAmount = INVALID_HANDLE;
new Handle:g_hCV_RobMode = INVALID_HANDLE;

//KeyValues Databases (As I stated earlier, this mod is for general admin accessability, and certain things will be kept simple.)
new String:g_sWeaponIDs[255]; //Array for the weapons
new String:g_sWeapons[30][255];
new String:g_sNPCPath[PLATFORM_MAX_PATH]; //Buildpath location
new String:g_sItemPath[PLATFORM_MAX_PATH];
new String:g_sConfigPath[PLATFORM_MAX_PATH];
new String:g_sJobSetupPath[PLATFORM_MAX_PATH];
new String:g_sJobPath[PLATFORM_MAX_PATH];
new String:g_sDoorPath[PLATFORM_MAX_PATH];
new String:g_sNoticePath[PLATFORM_MAX_PATH];

//Plugin Information:
public Plugin:myinfo =
{
	name = "Blue Roleplay",
	author = "EasSidezZ",
	description = "A generic and barebones Roleplay Plugin for users to run or customize with ease",
	version = PLUGINVERSION, //Major Revision.Minor Revision.Patch
	url = "sourcemod.net"
}

//Information: Called upon BlueRP's initial loading:
public OnPluginStart()
{
	g_iUptime = 0;
	
	LoadTranslations("common.phrases");

	//Admin Commands:
	RegAdminCmd("sm_addmoney", Command_addMoney, ADMFLAG_CUSTOM6, "[BlueRP] Add money to a specific player's Wallet");
	RegAdminCmd("sm_setmoney", Command_setMoney, ADMFLAG_CUSTOM6, "[BlueRP] Set the Money of a specific player's Wallet");
	RegAdminCmd("sm_cuff", Command_cuffPlayer, ADMFLAG_CUSTOM2, "[BlueRP] Cuff a player");
	RegAdminCmd("sm_uncuff", Command_uncuffPlayer, ADMFLAG_CUSTOM2, "[BlueRP] Uncuff a player");
	RegAdminCmd("sm_felony", Command_setFelony, ADMFLAG_CUSTOM3, "[BlueRP] Set the felony level of a player");
	RegAdminCmd("sm_setfelony", Command_setFelony, ADMFLAG_CUSTOM3, "[BlueRP] Set the felony level of a player");
	RegAdminCmd("sm_giveitem", Command_giveItem, ADMFLAG_CUSTOM6, "[BlueRP] Give an item to a player");
	RegAdminCmd("sm_takeitem", Command_takeItem, ADMFLAG_CUSTOM6, "[BlueRP] Give an item to a player");
	RegAdminCmd("sm_employ", Command_employPlayer, ADMFLAG_CUSTOM6, "[BlueRP] Employ a player to a Job");

	RegAdminCmd("sm_createnpc", Command_createNPC, ADMFLAG_CUSTOM4, "[BlueRP] Create a Map NPC");
	RegAdminCmd("sm_removenpc", Command_removeNPC, ADMFLAG_CUSTOM4, "[BlueRP] Delete a Map NPC");
	RegAdminCmd("sm_npcwho", Command_npcWho, ADMFLAG_CUSTOM2, "[BlueRP] Get a Map NPC's ID");
	RegAdminCmd("sm_npclist", Command_npcList, ADMFLAG_CUSTOM1, "[BlueRP] Print a list of Map NPCs");
	RegAdminCmd("sm_addvendoritem", Command_addVendorItems, ADMFLAG_CUSTOM3, "[BlueRP] Add an item to a vendor");

	RegAdminCmd("sm_setdoorprice", Command_setDoorPrice, ADMFLAG_CUSTOM4, "[BlueRP] Set a door's price");
	RegAdminCmd("sm_setdooramount", Command_setDoorPrice, ADMFLAG_CUSTOM4, "[BlueRP] Set a door's price");
	RegAdminCmd("sm_resetdoor", Command_resetDoor, ADMFLAG_CUSTOM4, "[BlueRP] Reset a door");

	RegAdminCmd("sm_createitem", Command_createItem, ADMFLAG_CUSTOM4, "[BlueRP] Create a new Item");
	RegAdminCmd("sm_removeitem", Command_deleteItem, ADMFLAG_CUSTOM4, "[BlueRP] Remove an Item");
	RegAdminCmd("sm_itemlist", Command_itemList, ADMFLAG_CUSTOM1, "[BlueRP] Show the Item List");

	RegAdminCmd("sm_createjob", Command_createJob, ADMFLAG_CUSTOM6, "[BlueRP] Create a Job");
	RegAdminCmd("sm_deletejob", Command_deleteJob, ADMFLAG_CUSTOM6, "[BlueRP] Delete a Job");
	RegAdminCmd("sm_listjobs", Command_listJobs, ADMFLAG_CUSTOM3, "[BlueRP] Displays all Jobs");

	RegAdminCmd("sm_setjail", Command_setJail, ADMFLAG_CUSTOM3, "[BlueRP] Create a Jail Location");
	RegAdminCmd("sm_exitzone", Command_setExit, ADMFLAG_CUSTOM3, "[BlueRP] Set the Exit Location");

	RegAdminCmd("sm_location", Command_getClientLocation, ADMFLAG_CUSTOM1, "[BlueRP] Returns your Coordinates");
	RegAdminCmd("sm_info", Command_edictInfo, ADMFLAG_CUSTOM1, "[BlueRP] Returns everything you can know of an Edict");   
	RegAdminCmd("sm_duplicate", Command_dupeEdict, ADMFLAG_CUSTOM1, "[BlueRP] Duplicate an Edict");
	RegAdminCmd("sm_create", Command_createEdict, ADMFLAG_CUSTOM1, "[BlueRP] Creates an Edict"); 
	RegAdminCmd("sm_throw", Command_throwEdict, ADMFLAG_CUSTOM1, "[BlueRP] Create and Throw an Edict"); 
	RegAdminCmd("sm_remove", Command_removeEdict, ADMFLAG_CUSTOM1, "[BlueRP] Removes an Edict");

	RegConsoleCmd("sm_items", Command_items, "[BlueRP] Show your inventory");
	RegConsoleCmd("sm_buydoor", Command_buyDoor, "[BlueRP] Purchase a door that is for sale");
	RegConsoleCmd("sm_selldoor", Command_sellDoor, "[BlueRP] Sell a door that you own");
	RegConsoleCmd("sm_switch", Command_switchJob, "[BlueRP] Switch to or from a Job");
	RegConsoleCmd("sm_door", Command_doorInfo, "[BlueRP] Display door information");
	RegConsoleCmd("sm_doorname", Command_setDoorMessage, "[BlueRP] Set the message of the door");
	RegConsoleCmd("sm_givekeys", Command_giveKeys, "[BlueRP] Give door access to a player");
	RegConsoleCmd("sm_deletekeys", Command_deleteKeys, "[BlueRP] Revoke door access from a player");

	AddCommandListener(Listener_MenuSelect, "sm_vmenuselect");

	//Events:
	HookEvent("player_spawn", Event_playerSpawn, EventHookMode_Post);
	HookEvent("player_death", Event_playerDeath);

	//Console Variables:
	AutoExecConfig(true, "BlueRP");

	CreateConVar("bluerp_version", PLUGINVERSION, "[BlueRP] The version of the plugin [THIS IS NOT FOR ADMINS TO EDIT]", FCVAR_NOTIFY | FCVAR_PLUGIN);
	g_hCV_DatabaseType = CreateConVar("rp_database_type", "0", "[BlueRP] The SQL Database type you'd like to use. (0 = SQLite, 1 = MySQL)", FCVAR_PLUGIN, true, 0.0, true, 1.0);
	g_hCV_RebelTeam = CreateConVar("rp_rebelteamname", "Citizens", "[BlueRP] The default team name for the Rebel Team", FCVAR_NOTIFY | FCVAR_PLUGIN);
	g_hCV_CombineTeam = CreateConVar("rp_combineteamname", "Civil Protection", "[BlueRP] The default team name for the Combine Team", FCVAR_NOTIFY | FCVAR_PLUGIN);
	g_hCV_Cuffable = CreateConVar("rp_cufflimit", "1800", "[BlueRP] The minimum Felony a player must have to be cuffed by cops", FCVAR_NOTIFY | FCVAR_PLUGIN);
	g_hCV_CrimeForBounty = CreateConVar("rp_crimeforbounty", "2500", "[BlueRP] The minimum Felony a player must have to get Bountied", FCVAR_NOTIFY | FCVAR_PLUGIN);
	g_hCV_CrimeMenuAmount = CreateConVar("rp_crimemenuamount", "300", "[BlueRP] The minimum Felony a player must have to be on the Crime Menu", FCVAR_NOTIFY | FCVAR_PLUGIN);
	g_hCV_RobMode = CreateConVar("rp_robmode", "1", "[BlueRP] Require cops to be online to rob", FCVAR_NOTIFY | FCVAR_PLUGIN);

	g_iFadeID = GetUserMessageId("Fade");
	
	LoadSQL();
}

//Information: Called upon the map loading
public OnMapStart()
{
	PrecacheSound("HL1/fvox/beep.wav", true);
	PrecacheSound("HL1/fvox/fuzz.wav", true);
	PrecacheModel("models/Items/BoxMRounds.mdl", true);
	g_iLaser = PrecacheModel("materials/sprites/laserbeam.vmt", true);
	g_iHalo = PrecacheModel("materials/sprites/halo01.vmt", true);
	PrecacheModel("models/money/broncoin.mdl", true);
	PrecacheModel("models/money/silvcoin.mdl", true);
	PrecacheModel("models/money/goldcoin.mdl", true);
	PrecacheModel("models/money/note3.mdl", true);
	PrecacheModel("models/money/note2.mdl", true);
	PrecacheModel("models/money/note.mdl", true);
	PrecacheModel("models/money/goldbar.mdl", true);

	if(GetConVarInt(FindConVar("mp_teamplay")) == 1)
	{
		decl String:sTeam[2][32];
		for(new X = 0; X < MAXENTS; X++) //I'm not really sure if this is an edict or entity, so I'll just use Entity...
		{
			if(IsValidEntity(X))
			{
				decl String:sClassName[16];
				GetEntityClassname(X, sClassName, sizeof(sClassName));
				if(StrEqual(sClassName, "team_manager", true))
				{
					new iTeam = GetEntProp(X, Prop_Send, "m_iTeamNum");			
					switch(iTeam)
					{
						//Combine Team:
						case 2:	
						{
							GetConVarString(g_hCV_CombineTeam, sTeam[0], sizeof(sTeam[]));
							SetEntPropString(X, Prop_Send, "m_szTeamname", sTeam[0]);
							ChangeEdictState(X, GetEntSendPropOffs(X, "m_szTeamname", true));
						}
						
						//Rebel Team:
						case 3:
						{
							GetConVarString(g_hCV_RebelTeam, sTeam[1], sizeof(sTeam[]));
							SetEntPropString(X, Prop_Send, "m_szTeamname", sTeam[1]);
							ChangeEdictState(X, GetEntSendPropOffs(X, "m_szTeamname", true));
						}
					}
				}
			}
		}
	}
	else SetConVarInt(FindConVar("mp_teamplay"), 1);

	for(new A = 0; A < 3; A++) 
	{
		for(new B = 0; B < 100; B++)
		{
			g_fRobCooldown[A][B] = GetGameTime();
		}
	}
	for(new i = 0; i < MAXEDICTS; i++)
	{
		g_iValue[i] = 0;
	}
	
	for(new X = 0; X < MAXDOORS; X++)
	{
		g_sNotice[X] = "NaN";
	}

	decl String:sNPC[64], String:sMap[32], String:sConfig[64], String:sDoor[64], String:sNotice[64];
	GetCurrentMap(sMap, sizeof(sMap));
	Format(sNPC, sizeof(sNPC), "data/BlueRP/%s/npc.txt", sMap);
	Format(sConfig, sizeof(sConfig), "data/BlueRP/%s/config.txt", sMap);
	Format(sDoor, sizeof(sDoor), "data/BlueRP/%s/doors.txt", sMap);
	Format(sNotice, sizeof(sNotice), "data/BlueRP/%s/notice.txt", sMap);
	CreateTimer(1.0, SpawnNPCS);

	BuildPath(Path_SM, g_sItemPath, PLATFORM_MAX_PATH, "data/BlueRP/items.txt");
	if(FileExists(g_sItemPath) == false) SetFailState("[BlueRP] FATAL ERROR: Missing file '%s'", g_sItemPath);

	BuildPath(Path_SM, g_sJobSetupPath, PLATFORM_MAX_PATH, "data/BlueRP/job_equipment.txt");
	if(FileExists(g_sJobSetupPath) == false) SetFailState("[BlueRP] FATAL ERROR: Missing file '%s'", g_sJobSetupPath);

	BuildPath(Path_SM, g_sJobPath, PLATFORM_MAX_PATH, "data/BlueRP/job.txt");
	if(FileExists(g_sJobPath) == false) SetFailState("[BlueRP] FATAL ERROR: Missing file '%s'", g_sJobPath);
	
	BuildPath(Path_SM, g_sWeaponIDs, PLATFORM_MAX_PATH, "data/BlueRP/WeaponIDs.txt");	
	if(FileExists(g_sWeaponIDs) == false) SetFailState("[BlueRP] FATAL ERROR: Missing file '%s'", g_sWeaponIDs);

	BuildPath(Path_SM, g_sConfigPath, PLATFORM_MAX_PATH, sConfig);
	if(FileExists(g_sConfigPath) == false) SetFailState("[BlueRP] FATAL ERROR: Missing file '%s'", sConfig);

	BuildPath(Path_SM, g_sNPCPath, PLATFORM_MAX_PATH, sNPC);
	if(FileExists(g_sNPCPath) == false) SetFailState("[BlueRP] FATAL ERROR: Missing file '%s'", sNPC);

	BuildPath(Path_SM, g_sDoorPath, PLATFORM_MAX_PATH, sDoor);
	if(FileExists(g_sDoorPath) == false) SetFailState("[BlueRP] FATAL ERROR: Missing file '%s'", sDoor);

	BuildPath(Path_SM, g_sNoticePath, PLATFORM_MAX_PATH, sNotice);
	if(FileExists(g_sNoticePath) == false) SetFailState("[BlueRP] FATAL ERROR: Missing file '%s'", sNotice);



	new Handle:hKVWeapons = CreateKeyValues("Vault");
	FileToKeyValues(hKVWeapons, g_sWeaponIDs);
	decl String:sWeaponID[255], String:sID[255];
	for(new X = 1; X < 26; X++)
	{
		IntToString(X, sID, sizeof(sID));
		LoadString(hKVWeapons, "WeaponsID", sID, "NaN", sWeaponID);
		if(!StrEqual(sWeaponID, "NaN"))
		{
			g_sWeapons[X] = sWeaponID;
		}
	}
	CloseHandle(hKVWeapons);


	new Handle:hKVDoors = CreateKeyValues("Vault");
	FileToKeyValues(hKVDoors, g_sConfigPath);

	decl String:sDoor1[32];
	for(new X = 0; X < 64; X++)
	{
		IntToString(X, sDoor1, sizeof(sDoor1));
		g_iPoliceDoor[X] = LoadInteger(hKVDoors, "PoliceDoor", sDoor, 0);
	}
	KvRewind(hKVDoors);
	CloseHandle(hKVDoors);

	reloadMapConfig();	
	reloadItems();
}

//Information: Called once a client is CONNECTED and AUTHORIZED
public OnClientPostAdminCheck(Client)
{	
	//Setup Default Values, Prevent a Client from having someone else's stats.
	g_iCash[Client] = 0;
	g_iBank[Client] = 0;
	g_iIncome[Client] = 0;
	g_sSteamID[Client] = "";
	g_sTempJob[Client] = "Unemployed";
	g_sPermJob[Client] = "Unemployed";
	g_bCuffed[Client] = false;
	g_bAdmin[Client] = false;
	g_iBounty[Client] = 0;
	g_iCuffedBy[Client] = -1;
	g_iTrails[Client] = 0;
	g_iIncomeTimer[Client] = WAGETIMER;
	g_fLastPressedShift[Client] = GetGameTime();
	g_fLastPressedE[Client] = GetGameTime();
	g_fMenuTime[Client] = GetGameTime();

	//Owns no doors
	for(new X = 0; X < MAXDOORS; X++)
	{
		g_iAccessDoor[Client][X] = 0;
	}
	for(new X = 1; X < MAXDOORS; X++)
	{
		g_iAccessDoor[Client][X] = 0;
	}

	g_bAdmin[Client] = CheckCommandAccess(Client, "sm_admin", ADMFLAG_GENERIC);
	GetClientAuthString(Client, g_sSteamID[Client], sizeof(g_sSteamID[])); //One general value grabbed upon auth is alot better than getting it every time
	
	//Hook Certain Events:
	SDKHook(Client, SDKHook_OnTakeDamage, OnTakeDamage); //Event_Damage
	
	//After setting defaults, we'll load the client.
	Load(Client);
}

//Information: Called once a client begins to disconnect
public OnClientDisconnect(Client)
{
	//Save right before DC
	Save(Client);
	
	//Set values to default
	g_iCash[Client] = 0;
	g_iBank[Client] = 0;
	g_iIncome[Client] = 0;
	g_sSteamID[Client] = "";
	g_sTempJob[Client] = "Unemployed";
	g_sPermJob[Client] = "Unemployed";
	g_bCuffed[Client] = false;
	g_bAdmin[Client] = false;
	g_iBounty[Client] = 0;
	g_iCuffedBy[Client] = -1;
	g_iTrails[Client] = 0;

	for(new X = 1; X < MAXDOORS; X++)
	{
		g_iAccessDoor[Client][X] = 0;
	}

	for(new X = 0; X < MaxClients; X++)
	{
		g_iGPSTo[Client][X] = 0;
	}

	SDKUnhook(Client, SDKHook_OnTakeDamage, OnTakeDamage); //Event_Damage
}

//Information: We'll use this to check if the owner wants MySQL or SQLite, and load the databases.
LoadSQL()
{
	new iDBType = GetConVarInt(g_hCV_DatabaseType);
	switch(iDBType)
	{
		case 0:
		{
			decl String:sError[164];
			g_hDB = SQLite_UseDatabase("BlueRP", sError, sizeof(sError));
			if(g_hDB == INVALID_HANDLE)
			{
				LogError("BlueRP SQLite Connection Error: %s", sError);
			}
			else
			{
				PrintToServer("BlueRP SQLite Connection Initialized");
				CreateTimer(0.4, CreatePlayerTables);
				CreateTimer(0.6, CreateItemsTables);
				CreateTimer(0.8, CreateDoorsTables);
			}
		}
		
		case 1:
		{
			if(SQL_CheckConfig("BlueRP"))
			{
				PrintToServer("BlueRP Initalizing Connection to MySQL Database");
				SQL_TConnect(DBConnect, "BlueRP");
			}
			else
			{
				LogError("BlueRP Database Error: No Database Config Found! (hl2mp/addons/sourcemod/configs/databases.cfg)");
			}
		}
	}
}

public DBConnect(Handle:hOwner, Handle:hHndl, const String:sError[], any:data)
{
	decl String:sError2[255];	
	if(hOwner == INVALID_HANDLE)
	{
		LogError("BlueRP Database Error: %s", sError);
		return false;
	}
	
	if(hHndl == INVALID_HANDLE)
	{
		LogError("BlueRP Database Error: %s", sError);
		g_hDB = SQLite_UseDatabase("BlueRP", sError2, sizeof(sError2));
		if(g_hDB == INVALID_HANDLE)
		{
			LogError("Database %s", sError);
			return false;
		}
	}
	else
	{
		decl String:sDriver[8];
		SQL_ReadDriver(g_hDB, sDriver, sizeof(sDriver));
		if(strcmp(sDriver, "mysql", false) == 0)
		{
			PrintToServer("BlueRP MySQL Database: fully connected");
		}
		else
		{
			PrintToServer("BlueRP SQLite Database: fully connected");
		}
		
		g_hDB = hHndl;
		CreateTimer(0.4, CreatePlayerTables);
		CreateTimer(0.6, CreateItemsTables);
		CreateTimer(0.8, CreateDoorsTables);
		return false;
	}	
	
	return true;
}

public SaveItem(Client, ItemId, Amount) 
{
	if(IsClientConnected(Client))
	{
		new String:sQuery[512];
		Format(sQuery, sizeof(sQuery), "UPDATE Items SET `%i` = %i WHERE SteamID = '%s';", ItemId, Amount, g_sSteamID[Client]);
		SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
	}
}

public Save(Client)
{
	decl String:sQuery[512];
	Format(sQuery, sizeof(sQuery), "UPDATE `Players` SET `Cash` = %i, `Bank` = %i, `Income` = %i, `Felony` = %i, `Cuffed` = %i, `JailTime` = %i, `JailProgress` = %i, `Minutes` = %i, `Employment` = '%s' WHERE `SteamID` = '%s';", g_iCash[Client], g_iBank[Client], g_iIncome[Client], g_iFelonyLevel[Client], BoolToInt(g_bCuffed[Client]), g_iJailTime[Client], g_iJailProgress[Client], g_iMinutes[Client], g_sPermJob[Client], g_sSteamID[Client]);
	SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
}

public Load(Client)
{
	decl String:sQuery[128];
	new iUser = GetClientUserId(Client); //We'll pass this to the loading query so we can give our client's their stuff
	Format(sQuery, sizeof(sQuery), "SELECT * FROM `Players` WHERE `SteamID` = '%s';", g_sSteamID[Client]);
	SQL_TQuery(g_hDB, SQL_Load, sQuery, iUser);

	DoorLoad(Client);
}

public ItemLoad(Client)
{
	decl String:sQuery[128];
	new iUser = GetClientUserId(Client);
	Format(sQuery, sizeof(sQuery), "SELECT * FROM `Items` WHERE `SteamID` = '%s';", g_sSteamID[Client]);
	SQL_TQuery(g_hDB, SQL_LoadItems, sQuery, iUser);
}

public SQL_Load(Handle:hOwner, Handle:hHndl, const String:sError[], any:data)
{
	new Client = GetClientOfUserId(data);
	if(Client == 0)
	{
		return;
	}
	
	if(hHndl == INVALID_HANDLE)
	{
		LogError("[BlueRP] SQL_Load: Query failed! %s", sError);
	}
	else
	{
		if(!SQL_GetRowCount(hHndl))
		{
			insertPlayer(Client);
		}
		else if(SQL_FetchRow(hHndl))
		{
			g_iCash[Client] = SQL_FetchInt(hHndl, 1);
			g_iBank[Client] = SQL_FetchInt(hHndl, 2);
			g_iIncome[Client] = SQL_FetchInt(hHndl, 3);
			g_iFelonyLevel[Client] = SQL_FetchInt(hHndl, 4);
			new iCuff = SQL_FetchInt(hHndl, 5);
			if(iCuff == 0)
			{
				g_bCuffed[Client] = false;
			}
			else g_bCuffed[Client] = true;
			g_iJailTime[Client] = SQL_FetchInt(hHndl, 6);
			g_iJailProgress[Client] = SQL_FetchInt(hHndl, 7);
			g_iMinutes[Client] = SQL_FetchInt(hHndl, 8);
			
			SQL_FetchString(hHndl, 9, g_sPermJob[Client], sizeof(g_sPermJob[]));
			if(StrContains(g_sPermJob[Client], "Police", false) != -1)
			{
				Format(g_sTempJob[Client], sizeof(g_sTempJob[]), "Off Duty Cop");
			}
			else strcopy(g_sTempJob[Client], sizeof(g_sTempJob[]), g_sPermJob[Client]);
		}		
	}
	ItemLoad(Client);
	if(g_bCuffed[Client])
	{
		CreateTimer(1.0, ProgressJail, Client, TIMER_REPEAT);
	}
	CreateTimer(0.1, ShowHud, Client);
}

public SQL_LoadItems(Handle:hOwner, Handle:hHndl, const String:sError[], any:data)
{
	new Client = GetClientOfUserId(data);
	if(Client == 0)
	{
		return;
	}

	if (hHndl == INVALID_HANDLE)
	{
		LogError("[BlueRP] SQL_LoadItems: Query failed! %s", sError);
	}
	else 
	{
		PrintToConsole(Client, "[BlueRP] Loading Player Items...");

		if(!SQL_GetRowCount(hHndl))
		{
			insertItems(Client);
		}
		else if(SQL_FetchRow(hHndl))
		{
			for(new X = 1; X <= MAXITEMS - 1; X++)
			{
				g_iItem[Client][X] = SQL_FetchInt(hHndl, X);
			}

			PrintToConsole(Client, "[BlueRP] Player Items Loaded.");
		}
	}
}

public insertItems(Client)
{
	decl String:sQuery[1024];
	Format(sQuery, sizeof(sQuery), "INSERT INTO Items (`SteamID`) VALUES ('%s');", g_sSteamID[Client]);
	SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
	PrintToConsole(Client, "[BlueRP] Created Player Items Account");
}

public insertPlayer(Client)
{
	decl String:sQuery[64];
	Format(sQuery, sizeof(sQuery), "INSERT INTO Players (`SteamID`) VALUES ('%s');", g_sSteamID[Client]);
	SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
	CPrintToChat(Client, "%s Creating new Player Account", CMDTAG);
	CPrintToChatAll("%s New Player: %N", CMDTAG, Client);
}

public SQL_RichestPlayers(Handle:hOwner, Handle:hHndl, const String:sError[], any:data)
{
	new Client = GetClientOfUserId(data);
	if(Client == 0)
	{
		return;
	}
	
	if (hHndl == INVALID_HANDLE)
	{
		LogError("[BlueRP] SQL_RichestPlayers: Query failed! %s", sError);
	}
	else 
	{
		decl String:FormatMessage[1024], iCash[25], iBank[25], String:sSteamID[25][32];	
		new len = 0; 
		new i = 1;
		len += Format(FormatMessage[len], sizeof(FormatMessage)-len, "   Richest Players:\n\n");

		while(SQL_FetchRow(hHndl))
		{
			SQL_FetchString(hHndl, 0, sSteamID[i], sizeof(sSteamID[]));
			iCash[i] = SQL_FetchInt(hHndl, 1);
			iBank[i] = SQL_FetchInt(hHndl, 2);
			len += Format(FormatMessage[len], sizeof(FormatMessage)-len, "%i   %s  (%i)\n", i, sSteamID[i], iBank[i] + iCash[i]);
			i++;
		}
		
		PrintEscapeText(Client, FormatMessage);
	}
}

public SQL_MostWanted(Handle:hOwner, Handle:hHndl, const String:sError[], any:data)
{
	new Client = GetClientOfUserId(data);
	if(Client == 0)
	{
		return;
	}

	if (hHndl == INVALID_HANDLE)
	{
		LogError("[BlueRP] SQL_MostWanted: Query failed! %s", sError);
	}
	else 
	{
		decl String:FormatMessage[2048], iFelony[25], String:sSteamID[25][32];
		new len = 0; 
		new i = 1;
		len += Format(FormatMessage[len], sizeof(FormatMessage)-len, "   Most Wanted Players:\n\n");
		
		while(SQL_FetchRow(hHndl))
		{
			SQL_FetchString(hHndl, 0, sSteamID[i], sizeof(sSteamID[]));
			iFelony[i] = SQL_FetchInt(hHndl, 1);
			if(iFelony[i] > 0)
			{
				len += Format(FormatMessage[len], sizeof(FormatMessage)-len, "%i   %s  (%i)\n", i, sSteamID[i], iFelony[i]);
			}
			
			i++;
		}
		PrintEscapeText(Client, FormatMessage);
	}
}

public SQL_Overall(Handle:hOwner, Handle:hHndl, const String:sError[], any:data)
{
	new Client = GetClientOfUserId(data);
	if(Client == 0)
	{
		return;
	}

	if (hHndl == INVALID_HANDLE)
	{
		LogError("[BlueRP] SQL_Overall: Query failed! %s", sError);
	}
	else 
	{
		decl String:FormatMessage[2048], String:sSteamID[25][32], iBank[25], iCash[25], iFelony[25], iIncome[25];
		new len = 0; 
		new i = 1;
		len += Format(FormatMessage[len], sizeof(FormatMessage)-len, "   Overall Top Players:\n\n");
		
		while(SQL_FetchRow(hHndl))
		{
			SQL_FetchString(hHndl, 0, sSteamID[i], sizeof(sSteamID[]));
			iBank[i] = SQL_FetchInt(hHndl, 1);
			iCash[i] = SQL_FetchInt(hHndl, 2);
			iFelony[i] = SQL_FetchInt(hHndl, 3);
			iIncome[i] = SQL_FetchInt(hHndl, 4);
			
			new iTotal = iBank[i] + iCash[i] + iFelony[i] + iIncome[i];
			if(iFelony[i] > 0)
			{
				len += Format(FormatMessage[len], sizeof(FormatMessage)-len, "%i   %s  (%i)\n", i, sSteamID[i], iTotal);
			}
			
			i++;
		}
		PrintEscapeText(Client, FormatMessage);
	}
}

public SQL_GenericQuery(Handle:hOwner, Handle:hHndl, const String:sError[], any:data)
{
	if(hHndl == INVALID_HANDLE)
	{
		LogError("[BlueRP] SQL_GenericQuery: Query failed! %s", sError);
	}
}

public Action:OnPlayerRunCmd(Client, &buttons, &impulse, Float:vel[3], Float:angles[3], &weapon)
{	
	if(IsPlayerAlive(Client))
	{
		if(g_bCuffed[Client])
		{
			buttons &= ~IN_USE;
			buttons &= ~IN_SPEED;
			buttons &= ~IN_JUMP;
		}

		if(g_bOnDrugs[Client])
		{
			buttons &= ~IN_SPEED;
		}
		
		if(buttons & IN_USE)
		{
			if(!g_bPreThink[Client])
			{
				useButton(Client);
				g_bPreThink[Client] = true;
			}
		}
		else if(buttons & IN_SPEED)
		{
			if(!g_bPreThink[Client])
			{
				speedButton(Client);
				g_bPreThink[Client] = true;
			}
		}
		else g_bPreThink[Client] = false;
	}
	return Plugin_Continue;
}

public Action:speedButton(Client)
{
	new iEnt = GetClientAimTarget(Client, false);
	decl String:sClass[64];
	if(IsValidEntity(iEnt))
	{
		GetEdictClassname(iEnt, sClass, sizeof(sClass));

		//Lock and Unlock Doors
		if(StrEqual(sClass, "func_door_rotating") || StrEqual(sClass, "prop_door_rotating"))
		{
			if(g_iAccessDoor[Client][iEnt] == 1)
			{
				if(g_bDoorLocked[iEnt]) 
				{
					AcceptEntityInput(iEnt, "Unlock");
					g_bDoorLocked[iEnt] = false;
					CPrintToChat(Client, "%s You unlock the door", CMDTAG);
					return Plugin_Handled;
				}
				else if(!g_bDoorLocked[iEnt])
				{
					AcceptEntityInput(iEnt, "Lock");
					g_bDoorLocked[iEnt] = true;
					CPrintToChat(Client, "%s You lock the door", CMDTAG);
					return Plugin_Handled;
				}
			}
		}

		//Robbing NPCs
		if(StrContains(sClass, "npc_", false) != -1)
		{
			GetEntPropVector(iEnt, Prop_Send, "m_vecOrigin", g_fNPCRobOrigin[Client]);
			if(g_fLastPressedShift[Client] >= GetGameTime() - 1.0)
			{
				decl String:sData[32], String:sBuffer[5][32], Float:fClientOrigin[3], Float:fOrigin[3], String:sNPC[32];
				new Handle:hVault = CreateKeyValues("Vault");
				FileToKeyValues(hVault, g_sNPCPath);
				for(new X = 0; X < 100; X++)
				{
					IntToString(X, sNPC, 255);
					LoadString(hVault, "1", sNPC, "Null", sData); //BANKER ROBBING LULZ
					if(StrContains(sData, "NaN", false) == -1)
					{
						ExplodeString(sData, " ", sBuffer, 5, 32);
						GetClientAbsOrigin(Client, fClientOrigin);
						fOrigin[0] = StringToFloat(sBuffer[1]);
						fOrigin[1] = StringToFloat(sBuffer[2]);
						fOrigin[2] = StringToFloat(sBuffer[3]);
						new Float:fDist = GetVectorDistance(fClientOrigin, fOrigin);
						if(fDist <= 150)
						{
							RobNPC(Client, 857, 1, StringToInt(sNPC));
						}
					}
					
					LoadString(hVault, "2", sNPC, "Null", sData);
					if(StrContains(sData, "NaN", false) == -1)
					{
						ExplodeString(sData, " ", sBuffer, 5, 32);
						GetClientAbsOrigin(Client, fClientOrigin);
						fOrigin[0] = StringToFloat(sBuffer[1]);
						fOrigin[1] = StringToFloat(sBuffer[2]);
						fOrigin[2] = StringToFloat(sBuffer[3]);
						
						new Float:fDist = GetVectorDistance(fClientOrigin, fOrigin);
						if(fDist <= 150)
						{
							RobNPC(Client, 622, 2, StringToInt(sNPC));
						}
					}
				}

				CloseHandle(hVault);
				return Plugin_Handled;
			}
			else
			{
				CPrintToChat(Client, "%s Press <SPRINT> again to rob this NPC", CMDTAG);
				g_fLastPressedShift[Client] = GetGameTime();
			}
		}
	}
	return Plugin_Continue;
}

public Action:useButton(Client)
{
	g_fLastPressedE[Client] = GetGameTime();
	new iEnt = GetClientAimTarget(Client, false);
	if(iEnt != -1 && IsValidEntity(iEnt))
	{
		decl Float:fClientOrigin[3], Float:fEntOrigin[3], String:sClassName[64];
		GetClientAbsOrigin(Client, fClientOrigin);
		GetEntPropVector(iEnt, Prop_Send, "m_vecOrigin", fEntOrigin);
		GetEntityClassname(iEnt, sClassName, sizeof(sClassName));
		new Float:fDist = GetVectorDistance(fClientOrigin, fEntOrigin);
		if(fDist <= 100) 
		{
			if(bIsMoneyModel(iEnt) && g_iValue[iEnt] > 0) //Picking up money
			{
				CPrintToChat(Client, "%s You picked up $%i", CMDTAG, g_iValue[iEnt]);
				g_iCash[Client] += g_iValue[iEnt];
				g_iValue[iEnt] = 0;
				AcceptEntityInput(iEnt, "Kill");
				return Plugin_Handled;
			}
			
			for(new i = 0; i < MAXITEMS; i++)
			{
				if(g_iItemAmount[iEnt][i] > 0) //Picking up Items
				{
					decl String:sModel[128];
					GetEntPropString(iEnt, Prop_Data, "m_ModelName", sModel, 128);
					if(StrEqual(sModel, "models/Items/BoxMRounds.mdl", false))
					{
						AcceptEntityInput(iEnt, "Kill", Client);
						g_iItem[Client][i] += g_iItemAmount[iEnt][i];
						SaveItem(Client, i, g_iItemAmount[iEnt][i]); 
						CPrintToChat(Client, "%s You pick up %i of %s!", CMDTAG, g_iItemAmount[iEnt][i], g_sItemName[i]);
						g_iItemAmount[iEnt][i] = 0;
						return Plugin_Handled;
					}
				}
			}
		}
			
		if(iEnt > 0 && iEnt < MaxClients && IsClientInGame(iEnt) && IsClientConnected(iEnt)) //Players
		{
			decl Float:fOrigin[3];
			decl Float:fPlayerOrigin[3];
			GetClientAbsOrigin(Client, fOrigin);
			GetClientAbsOrigin(iEnt, fPlayerOrigin);
			new Float:fDistz = GetVectorDistance(fOrigin, fPlayerOrigin);
			if(fDistz <= 120)
			{
				if(bIsCombine(Client) && g_bCuffed[iEnt])
				{
					if(g_fLastPressedE[Client] >= GetGameTime() - 3.0)
					{
						if(g_bHasJail)
						{
							sendToJail(iEnt);
							CPrintToChat(Client, "%s You sent %N to Jail", CMDTAG, iEnt);
							CPrintToChat(iEnt, "%s You are sent to Jail by %N", CMDTAG, Client);
						}
						else
						{
							CPrintToChat(Client, "%s Server has not created a Jail", CMDTAG);
						}
					}
					else
					{
						CPrintToChat(Client, "%s Press <USE> again to send %N to Jail", CMDTAG, iEnt);
						g_fLastPressedE[Client] = GetGameTime();
					}
				}
				
				g_iMenuTarget[Client] = iEnt;
				new Handle:hPlayerInfo = CreateMenu(PlayerInformation);
				SetMenuTitle(hPlayerInfo, "Interacting with: %N", iEnt);
				AddMenuItem(hPlayerInfo, "1", "[Give Money]");
				AddMenuItem(hPlayerInfo, "2", "[Give Item]");
				if(bIsCombine(Client))
				{		
					AddMenuItem(hPlayerInfo, "3", "[Send to AFK Room]");
					if(g_bCuffed[g_iMenuTarget[Client]])
					{
						AddMenuItem(hPlayerInfo, "4", "[Jail]");
						AddMenuItem(hPlayerInfo, "5", "[Restrain]");
						AddMenuItem(hPlayerInfo, "6", "[Suicide Chamber]");
						AddMenuItem(hPlayerInfo, "7", "[Release Player]");
					}
				}	
				SetMenuPagination(hPlayerInfo, 7);
				DisplayMenu(hPlayerInfo, Client, 30);
				return Plugin_Handled;
			}
		}

		if(StrContains(sClassName, "npc_", false) != -1 && !g_bCuffed[Client])
		{
			decl Float:fCOrigin[3];
			decl Float:fNPCOrigin[3];
			GetClientAbsOrigin(Client, fCOrigin);
			decl String:sNPC[255], String:sData[255], String:sBuffer[5][32];
			new Handle:hVault = CreateKeyValues("Vault");
			FileToKeyValues(hVault, g_sNPCPath);
			for(new X = 0; X < MAXNPCS; X++)
			{
				IntToString(X, sNPC, sizeof(sNPC));	
				
				LoadString(hVault, "0", sNPC, "NaN", sData); //Employers
				if(StrContains(sData, "NaN", false) == -1)
				{
					ExplodeString(sData, " ", sBuffer, 5, 32);

					GetClientAbsOrigin(Client, fCOrigin);
					fNPCOrigin[0] = StringToFloat(sBuffer[1]);
					fNPCOrigin[1] = StringToFloat(sBuffer[2]);
					fNPCOrigin[2] = StringToFloat(sBuffer[3]);

					new Float:fDist2 = GetVectorDistance(fCOrigin, fNPCOrigin);
					if(fDist2 <= 100)
					{
						//JobMenu(Client);		
						CloseHandle(hVault);
						return Plugin_Handled;
					}
				}
				
				LoadString(hVault, "1", sNPC, "NaN", sData); //Bankers
				if(StrContains(sData, "NaN", false) == -1)
				{
					ExplodeString(sData, " ", sBuffer, 5, 32);
					GetClientAbsOrigin(Client, fCOrigin);
					fNPCOrigin[0] = StringToFloat(sBuffer[1]);
					fNPCOrigin[1] = StringToFloat(sBuffer[2]);
					fNPCOrigin[2] = StringToFloat(sBuffer[3]);
					new Float:fDist2 = GetVectorDistance(fCOrigin, fNPCOrigin);
					if(fDist2 <= 100)
					{
						Bank(Client);		
						CloseHandle(hVault);
						return Plugin_Handled;
					}
				}
				
				LoadString(hVault, "2", sNPC, "NaN", sData); //Vendors
				if(StrContains(sData, "NaN", false) == -1)
				{
					ExplodeString(sData, " ", sBuffer, 5, 32);
					GetClientAbsOrigin(Client, fCOrigin);
					fNPCOrigin[0] = StringToFloat(sBuffer[1]);
					fNPCOrigin[1] = StringToFloat(sBuffer[2]);
					fNPCOrigin[2] = StringToFloat(sBuffer[3]);
					new Float:fDist2 = GetVectorDistance(fCOrigin, fNPCOrigin);
					if(fDist2 <= 100)
					{
						Vendor(Client, X);		
						CloseHandle(hVault);
						return Plugin_Handled;
					}
				}
			}
			CloseHandle(hVault);
			return Plugin_Handled;
		}
	}
	return Plugin_Continue;
}

public PlayerInformation(Handle:hPlayerInfo, MenuAction:action, Client, param2)
{
	if(action == MenuAction_Select)
	{
		new String:sData[64];
		GetMenuItem(hPlayerInfo, param2, sData, sizeof(sData));
		new iData = StringToInt(sData);
		
		switch(iData)
		{
			case 1: //Giving Money
			{
				new Handle:hGiveMoney = CreateMenu(GiveMoney);
				SetMenuTitle(hGiveMoney, "Giving Money to: %N", g_iMenuTarget[Client]);
				new String:sDisplay[11][64] = {"1", "5", "25", "100", "500", "1000", "5000", "10000", "50000", "100000", "All"};
				new iAmount[11] = {1, 5, 25, 100, 500, 1000, 5000, 10000, 50000, 100000, 0};
				new String:sAmount[11][32];
				for(new X = 0; X < 11; X++)
				{
					IntToString(iAmount[X], sAmount[X], sizeof(sAmount[]));
					AddMenuItem(hGiveMoney, sDisplay[X], sAmount[X]);
				}
				SetMenuPagination(hGiveMoney, 7);
				DisplayMenu(hGiveMoney, Client, 30);
				
				CPrintToChat(Client, "%s Press <ESC> to view the menu", CMDTAG);
			}
			case 2: //Giving an Item
			{
				g_bGivingItems[Client] = true;
				ShowItems(Client);
			}
		}
	}
}

public GiveMoney(Handle:hGiveMoney, MenuAction:action, Client, param2)
{
	decl String:sData[64];
	GetMenuItem(hGiveMoney, param2, sData, sizeof(sData));
	new iData = StringToInt(sData);
	new iTarget = g_iMenuTarget[Client];

	if(action == MenuAction_Select)
	{
		if(g_iCash[Client] >= iData)
		{
			g_iCash[Client] -= iData;
			g_iCash[iTarget] += iData;
			Save(Client);
			Save(iTarget);

			CPrintToChat(Client, "%s You give %N $%i", CMDTAG, iTarget, iData);
			CPrintToChat(iTarget, "%s %N gave you $%i", CMDTAG, Client, iData);
		}
		else 
		{
			CPrintToChat(Client, "%s You don't have $%i", CMDTAG, iData);
		}
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(hGiveMoney);
	}
	return 0;
}

public GiveItems(Handle:hGiveAmount, MenuAction:action, Client, param2)
{
	decl String:sData[64];
	GetMenuItem(hGiveAmount, param2, sData, sizeof(sData));
	new iTarget = g_iMenuTarget[Client];
	if(action == MenuAction_Select)
	{
		if(IsClientConnected(iTarget) && IsClientInGame(iTarget))
		{			
			decl String:sSplit[2][255];
			ExplodeString(sData, "-", sSplit, 2, 32);
			new iItem = StringToInt(sSplit[0]);
			new iAmount = StringToInt(sSplit[1]);
			
			if(g_iItem[Client][iItem] - iAmount >= 0)
			{
				g_iItem[Client][iItem] -= iAmount;
				SaveItem(Client, iItem, g_iItem[Client][iItem]); 
				Save(Client);
				
				g_iItem[iTarget][iItem] += iAmount;
				//Print:
				CPrintToChat(Client, "%s You give %N %i of %s!", CMDTAG, iTarget, iAmount, g_sItemName[iItem]);
				CPrintToChat(iTarget, "%s You recieve %i of %s from %N!", CMDTAG, iAmount, g_sItemName[iItem], Client);
				
				g_bGivingItems[Client] = true;
				ShowItems(Client);
				Save(iTarget);
				SaveItem(Client, iItem, g_iItem[iTarget][iItem]);
			}
			else
			{
				CPrintToChat(Client, "%s You don't have %i x %s!", CMDTAG, iAmount, g_sItemName[iItem]);
			}
		}
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(hGiveAmount);
	}
	return 0;
}

public Action:ShowItems(Client)
{
	new bool:bHasItem = false;	
	new Handle:hItem = CreateMenu(InventoryMenu);
	
	new iNumber = 0;
	new iValue = 0;

	new iTaken[64];
	for(new X = 0; X < MAXITEMS; X++)
	{
		if(g_iItem[Client][X] > 0)
		{
			bHasItem = true;
			if(g_iItemAction[X] == 1 && iTaken[1] != 2)
			{
				iTaken[1] = 2;
				AddMenuItem(hItem, "-1", "[Weapons]");
			}
			else if(g_iItemAction[X] == 2 && iTaken[2] != 2)
			{
				iTaken[2] = 2;
				AddMenuItem(hItem, "-2", "[Alcohol]");
			}
			else if(g_iItemAction[X] == 3 && iTaken[3] != 2)
			{
				iTaken[3] = 2;
				AddMenuItem(hItem, "-3", "[Drugs]");
			}
			else if(g_iItemAction[X] == 4 && iTaken[4] != 2)
			{
				iTaken[4] = 2;
				AddMenuItem(hItem, "-4", "[Food]");
			}
			else if(g_iItemAction[X] == 5 && iTaken[5] != 2)
			{
				iTaken[5] = 2;
				AddMenuItem(hItem, "-5", "[Lockpicks]");
			}
			else if(g_iItemAction[X] == 6 && iTaken[6] != 2)
			{
				iTaken[6] = 2;
				AddMenuItem(hItem, "-6", "[Doorhacks]");
			}
			else if(g_iItemAction[X] == 7 && iTaken[7] != 2)
			{
				iTaken[7] = 2;
				AddMenuItem(hItem, "-7", "[Furniture]");
			}
			else if(g_iItemAction[X] == 8 && iTaken[8] != 2)
			{
				iTaken[8] = 2;
				AddMenuItem(hItem, "-8", "[Medical Kits]");
			}
			else if(g_iItemAction[X] == 9 && iTaken[9] != 2)
			{
				iTaken[9] = 2;
				AddMenuItem(hItem, "-9", "[Cuff Keys]");
			}
			else if(g_iItemAction[X] == 10 && iTaken[10] != 2)
			{
				iTaken[10] = 2;
				AddMenuItem(hItem, "-10", "[Server Commands]");
			}
			else if(g_iItemAction[X] == 11 && iTaken[11] != 2)
			{
				iTaken[11] = 2;
				AddMenuItem(hItem, "-11", "[Locks]");
			}
			else if(g_iItemAction[X] == 12 && iTaken[12] != 2)
			{
				iTaken[12] = 2;
				AddMenuItem(hItem, "-12", "[Lockbreakers]");
			}
			else if(g_iItemAction[X] == 13 && iTaken[13] != 2)
			{
				iTaken[13] = 2;
				AddMenuItem(hItem, "-13", "[GPS Items]");
			}
			else if(g_iItemAction[X] == 14 && iTaken[14] != 2)
			{
				iTaken[14] = 2;
				AddMenuItem(hItem, "-14", "[Police Trackers]");
			}
			else if(g_iItemAction[X] == 15 && iTaken[15] != 2)
			{
				iTaken[15] = 2;
				AddMenuItem(hItem, "-15", "[Police Jammers]");
			}
			else if(g_iItemAction[X] == 16 && iTaken[16] != 2)
			{
				iTaken[16] = 2;
				AddMenuItem(hItem, "-16", "[Player Models]");
			}
			else if(g_iItemAction[X] == 17 && iTaken[17] != 2)
			{
				iTaken[17] = 2;
				AddMenuItem(hItem, "-17", "[Trails]");
			}
			iNumber += g_iItem[Client][X];
			iValue += g_iItem[Client][X] * g_iItemCost[X];
		}
	}
	SetMenuTitle(hItem, "Item Inventory:\nTotal Items: %i\nTotal Value: $%i", iNumber, iValue);
	
	if(bHasItem)
	{
		SetMenuPagination(hItem, 7);
		DisplayMenu(hItem, Client, 50);
		CPrintToChat(Client, "%s Press <ESC> to view your items", CMDTAG);
	}
	else
	{
		CloseHandle(hItem);
		CPrintToChat(Client, "%s You don't have any items!", CMDTAG);
		return Plugin_Handled;
	}
	return Plugin_Handled;
}

public InventoryMenu(Handle:hItem, MenuAction:action, Client, param2)
{
	if(action == MenuAction_Select)
	{
		decl String:sData[64];
		GetMenuItem(hItem, param2, sData, sizeof(sData));
		new iData = StringToInt(sData);
		decl String:sHiddenInfo[64];
		
		if(iData == 99999)
		{
			ShowItems(Client);
		}
		else if(g_bGivingItems[Client])
		{
			if(iData < 0)
			{
				OrganizeList(Client, iData);
			}
			else
			{
				decl String:sAll[64];		
				new Handle:hGiveAmount = CreateMenu(GiveItems);
				SetMenuTitle(hGiveAmount, "Giving %s(s)\n=============\n%i left", g_sItemName[iData], g_iItem[Client][iData]);
				new String:sBuffers[9][10] = {"0", "1", "5", "25", "100", "500", "1000", "5000", "10000"};
				
				for(new X = 0; X < 9; X++)
				{
					if(StringToInt(sBuffers[X]) == 0)
					{
						Format(sAll, 64, "All (%i)", g_iItem[Client][iData]);
						Format(sHiddenInfo, 64, "%s-%i", sData, g_iItem[Client][iData]);
						AddMenuItem(hGiveAmount, sHiddenInfo, sAll);
					}
					else
					{
						Format(sHiddenInfo, 64, "%s-%s", sData, sBuffers[X]);
						AddMenuItem(hGiveAmount, sHiddenInfo, sBuffers[X]);
					}
				}
				SetMenuPagination(hGiveAmount, 7);
				DisplayMenu(hGiveAmount, Client, 30);
			}
		}
		else
		{
			if(iData < 0)
			{
				OrganizeList(Client, iData);
			}
			else
			{
				decl String:sHiddenInfo2[64];
				Format(sHiddenInfo, 64, "%s-1", sData);
				Format(sHiddenInfo2, 64, "%s-2", sData);
				new Handle:hItemUse = CreateMenu(UseItem);
				SetMenuTitle(hItemUse, "%s\n=============\n%i left", g_sItemName[iData], g_iItem[Client][iData]);
				switch(g_iItemAction[iData])
				{
					case 7:
					{
						AddMenuItem(hItemUse, sHiddenInfo, "[Spawn]");
						AddMenuItem(hItemUse, sHiddenInfo2, "[Drop]");
					}
					case 11, 12:
					{
						decl String:sHiddenInfo3[64], String:sHiddenInfo4[64], String:sHiddenInfo5[64], String:sHiddenInfo6[64];
						Format(sHiddenInfo3, 64, "%s-4", sData);
						Format(sHiddenInfo4, 64, "%s-5", sData);
						Format(sHiddenInfo5, 64, "%s-6", sData);
						Format(sHiddenInfo6, 64, "%s-7", sData);
						AddMenuItem(hItemUse, sHiddenInfo, "[Use]");
						AddMenuItem(hItemUse, sHiddenInfo3, "[Use 5]");
						AddMenuItem(hItemUse, sHiddenInfo4, "[Use 10]");
						AddMenuItem(hItemUse, sHiddenInfo5, "[Use 25]");
						AddMenuItem(hItemUse, sHiddenInfo6, "[Use 100]");
						AddMenuItem(hItemUse, sHiddenInfo2, "[Drop]");
					}
					default:
					{
						AddMenuItem(hItemUse, sHiddenInfo, "[Use]");
						AddMenuItem(hItemUse, sHiddenInfo2, "[Drop]");
					}
				}
				
				AddMenuItem(hItemUse, "69-3", "[Back to Inventory]");
				
				SetMenuPagination(hItemUse, 7);
				DisplayMenu(hItemUse, Client, 30);
			}
		}
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(hItem);
	}
	return 0;
}

public UseItem(Handle:hItemUse, MenuAction:action, Client, param2)
{
	if(action == MenuAction_Select)
	{
		decl String:sData[64];
		GetMenuItem(hItemUse, param2, sData, sizeof(sData));
		
		decl String:sSplitter[2][255];
		ExplodeString(sData, "-", sSplitter, 2, 32);
		new iItem = StringToInt(sSplitter[0]);
		new iData = StringToInt(sSplitter[1]);
		
		//Use:
		if(iData == 1 || iData == 4 || iData == 5 || iData == 6 || iData == 7 && /*lastspawn[Client] <= (GetGameTime() - 5) &&*/g_iItem[Client][iItem] > 0)
		{
			//lastspawn[Client] = GetGameTime();		
			switch(g_iItemAction[iItem])
			{
				case 1: //Weapons, HL2 Items
				{
					GivePlayerItem(Client, g_sItemVar[iItem]);
					(StrHasVowel(g_sItemName[iItem])) ? CPrintToChat(Client, "%s You use an %s", CMDTAG, g_sItemName[iItem]) : CPrintToChat(Client, "%s You use a %s", CMDTAG, g_sItemName[iItem]);
					
					g_iItem[Client][iItem] -= 1;
					SaveItem(Client, iItem, g_iItem[Client][iItem]);
				}
				
				case 2, 3: //Alcohol, Drugs
				{
					new iRandom = GetRandomInt(1, 3);
					new iVar = StringToInt(g_sItemVar[iItem]);
					if(iVar == 0)
					{
						switch(iRandom)
						{
							case 1:
							{
								ClientCommand(Client, "r_screenoverlay effects/tp_eyefx/tpeye.vmt");
							}

							case 2:
							{
								ClientCommand(Client, "r_screenoverlay effects/tp_eyefx/tpeye2.vmt");
							}

							case 3:
							{
								ClientCommand(Client, "r_screenoverlay effects/tp_eyefx/tpeye3.vmt");
							}
						}
						Shake(Client, 60.0, (5.0 * StringToFloat(g_sItemVar[iItem])));
						CreateTimer(60.0, RemoveOverlay, Client);
						CPrintToChat(Client, "%s You use %s", CMDTAG, g_sItemName[iItem]);
						g_iItem[Client][iItem] -= 1;
					}
					if(iVar == 1) 
					{
						ClientCommand(Client, "r_screenoverlay models/props_combine/portalball001_sheet.vmt"); //Weed
						g_fSpeed[Client] = 80.0; 
						SetEntityHealth(Client, 150);
						SetEntityGravity(Client, 0.8);
					}
					if(iVar == 2)
					{
						ClientCommand(Client, "r_screenoverlay debug/yuv.vmt"); //Coke
						SetEntityHealth(Client,150);
					}
					if(iVar == 3)
					{
						ClientCommand(Client, "r_screenoverlay effects/com_shield002a.vmt"); //Heroin
						SetEntityHealth(Client,1);
						SetEntityGravity(Client,0.4);
					}
					if(iVar == 4)
					{
						ClientCommand(Client, "r_screenoverlay models/effects/portalfunnel_sheet.vmt"); //LSD
						g_fSpeed[Client] = 290.0; 
						SetEntityGravity(Client,0.8); 
					}
					if(iVar == 5) 
					{
						ClientCommand(Client, "r_screenoverlay effects/tp_eyefx/tp_eyefx.vmt"); //Extasy
						g_fSpeed[Client] = 390.0;
						
					}
					if(iVar == 6) 
					{
						ClientCommand(Client, "r_screenoverlay effects/combine_binocoverlay.vmt"); //Trip
						g_fSpeed[Client] = 300.0; 
						SetEntityHealth(Client,150);  
					} 
					if(iVar == 7) //Speedhack
					{
						g_fSpeed[Client] = 300.0; 
					}
					if(iVar == 8) //Doping Health
					{
						SetEntityHealth(Client, 200);
					}
					if(iVar == 9) //Doping Gravity
					{
						SetEntityGravity(Client, 0.5);
					}
					if(iVar == 10) //Multi Dope
					{
						g_fSpeed[Client] = 200.0;
						SetEntityHealth(Client, 200); 
						SetEntityGravity(Client, 0.6);
					}
					if(iVar == 11) //Bear Dope
					{
						SetEntityHealth(Client, 800);
					}
					if(iVar == 12)
					{
						ClientCommand(Client, "r_screenoverlay models/effects/portalfunnel_sheet.vmt"); //Eas Dope
						g_fSpeed[Client] = 800.0;
						SetEntityHealth(Client, 169);
					}
					if(iVar == 13)
					{
						SetEntityHealth(Client, 125);
					}
					
					if(iVar <= 6)
					{
						Shake(Client, 60.0, (5.0 * StringToFloat(g_sItemVar[iItem])));
						if(iVar >= 4 && iVar != 6) 
						{
							for(new X = 0; X < 60; X += 7 - iVar) 
							{
								CreateTimer(float(X), DrugFade, Client);
							}
						}
					}

					g_bOnDrugs[Client] = true;
					if(iVar < 13) CreateTimer(10.0, RemoveDrugEffect, Client);
					CPrintToChat(Client, "%s You use %s", CMDTAG, g_sItemName[iItem]);
					g_iItem[Client][iItem] -= 1;		
				}
				
				case 4: //Food
				{
					new iHP = GetClientHealth(Client);
					new iHeal = StringToInt(g_sItemVar[iItem]);
					if(iHP > 99)
					{
						CPrintToChat(Client, "%s You already have full health", CMDTAG);
					}
					else 
					{
						if(StrHasVowel(g_sItemName[iItem]))
						{
							if(iHP + iHeal >= 100)
							{
								SetEntityHealth(Client, 100);
							}
							SetEntityHealth(Client, iHP + iHeal);
							CPrintToChat(Client, "%s You eat an %s", CMDTAG, g_sItemName[iItem]);	
						}
						else
					 	{
					 		if(iHP + iHeal >= 100)
							{
								SetEntityHealth(Client, 100);
							}
							SetEntityHealth(Client, iHP + iHeal);
							CPrintToChat(Client, "%s You eat a %s", CMDTAG, g_sItemName[iItem]);
						}
						
						g_iItem[Client][iItem] -= 1;
						SaveItem(Client, iItem, g_iItem[Client][iItem]);
					}
				}
				
				case 5: //Lockpicks
				{
					decl String:sClassName[16];
					new iDurability = StringToInt(g_sItemVar[iItem]);
					new iDoor = GetClientAimTarget(Client, false);
					GetEntityClassname(iDoor, sClassName, sizeof(sClassName));
					if(IsValidEdict(iDoor))
					{
						if(StrEqual(sClassName, "func_door_rotating") || StrEqual(sClassName, "prop_door_rotating"))
						{
							if(g_iLocks[iDoor] <= iDurability)
							{
								AcceptEntityInput(iDoor, "Unlock");
								CPrintToChat(Client, "%s You pick the lock on the door", CMDTAG);
								g_iItem[Client][iItem] -= 1;
							}
							else
							{
								CPrintToChat(Client, "%s This door has locks on it", CMDTAG);
							}
						}
						else
						{
							CPrintToChat(Client, "%s You can't lockpick this type of door", CMDTAG);
						}
					}
				}
				
				case 6: //Doorhacks
				{
					decl String:sClassN[16], String:sSplit1[2][32];
					ExplodeString(g_sItemVar[iItem], "-", sSplit1, 2, sizeof(sSplit1[]));
					new iDurability = StringToInt(sSplit1[1]);
					new iDoor = GetClientAimTarget(Client, false);
					GetEntityClassname(iDoor, sClassN, sizeof(sClassN));
					if(IsValidEdict(iDoor))
					{
						if(StrEqual(sClassN, "func_door"))
						{
							if(g_iLocks[iDoor] <= iDurability)
							{
								AcceptEntityInput(iDoor, "Unlock");
								AcceptEntityInput(iDoor, "Open");
								CPrintToChat(Client, "%s You doorhack the door open", CMDTAG);
								g_iItem[Client][iItem] -= 1;
							}
							else
							{
								CPrintToChat(Client, "%s This door has locks on it", CMDTAG);
							}
						}
						else
						{
							CPrintToChat(Client, "%s You can't lockpick this type of door", CMDTAG);
						}
					}
				}
				
				case 7: //Furniture
				{
					decl Float:fOrigin[3], Float:fEyes[3], Float:fPropOrigin[3];
					GetClientAbsOrigin(Client, fOrigin);
					GetClientEyeAngles(Client, fEyes);
					
					fPropOrigin[0] = fOrigin[0] + (50 * Cosine(DegToRad(fEyes[1])));
					fPropOrigin[1] = fOrigin[1] + (50 * Sine(DegToRad(fEyes[1])));
					fPropOrigin[2] = fOrigin[2] + 100;
					
					//Create Entity and make it Destructable
					new iEnt = CreateEntityByName("prop_physics_override");
					DispatchKeyValue(iEnt, "physdamagescale", "1.0");
					SetEntProp(iEnt, Prop_Data, "m_takedamage", 2, 1);
					DispatchKeyValue(iEnt, "Health", "100");
					DispatchKeyValue(iEnt, "Model", g_sItemVar[iItem]);
					DispatchSpawn(iEnt);
					TeleportEntity(iEnt, fPropOrigin, NULL_VECTOR, NULL_VECTOR);
					
					CPrintToChat(Client, "%s You spawn a prop", CMDTAG);	
					g_iItem[Client][iItem] -= 1;
					SaveItem(Client, iItem, g_iItem[Client][iItem]);
				}
				
				case 8: //Medical Kits
				{
					new iVar = StringToInt(g_sItemVar[iItem]);
					new iHealth = GetClientHealth(Client);
					new iTarget = GetClientAimTarget(Client, false);

					if(iVar + iHealth < 100)
					{
						CPrintToChat(Client, "%s You heal %N to full health", CMDTAG, iTarget);
						SetEntityHealth(iTarget, 100);
						g_iItem[Client][iItem] -= 1;
					}

					CPrintToChat(Client, "%s You heal %N for %i", CMDTAG, iVar);
					SetEntityHealth(iTarget, iHealth + iVar);
					g_iItem[Client][iItem] -= 1;
				}
				
				case 9: //Cuff Keys
				{
					new iTarget = GetClientAimTarget(Client, false); //Who we're uncuffing
					if(iTarget > 0) //Make sure they're a valid client
					{
						if(g_bCuffed[iTarget]) //Uncuff only cuffed people
						{
							CPrintToChat(Client, "%s You use a cuff key on %N", CMDTAG, iTarget);
							CPrintToChat(iTarget, "%s %N Unlocked your cuffs with a Cuff Key", CMDTAG, Client);
							SetEntityRenderMode(iTarget, RENDER_NORMAL);
							SetEntityRenderColor(iTarget, 255, 255, 255, 255);
							g_iCuffedBy[iTarget] = -1;
							g_bCuffed[iTarget] = false;
							g_iItem[Client][iItem] -= 1;
						}
					}
					else
					{
						CPrintToChat(Client, "%s Invalid Target", CMDTAG);
					}
				}
				
				case 10: //Server Commands
				{
					ServerCommand(g_sItemVar[iItem]);
					CPrintToChat(Client, "%s You used a command: (%s)", CMDTAG, g_sItemVar[iItem]);
					g_iItem[Client][iItem] -= 1;
				}
				
				case 11: //Locks
				{
					decl String:sClassN2[32];
					new iDoor = GetClientAimTarget(Client, false);
					GetEdictClassname(iDoor, sClassN2, sizeof(sClassN2));
					if(IsValidEdict(iDoor))
					{
						if(StrEqual(sClassN2, "func_door") || StrEqual(sClassN2, "func_door_rotating") || StrEqual(sClassN2, "prop_door_rotating"))
						{
							switch(iData)
							{
								case 1: //Add 1
								{
									if(g_iItem[Client][iItem] >= 1)
									{
										g_iLocks[iDoor] += 1; //Add 1 lock to the door (iDoor)
										g_iItem[Client][iItem] -= 1;
										SaveItem(Client, iItem, g_iItem[Client][iItem]);
										CPrintToChat(Client, "%s You add one lock to the Door", CMDTAG);
										decl String:sQuery[255];
										Format(sQuery, sizeof(sQuery), "UPDATE `Doors` SET `Locks` = %i WHERE `DoorID` = %i", g_iLocks[iDoor], iDoor);
										SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
									}
								}	
								case 4: //Add 5
								{
									if(g_iItem[Client][iItem] >= 5)
									{
										g_iLocks[iDoor] += 5; //Add 5 Locks to the door (iDoor)
										g_iItem[Client][iItem] -= 5;
										SaveItem(Client, iItem, g_iItem[Client][iItem]);
										CPrintToChat(Client, "%s You add 5 locks to the Door", CMDTAG);
										decl String:sQuery[255];
										Format(sQuery, sizeof(sQuery), "UPDATE `Doors` SET `Locks` = %i WHERE `DoorID` = %i", g_iLocks[iDoor], iDoor);
										SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
									}
								}
								case 5: //Add 10
								{
									if(g_iItem[Client][iItem] >= 10)
									{
										g_iLocks[iDoor] += 10; //Add 10 Locks to the door (iDoor)
										g_iItem[Client][iItem] -= 10;
										SaveItem(Client, iItem, g_iItem[Client][iItem]);
										CPrintToChat(Client, "%s You add 10 locks to the Door", CMDTAG);
										decl String:sQuery[255];
										Format(sQuery, sizeof(sQuery), "UPDATE `Doors` SET `Locks` = %i WHERE `DoorID` = %i", g_iLocks[iDoor], iDoor);
										SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
									}
								}
								case 6: //Add 25
								{
									if(g_iItem[Client][iItem] >= 25)
									{
										g_iLocks[iDoor] += 25; //Add 25 Locks to the door (iDoor)
										g_iItem[Client][iItem] -= 25;
										SaveItem(Client, iItem, g_iItem[Client][iItem]);
										CPrintToChat(Client, "%s You add 25 locks to the Door", CMDTAG);
										decl String:sQuery[255];
										Format(sQuery, sizeof(sQuery), "UPDATE `Doors` SET `Locks` = %i WHERE `DoorID` = %i", g_iLocks[iDoor], iDoor);
										SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
									}
								}
								case 7: //Add 100
								{
									if(g_iItem[Client][iItem] >= 100)
									{
										g_iLocks[iDoor] += 100; //Add 100 Locks to the door (iDoor)
										g_iItem[Client][iItem] -= 100;
										SaveItem(Client, iItem, g_iItem[Client][iItem]);
										CPrintToChat(Client, "%s You add 100 locks to the Door", CMDTAG);
										decl String:sQuery[255];
										Format(sQuery, sizeof(sQuery), "UPDATE `Doors` SET `Locks` = %i WHERE `DoorID` = %i", g_iLocks[iDoor], iDoor);
										SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
									}
								}
							}	
						}
					}		
				}
				
				case 12: //Lockbreakers
				{
					decl String:sClassN3[32];
					new iDoor = GetClientAimTarget(Client, false);
					GetEdictClassname(iDoor, sClassN3, sizeof(sClassN3));
					if(IsValidEdict(iDoor))
					{
						if(StrEqual(sClassN3, "func_door") || StrEqual(sClassN3, "func_door_rotating") || StrEqual(sClassN3, "prop_door_rotating"))
						{
							switch(iData)
							{
								case 1: //Break 1
								{
									g_iLocks[iDoor] -= 1;
									g_iItem[Client][iItem] -= 1;
									SaveItem(Client, iItem, g_iItem[Client][iItem]);
									CPrintToChat(Client, "%s You broke one lock from the Door", CMDTAG);
									decl String:sQuery[255];
									Format(sQuery, sizeof(sQuery), "UPDATE `Doors` SET `Locks` = %i WHERE `DoorID` = %i", g_iLocks[iDoor], iDoor);
									SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
								}	
								case 4: //Break 5
								{
									g_iLocks[iDoor] -= 5;
									g_iItem[Client][iItem] -= 5;
									SaveItem(Client, iItem, g_iItem[Client][iItem]);
									CPrintToChat(Client, "%s You add 5 locks to the Door", CMDTAG);
									decl String:sQuery[255];
									Format(sQuery, sizeof(sQuery), "UPDATE `Doors` SET `Locks` = %i WHERE `DoorID` = %i", g_iLocks[iDoor], iDoor);
									SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
								}
								case 5: //Break 10
								{
									g_iLocks[iDoor] -= 10;
									g_iItem[Client][iItem] -= 10;
									SaveItem(Client, iItem, g_iItem[Client][iItem]);
									CPrintToChat(Client, "%s You add 10 locks to the Door", CMDTAG);
									decl String:sQuery[255];
									Format(sQuery, sizeof(sQuery), "UPDATE `Doors` SET `Locks` = %i WHERE `DoorID` = %i", g_iLocks[iDoor], iDoor);
									SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
								}
								case 6: //Break 25
								{
									g_iLocks[iDoor] -= 25;
									g_iItem[Client][iItem] -= 25;
									SaveItem(Client, iItem, g_iItem[Client][iItem]);
									CPrintToChat(Client, "%s You broke 25 locks from the Door", CMDTAG);
									decl String:sQuery[255];
									Format(sQuery, sizeof(sQuery), "UPDATE `Doors` SET `Locks` = %i WHERE `DoorID` = %i", g_iLocks[iDoor], iDoor);
									SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
								}
								case 7: //Break 100
								{
									g_iLocks[iDoor] -= 100;
									g_iItem[Client][iItem] -= 100;
									SaveItem(Client, iItem, g_iItem[Client][iItem]);
									CPrintToChat(Client, "%s You broke 100 locks from the Door", CMDTAG);
									decl String:sQuery[255];
									Format(sQuery, sizeof(sQuery), "UPDATE `Doors` SET `Locks` = %i WHERE `DoorID` = %i", g_iLocks[iDoor], iDoor);
									SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
								}
							}	
						}
					}	
				}
				
				case 13: //GPS Items
				{
					new iTarget = GetClientAimTarget(Client, false);
					new iVar = StringToInt(g_sItemVar[iItem]);
					if(IsValidEntity(iTarget) && iTarget > 0)
					{
						if(IsClientConnected(iTarget) && IsClientInGame(iTarget))
						{
							switch(iVar)
							{
								case 1:
								{
									g_iGPSTo[Client][iTarget] = 1;
									CPrintToChat(Client, "%s You put a GPS Tracer on %N", CMDTAG, iTarget);
									g_iItem[Client][iItem] -= 1;
								}

								case 2:
								{
									for(new X = 1; X <= MaxClients; X++)
									{
										if(g_iGPSTo[X][Client] == 1)
										{
											CPrintToChat(Client, "%s Found %N's GPS Tracer", CMDTAG, X);
											g_iGPSTo[X][Client] = 0;
											g_iItem[Client][iItem] -= 1;
										}
									}
								}
							}
						}
					}
				}
				
				case 14, 15: //Police Tracers, Jammers
				{
					decl String:sSplit2[2][32];
					ExplodeString(g_sItemVar[iItem], "-", sSplit2, 2, sizeof(sSplit2[]));
					new iType = StringToInt(sSplit2[0]);
					new iTime = StringToInt(sSplit2[1]);
					switch(iType)
					{
						case 1: //Trace Cops
						{
							CPrintToChat(Client, "%s Initializing Police Tracer for %i minutes", CMDTAG, iTime);
							g_fPoliceScannerTime[Client] = GetGameTime() + 60 * iTime;
							g_iItem[Client][iItem] -= 1; 
						}

						case 2: //Jam Radar
						{
							CPrintToChat(Client, "%s Initializing Police Jammer for %i minutes", CMDTAG, iTime);
							g_fPoliceJammerTime[Client] = GetGameTime() + 60 * iTime;
							g_iItem[Client][iItem] -= 1;
						}
					}
				}
				
				case 16: //Player Models
				{
					decl String:sSplit3[2][64];
					ExplodeString(g_sItemVar[iItem], "-", sSplit3, 2, sizeof(sSplit3[])); //Model-Name: "154^Eli Model^22^models/eli.mdl-Eli^10000" ([RP] You have become Eli)
					if(bIsCombine(Client))
					{
						CPrintToChat(Client, "%s You can't change your model as a cop", CMDTAG);
					}

					SetEntityModel(Client, sSplit3[0]);
					CPrintToChat(Client, "%s You have become %s", CMDTAG, sSplit3[1]);
				}
				
				case 17: //Trails
				{
					if(IsClientConnected(Client) && IsClientInGame(Client))
					{
						if(g_iTrails[Client] == 5)
						{
							CPrintToChat(Client, "%s You have too many trails", CMDTAG);
						}
						else
						{
							decl String:sSplit4[2][32];
							ExplodeString(g_sItemVar[iItem], "-", sSplit4, 2, sizeof(sSplit4[]));
							sSplit4[1] = "";
							new iLen = strlen(sSplit4[1]);
							if(g_iTrails[Client] == 10) g_iTrails[Client] = 0;
							g_iTrails[Client] += 1;
							new String:sName[16];
							Format(sName, sizeof(sName), "target%i", Client);
							DispatchKeyValue(Client, "targetname", sName);
							
							new iTrail = CreateEntityByName("env_spritetrail");
							DispatchKeyValue(iTrail, "parentname", sName);
							SetVariantString(sName);
							DispatchKeyValue(iTrail, "renderamt", "255");
							DispatchKeyValue(iTrail, "rendercolor", g_sItemVar[iItem]);
							DispatchKeyValue(iTrail, "rendermode", "1");
							(iLen <= 0) ? DispatchKeyValue(iTrail, "spritename", "materials/sprites/laserbeam.vmt") : DispatchKeyValue(iTrail, "spritename", sSplit4[1]); //Allows for custom trails
							DispatchKeyValue(iTrail, "lifetime", "1.5");
							DispatchKeyValue(iTrail, "startwidth", "7");
							DispatchKeyValue(iTrail, "endwidth", "0.1");
							DispatchSpawn(iTrail);
							
							decl Float:fOrigin[3];
							GetClientAbsOrigin(Client, fOrigin);
							if(g_iTrails[Client] == 1) fOrigin[2] += 10.0;
							if(g_iTrails[Client] == 2) fOrigin[2] += 20.0;
							if(g_iTrails[Client] == 3) fOrigin[2] += 30.0;
							if(g_iTrails[Client] == 4) fOrigin[2] += 40.0;
							if(g_iTrails[Client] == 5) fOrigin[2] += 50.0;
							TeleportEntity(iTrail, fOrigin, NULL_VECTOR, NULL_VECTOR);
							AcceptEntityInput(iTrail, "SetParent", iTrail, iTrail);
							g_iItem[Client][iItem] -= 1;
							CPrintToChat(Client, "%s %s is now enabled. [%i/5 Active] (Trail will delete if you disconnect)", CMDTAG, g_sItemName[iItem], g_iTrails[Client]);
						}
					}
				}
			}
		}

		if(iData == 2)
		{
			new Handle:hDrop = CreateMenu(DropHandle);
			SetMenuTitle(hDrop, "Dropping Item: %s", g_sItemName[iItem]);
			decl String:sFormat[5][16];

			Format(sFormat[0], sizeof(sFormat[]), "%i-1", iItem);
			Format(sFormat[1], sizeof(sFormat[]), "%i-5", iItem);
			Format(sFormat[2], sizeof(sFormat[]), "%i-10", iItem);
			Format(sFormat[3], sizeof(sFormat[]), "%i-25", iItem);
			Format(sFormat[4], sizeof(sFormat[]), "%i-100", iItem);

			AddMenuItem(hDrop, sFormat[0], "1");
			AddMenuItem(hDrop, sFormat[1], "5");
			AddMenuItem(hDrop, sFormat[2], "10");
			AddMenuItem(hDrop, sFormat[3], "25");
			AddMenuItem(hDrop, sFormat[4], "100");

			SetMenuPagination(hDrop, 7);
			DisplayMenu(hDrop, Client, 30);
		}
	}
	return 0;
}

public DropHandle(Handle:hDrop, MenuAction:action, Client, param2)
{
	decl String:sData[16], String:sDrop[2][16];
	GetMenuItem(hDrop, param2, sData, sizeof(sData));
	ExplodeString(sData, "-", sDrop, 2, 16);
	new iItem = StringToInt(sDrop[0]);
	new iAmount = StringToInt(sDrop[1]);
	decl Float:fPos[3];
	new Float:fAngles[3] = {0.0, 0.0, 0.0};

	if(g_iItem[Client][iItem] >= iAmount)
	{
		new iEnt = CreateEntityByName("prop_physics_override");

		DispatchKeyValue(iEnt, "model", "models/Items/BoxMRounds.mdl");
		DispatchSpawn(iEnt);
		
		fAngles[1] = GetRandomFloat(0.0, 360.0);
		fPos[2] += 10.0;

		GetClientAbsOrigin(Client, fPos);

		new iCollision = GetEntSendPropOffs(iEnt, "m_CollisionGroup");
		if(IsValidEntity(iEnt)) SetEntData(iEnt, iCollision, 1, 1, true);
		TeleportEntity(iEnt, fPos, fAngles, NULL_VECTOR);

		CPrintToChat(Client, "%s You drop %i of %s", CMDTAG, iAmount, g_sItemName[iItem]);
		g_iItem[Client][iItem] -= iAmount;
		g_iItemAmount[iEnt][iItem] = iAmount;
		SaveItem(Client, iItem, g_iItem[Client][iItem]);
		Save(Client);
	}
}

public Action:OrganizeList(Client, iData)
{
	new Handle:hItemMenu = CreateMenu(InventoryMenu);
	new iNumber = 0;
	new iValue = 0;
	decl String:sLine[64];
	decl String:sItem[12];
	for(new X = 0; X < MAXITEMS; X++)
	{
		if(g_iItem[Client][X] > 0 && g_iItemAction[X] == FloatAbs(float(iData)))
		{
			Format(sLine, 64, "[%i] x %s", g_iItem[Client][X], g_sItemName[X]);
			Format(sItem, 12, "%i", X);
			AddMenuItem(hItemMenu, sItem, sLine);
			iNumber += g_iItem[Client][X];
			iValue += g_iItem[Client][X] * g_iItemCost[X];
		}
	}
	
	AddMenuItem(hItemMenu, "99999", "[Back to Inventory]");
	SetMenuTitle(hItemMenu, "Item Inventory:\n=============\nTotal Items: %i\nTotal Value: $%i", iNumber, iValue);
	
	SetMenuPagination(hItemMenu, 7);
	DisplayMenu(hItemMenu, Client, 50);
}

public Vendor(Client, iVendor)
{
	decl String:sData[255], String:sVendor[255];

	g_iUsingVendor[Client] = iVendor;
	
	IntToString(iVendor, sVendor, 255);
	new Handle:hVault = CreateKeyValues("Vault");
	
	FileToKeyValues(hVault, g_sNPCPath);
	
	//Load:
	LoadString(hVault, "Vendor_Items", sVendor, "NaN", sData);
	
	//Found in DB:
	if(!StrEqual(sData, "NaN", false))
	{
		
		//Declare:
		new iInfo[25], iPrice;
		new String:sStore[25][32];
		
		//Explode:
		ExplodeString(sData, " ", sStore, 25, 32); //What this does is takes the string, and breaks it into 25 pieces, so that we can store 25 values in it. It's complicated for a while.
		new Handle:hVendor = CreateMenu(VendorHandle);
		SetMenuTitle(hVendor, "Vendor:\n");
		decl String:sDisplay[64], String:sVendorData[64];

		
		for(new X = 0; X < 25; X++)
		{			
			Format(sVendorData, 64, "%s-%s", sStore[X], sVendor); //Keep the IDs Unique
			iInfo[X] = StringToInt(sStore[X]);	
			iPrice = g_iItemCost[iInfo[X]];	
			if(strlen(g_sItemName[iInfo[X]]) > 0)
			{
				Format(sDisplay, sizeof(sDisplay), "%s - $%i", g_sItemName[iInfo[X]], iPrice);
				AddMenuItem(hVendor, sVendorData, sDisplay);
			}
		}

		SetMenuPagination(hVendor, 7);
		DisplayMenu(hVendor, Client, 30);
		
		CPrintToChat(Client, "%s Press <ESC> to view the store", CMDTAG);
	}
	CloseHandle(hVault);
}

public VendorHandle(Handle:hVendor, MenuAction:action, Client, param2)
{
	if (action == MenuAction_Select)
	{
		new String:sData[64];
		GetMenuItem(hVendor, param2, sData, sizeof(sData));
		decl String:InfoType1[64][2];
		Format(InfoType1[0], 64, "Cash-%s", sData);
		Format(InfoType1[1], 64, "Bulk-%s", sData);
		Format(InfoType1[2], 64, "Back-%s", sData);
		
		new Handle:hPay = CreateMenu(PayVendor);
		SetMenuTitle(hPay, "Payment:");
		AddMenuItem(hPay, InfoType1[0], "[Cash]");
		AddMenuItem(hPay, InfoType1[1], "[Buy Bulk]");
		AddMenuItem(hPay, InfoType1[2], "[Back]");
		SetMenuPagination(hPay, 7);
		DisplayMenu(hPay, Client, 30);
	}
}

public PayVendor(Handle:hPay, MenuAction:action, Client, param2)
{
	if(action == MenuAction_Select)
	{
		new String:sData[64];
		GetMenuItem(hPay, param2, sData, sizeof(sData));
		
		if(!StrContains(sData, "Cash", false))
		{
			decl String:sBuffer[3][255];
			ExplodeString(sData, "-", sBuffer, 3, 32);			
			new iItem = StringToInt(sBuffer[1]);	
			new iVendor = StringToInt(sBuffer[2]);
			
			if(g_iCash[Client] >= g_iItemCost[iItem])
			{
				g_iCash[Client] -= g_iItemCost[iItem];
				g_iItem[Client][iItem] += 1;
				SaveItem(Client, iItem, g_iItem[Client][iItem]);
				if(StrHasVowel(g_sItemName[iItem]))
				{
					CPrintToChat(Client, "%s You purchase an %s for $%i with cash", CMDTAG, g_sItemName[iItem], g_iItemCost[iItem]);
				}
				else
				{
					CPrintToChat(Client, "%s You purchase a %s for $%i with cash", CMDTAG, g_sItemName[iItem], g_iItemCost[iItem]);
				}
				Save(Client);
				Vendor(Client, iVendor);
				EmitSoundToClient(Client, "HL1/fvox/fuzz.wav", SOUND_FROM_PLAYER, 1);
			}
			else
			{
				CPrintToChat(Client, "%s You can't afford this item ($%i)", CMDTAG, g_iItemCost[iItem]);
				EmitSoundToClient(Client, "HL1/fvox/beep.wav", SOUND_FROM_PLAYER, 1);
			}
		}
		
		if(!StrContains(sData, "Bulk", false))
		{
			decl String:sBuffer2[3][32];
			ExplodeString(sData, "-", sBuffer2, 3, 32);
			new iItem = StringToInt(sBuffer2[1]);
			new iVendor = StringToInt(sBuffer2[2]);
			
			decl String:s2[64], String:s5[64], String:s10[64], String:s25[64], String:sTitle[64];
			decl String:s2S[64], String:s5S[64], String:s10S[64], String:s25S[64];
			
			Format(sTitle, 64, "Bulk [%s]", g_sItemName[iItem]);
			Format(s2, 64, "2-%i-%i", iItem, iVendor);
			Format(s5, 64, "5-%i-%i", iItem, iVendor);
			Format(s10, 64, "10-%i-%i", iItem, iVendor);
			Format(s25, 64, "25-%i-%i", iItem, iVendor);
			
			Format(s2S, 64, "2x - $%i", (g_iItemCost[iItem] * 2));
			Format(s5S, 64, "5x - $%i", (g_iItemCost[iItem] * 5));
			Format(s10S, 64, "10x - $%i", (g_iItemCost[iItem] * 10));
			Format(s25S, 64, "25x - $%i", (g_iItemCost[iItem] * 25));
			
			new Handle:hBulk = CreateMenu(Bulk);
			SetMenuTitle(hBulk, sTitle);
			if(g_iItemCost[iItem] * 2 > 0) AddMenuItem(hBulk, s2, s2S);
			if(g_iItemCost[iItem] * 5 > 0) AddMenuItem(hBulk, s5, s5S);
			if(g_iItemCost[iItem] * 10 > 0) AddMenuItem(hBulk, s10, s10S);
			if(g_iItemCost[iItem] * 25 > 0) AddMenuItem(hBulk, s25, s25S);
			SetMenuPagination(hBulk, 7);
			DisplayMenu(hBulk, Client, 30);
		}
		
		if(!StrContains(sData, "Back", false))
		{
			decl String:sCheckVen[3][32];
			ExplodeString(sData, "-", sCheckVen, 3, 32);
			new iVendor = StringToInt(sCheckVen[2]);
			Vendor(Client, iVendor);
		}
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(hPay);
	}
	return 0;
}

public Bulk(Handle:hBulk, MenuAction:action, Client, param2)
{
	if(action == MenuAction_Select)
	{
		new String:sData[64];
		GetMenuItem(hBulk, param2, sData, sizeof(sData));
		
		decl String:sBulk[3][32];
		ExplodeString(sData, "-", sBulk, 3, 32);
		new iAmount = StringToInt(sBulk[0]);
		new iItem = StringToInt(sBulk[1]);
		new iVendor = StringToInt(sBulk[2]);
		
		if(g_iCash[Client] >= g_iItemCost[iItem] * iAmount)
		{
			g_iCash[Client] -= g_iItemCost[iItem] * iAmount;
			g_iItem[Client][iItem] += iAmount;
			SaveItem(Client, iItem, g_iItem[Client][iItem]);
			CPrintToChat(Client, "%s You purchase %i of %s for $%i", CMDTAG, iAmount, g_sItemName[iItem], g_iItemCost[iItem]*iAmount);
			Save(Client);
			Vendor(Client, iVendor);
			EmitSoundToClient(Client, "HL1/fvox/fuzz.wav", SOUND_FROM_PLAYER, 1);
		}
		else
		{
			CPrintToChat(Client, "%s You can't afford this item ($%i)", CMDTAG, g_iItemCost[iItem]*iAmount);
			EmitSoundToClient(Client, "HL1/fvox/beep.wav", SOUND_FROM_PLAYER, 1);
		}
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(hBulk);
	}
	return 0;
}

public Action:Bank(Client)
{
	new iPromotion = RoundToCeil(Pow(float(g_iIncome[Client]), 3.0)) - g_iMinutes[Client];
	
	new Handle:hBank = CreateMenu(BankHandle);
	SetMenuTitle(hBank, "Cash: $%i\nBank Account: $%i\nIncome: $%i\nNext Promotion: %i minutes", g_iCash[Client], g_iBank[Client], g_iIncome[Client], iPromotion);
	
	AddMenuItem(hBank, "1", "[Withdraw]");
	AddMenuItem(hBank, "2", "[Deposit]");
	AddMenuItem(hBank, "3", "[Hiscores]");
	SetMenuPagination(hBank, 7);
	DisplayMenu(hBank, Client, 20);
}

public BankHandle(Handle:hBank, MenuAction:action, Client, param2)
{
	if(action == MenuAction_Select)
	{
		new String:sData[64];
		GetMenuItem(hBank, param2, sData, sizeof(sData));
		new iData = StringToInt(sData);	
		switch(iData)
		{
			case 1:
			{
				WithdrawHandle(Client);
			}
			
			case 2:
			{
				DepositHandle(Client);
			}
			
			case 3:
			{
				Hiscores(Client);
			}
		}
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(hBank);
	}
	return;
}

public Action:Hiscores(Client)
{
	new Handle:hTop = CreateMenu(HiscoresHandle);
	SetMenuTitle(hTop, "Hiscores:");
	AddMenuItem(hTop, "1", "[Wealthiest Players]");
	AddMenuItem(hTop, "2", "[Most Wanted]");
	AddMenuItem(hTop, "3", "[Overall Top 10]");
	AddMenuItem(hTop, "-1", "[Back to Bank]");
	SetMenuPagination(hTop, 7);
	DisplayMenu(hTop, Client, 30);
}

public HiscoresHandle(Handle:hTop, MenuAction:action, Client, param2)
{
	decl String:sData[32];
	GetMenuItem(hTop, param2, sData, sizeof(sData));
	new iData = StringToInt(sData);
	
	if(action == MenuAction_Select)
	{
		switch(iData)
		{
			case 1: //Wealthiest
			{
				decl String:sQuery[128];
				Format(sQuery, sizeof(sQuery), "SELECT SteamID, Bank, Cash FROM `Players` ORDER BY Bank + Cash DESC LIMIT 25;");
				new iUser = GetClientUserId(Client);
				SQL_TQuery(g_hDB, SQL_RichestPlayers, sQuery, iUser);
			}
			
			case 2: //Most Wanted
			{
				decl String:sQuery[128];
				Format(sQuery, sizeof(sQuery), "SELECT SteamID, Felony FROM `Players` ORDER BY Felony DESC LIMIT 25;");
				new iUser = GetClientUserId(Client);
				SQL_TQuery(g_hDB, SQL_MostWanted, sQuery, iUser);
			}
			
			case 3: //Overall
			{
				decl String:sQuery[128];
				Format(sQuery, sizeof(sQuery), "SELECT SteamID, Bank, Cash, Felony, Income FROM `Players` ORDER BY Bank + Cash + Felony + Income DESC LIMIT 10;");
				new iUser = GetClientUserId(Client);
				SQL_TQuery(g_hDB, SQL_Overall, sQuery, iUser);
			}
		}
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(hTop);
	}
	
	return;
}	

public Action:WithdrawHandle(Client)
{
	decl String:sAll[64], String:sBank[24], String:sDollar[24];
	decl String:sBuffers[11][10] = {"0", "1", "5", "25", "100", "500", "1000", "5000", "10000", "50000", "100000"};
	new Handle:hWithdraw = CreateMenu(WithdrawMain);
	SetMenuTitle(hWithdraw, "Withdrawing\nCash: $%i\nBank Account: $%i", g_iCash[Client], g_iBank[Client]);
	
	for(new X = 0; X < 11; X++)
	{
		if(StringToInt(sBuffers[X]) == 0) //All
		{
			Format(sAll, 64, "All ($%i)", g_iBank[Client]);
			IntToString(g_iBank[Client], sBank, 24);
			AddMenuItem(hWithdraw, sBank, sAll);
		}
		else //Value
		{
			Format(sDollar, sizeof(sDollar), "$%s", sBuffers[X]); //Putting a Dollar sign in front
			AddMenuItem(hWithdraw, sBuffers[X], sDollar);
		}
	}
	AddMenuItem(hWithdraw, "-1", "[Back to Bank]");
	SetMenuPagination(hWithdraw, 7);
	DisplayMenu(hWithdraw, Client, 30);
}

public Action:DepositHandle(Client)
{
	decl String:sAll[64], String:sCash[24], String:sDollar[24];
	decl String:sBuffers[11][10] = {"0", "1", "5", "25", "100", "500", "1000", "5000", "10000", "50000", "100000"};
	new Handle:hDeposit = CreateMenu(DepositMain);
	SetMenuTitle(hDeposit, "Depositing\nCash: $%i\nBank Account: $%i", g_iCash[Client], g_iBank[Client]);
	
	for(new X = 0; X < 11
	; X++)
	{
		if(StringToInt(sBuffers[X]) == 0)
		{
			Format(sAll, 64, "All ($%i)", g_iCash[Client]);
			IntToString(g_iCash[Client], sCash, 24);
			AddMenuItem(hDeposit, sCash, sAll);
		}
		else
		{
			Format(sDollar, sizeof(sDollar), "$%s", sBuffers[X]);
			AddMenuItem(hDeposit, sBuffers[X], sDollar);
		}
	}
	AddMenuItem(hDeposit, "-1", "[Back to Bank Menu]");
	SetMenuPagination(hDeposit, 7);
	DisplayMenu(hDeposit, Client, 30);
}

public WithdrawMain(Handle:hWithdraw, MenuAction:action, Client, param2)
{
	if(action == MenuAction_Select)
	{
		new String:sData[64];
		GetMenuItem(hWithdraw, param2, sData, sizeof(sData));
		new iData = StringToInt(sData);

		if(iData == -1)
		{
			Bank(Client);
		}
		else if(iData <= g_iBank[Client])
		{
			g_iBank[Client] -= iData;
			g_iCash[Client] += iData;
			Save(Client);
			CPrintToChat(Client, "%s You withdraw $%i", CMDTAG, iData);
			WithdrawHandle(Client);
			
		}
		else
		{
			CPrintToChat(Client, "%s You do not have $%i in the bank", CMDTAG, iData);
		}
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(hWithdraw);
	}
	return;
}

public DepositMain(Handle:hDeposit, MenuAction:action, Client, param2) 
{
	if(action == MenuAction_Select)
	{
		new String:sData[64];
		GetMenuItem(hDeposit, param2, sData, sizeof(sData));
		new iData = StringToInt(sData);
		
		if(iData == -1)
		{
			Bank(Client);
		}
		else if(iData <= g_iCash[Client])
		{
			g_iBank[Client] += iData;
			g_iCash[Client] -= iData;
			Save(Client);
			CPrintToChat(Client, "%s You deposit $%i", CMDTAG, iData);
			DepositHandle(Client);
		}
		else
		{
			CPrintToChat(Client, "%s You do not have $%i", CMDTAG, iData);
		}
	}
	else if(action == MenuAction_End)
	{
		CloseHandle(hDeposit);
	}
	return;
}

public Action:OnTakeDamage(iVictim, &iAttacker, &iInflictor, &Float:fDamage, &iDamageType)
{
	decl String:sWeapon[16];
	GetEdictClassname(iInflictor, sWeapon, sizeof(sWeapon));
	
	if(bIsCombine(iAttacker))
	{
		if(StrEqual(sWeapon, "weapon_stunstick", false))
		{
			fDamage = 0.0;
			
			if(g_iFelonyLevel[iVictim] >= GetConVarInt(g_hCV_Cuffable) && !g_bCuffed[iVictim])
			{
				Cuff(iVictim, iAttacker);
			}
			else if(g_bCuffed[iVictim])
			{
				Uncuff(iVictim, iAttacker);
			}
		}
	}
}

public Event_playerSpawn(Handle:hEvent, const String:sName[], bool:bBroadcast)
{
	new Client = GetClientOfUserId(GetEventInt(hEvent, "userid"));
	if(IsPlayerAlive(Client))
	{
		if(g_bCuffed[Client])
		{
			CreateTimer(1.0, ProgressJail, Client, TIMER_REPEAT);
			sendToJail(Client);
		}
	}
	CreateTimer(0.2, SetupSpawn, Client);
}

public Action:Event_playerDeath(Handle:hEvent, const String:sName[], bool:bBroadcast)
{
	new Client = GetClientOfUserId(GetEventInt(hEvent, "userid")); //Dead Guy
	new iAttacker = GetClientOfUserId(GetEventInt(hEvent, "attacker")); //Alive Guy

	if(iAttacker != Client)
	{
		if(bIsCombine(Client) && !bIsCombine(iAttacker))
		{
			g_iFelonyLevel[iAttacker] += 445;
		}

		if(!bIsCombine(Client) && !bIsCombine(iAttacker))
		{
			g_iFelonyLevel[iAttacker] += 400;
			dropMoney(Client, g_iCash[Client]);
			g_iNotoriety[iAttacker] += 5;
			g_iPrestige[iAttacker] -= 5;
		}
	}

	if(g_iBounty[Client] > 0)
	{
		SetEntityRenderMode(Client, RENDER_GLOW);
		SetEntityRenderColor(Client, 255, 0, 0, 180);
		if(Client != iAttacker)
		{
			if(g_iBounty[Client] > 0)
			{
				g_iCash[iAttacker] += RoundToCeil(g_iBounty[Client] * 1.6);
			}
		}

		g_iJailTime[Client] = g_iFelonyLevel[Client] / 60 * 2;
		g_iJailProgress[Client] = g_iJailTime[Client];
		CreateTimer(1.0, ProgressJail, Client, TIMER_REPEAT);
		g_iFelonyLevel[Client] = 0;
		g_iBounty[Client] = 0;
		g_bCuffed[Client] = true;
	}
}

public Action:RemoveOverlay(Handle:hTimer, any:Client)
{
	ClientCommand(Client, "r_screenoverlay 0");
}

public Action:DrugFade(Handle:hTimer, any:Client)
{
	if(IsClientInGame(Client))
	{
		new SendClient[2];
		SendClient[0] = Client;

		new Handle:hViewMessage = StartMessageEx(g_iFadeID, SendClient, 1);
		BfWriteShort(hViewMessage, 255);
		BfWriteShort(hViewMessage, 255);
		BfWriteShort(hViewMessage, (0x0002));
		BfWriteByte(hViewMessage, GetRandomInt(0, 255));
		BfWriteByte(hViewMessage, GetRandomInt(0, 255));
		BfWriteByte(hViewMessage, GetRandomInt(0, 255));
		BfWriteByte(hViewMessage, 128);
		
		EndMessage();
	}
}

public Action:RemoveDrugEffect(Handle:hTimer, any:Client)
{
	g_bOnDrugs[Client] = false;
	ClientCommand(Client, "r_screenoverlay 0"); 
	SetSpeed(Client, 190.0);
	SetEntityGravity(Client, 1.0);
	SetEntityHealth(Client, 100);
}

public Action:CreateItemsTables(Handle:hTimer)
{
	new len = 0;
	decl String:sQuery[25000];

	len += Format(sQuery[len], sizeof(sQuery)-len, "CREATE TABLE IF NOT EXISTS `Items`");
	len += Format(sQuery[len], sizeof(sQuery)-len, " (`SteamID` VARCHAR(64), ");

	for(new i = 1; i <= MAXITEMS; i++)
	{
		len += Format(sQuery[len], sizeof(sQuery)-len,  "`%i` INT(10) NOT NULL DEFAULT 0, ", i);
	}

	len += Format(sQuery[len], sizeof(sQuery)-len, "PRIMARY KEY (`SteamID`));");
	SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
	PrintToServer("BlueRP Created Items Tables");
}

public Action:CreatePlayerTables(Handle:hTimer)
{
	decl String:sQuery[512];
	Format(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS `Players` (`SteamID` VARCHAR(32) PRIMARY KEY, Cash INT(9) NOT NULL DEFAULT 0, Bank INT(9) NOT NULL DEFAULT 3000, Income INT(9) NOT NULL DEFAULT 7, Felony INT(9) NOT NULL DEFAULT 0, Cuffed INT(2) NOT NULL DEFAULT 0, JailTime INT(9) NOT NULL DEFAULT 0, JailProgress INT(9) NOT NULL DEFAULT 0, Minutes INT(9) NOT NULL DEFAULT 0, Employment TEXT(32) NOT NULL DEFAULT 'Unemployed');");
	SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
	PrintToServer("BlueRP Created Player Tables");
}

public Action:CreateDoorsTables(Handle:hTimer)
{
	decl String:sQuery[256];
	Format(sQuery, sizeof(sQuery), "CREATE TABLE IF NOT EXISTS Doors (DoorID INT(6) PRIMARY KEY, `Owner` VARCHAR(32) NOT NULL DEFAULT 'NaN', Locks INT(9) NOT NULL DEFAULT 0, `Locked` INT(2) NOT NULL DEFAULT 0, `Buyable` INT(2) NOT NULL DEFAULT 0, `Price` INT(9) NOT NULL DEFAULT 0);");
	SQL_TQuery(g_hDB, SQL_GenericQuery, sQuery);
	PrintToServer("BlueRP Created Doors Tables");
}
	
public Action:SpawnNPCS(Handle:hTimer, any:iVal)
{
	decl String:sData[255],String:sNotice[255];
	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sNPCPath);
	for(new X = 0; X < MAXNPCS; X++) //Loop to get EVERY NPC
	{
		decl String:sNPC[255];
		IntToString(X, sNPC, 255);
		
		for(new Y = 0; Y < 3; Y++) //3 Different NPC Types
		{
			decl String:sType[32];
			IntToString(Y, sType, 32);
			LoadString(hVault, sType, sNPC, "NaN", sData);
			LoadString(hVault, "Notice", sNPC, "NaN", sNotice);
			if(StrContains(sData, "NaN", false) == -1)
			{
				decl Float:fOrigin[3];
				decl String:sClassName[32], String:sPrecache[64], String:sAngles[32];
				new String:sBuffer[6][32];
				
				ExplodeString(sData, " ", sBuffer, 6, 32);
				Format(sAngles, 32, "0 %i 0", StringToInt(sBuffer[4]));
				Format(sClassName, sizeof(sClassName), "npc_%s", sBuffer[0]);
				if(sBuffer[5][0]) Format(sPrecache, 64, "models/%s.mdl", sBuffer[5]);
				else Format(sPrecache, 64, "models/%s.mdl", sBuffer[0]); 
				PrecacheModel(sPrecache, true);
				
				new iNPC = CreateEntityByName(sClassName);
				DispatchKeyValue(iNPC, "angles", sAngles);
				DispatchSpawn(iNPC);
				SetEntProp(iNPC, Prop_Data, "m_takedamage", 0, 1);
				fOrigin[0] = StringToFloat(sBuffer[1]);
				fOrigin[1] = StringToFloat(sBuffer[2]);
				fOrigin[2] = StringToFloat(sBuffer[3]);
				
				if(sBuffer[5][0]) SetEntityModel(iNPC, sPrecache);
				g_iNPCList[X] = iNPC;   
				g_iNPCListInverse[iNPC] = X;
				g_iNPCLiveUpdate[Y][X] = iNPC;
				TeleportEntity(iNPC, fOrigin, NULL_VECTOR, NULL_VECTOR);
			}
		}
	}
	
	CloseHandle(hVault);
}
	
public Action:removeWeapons(Handle:hTimer, any:Client)
{
	decl iMaxGuns;
	new iOffset = FindSendPropOffs("CHL2MP_Player", "m_hMyWeapons");
	if(g_bCuffed[Client]) iMaxGuns = 256;
	else iMaxGuns = 16;
	for(new X = 0; X < iMaxGuns; X = (X + 4))
	{
		new iWeaponId = GetEntDataEnt2(Client, iOffset + X);
		if(iWeaponId > 0)
		{
			//Remove Weapons:
			RemovePlayerItem(Client, iWeaponId);
			RemoveEdict(iWeaponId);
		}
	}
}

public Action:SetupSpawn(Handle:hTimer, any:Client)
{
	if(IsClientConnected(Client) && IsClientInGame(Client) && IsPlayerAlive(Client))
	{
		decl iMaxGuns;
		new iOffset = FindSendPropOffs("CHL2MP_Player", "m_hMyWeapons");
		if(g_bCuffed[Client]) iMaxGuns = 256;
		else iMaxGuns = 16;
		for(new X = 0; X < iMaxGuns; X = (X + 4))
		{
			new iWeaponId = GetEntDataEnt2(Client, iOffset + X);
			if(iWeaponId > 0)
			{
				//Remove Weapons:
				RemovePlayerItem(Client, iWeaponId);
				RemoveEdict(iWeaponId);
			}
		}
		if(!g_bCuffed[Client])
		{
			if(bIsCombine(Client))
			{
				GivePlayerItem(Client, "weapon_stunstick");
				GivePlayerItem(Client, "weapon_pistol");
			}
			else
			{
				if(g_iNotoriety[Client] > 120 && g_iNotoriety[Client] < 240)
				{
					GivePlayerItem(Client, "weapon_crowbar");
				}
				else if(g_iNotoriety[Client] > 239 && g_iNotoriety[Client] < 480)
				{
					GivePlayerItem(Client, "weapon_crowbar");
					GivePlayerItem(Client, "weapon_stunstick");
				}
				else if(g_iNotoriety[Client] > 479 && g_iNotoriety[Client] < 960)
				{
					GivePlayerItem(Client, "weapon_crowbar");
					GivePlayerItem(Client, "weapon_stunstick");
					GivePlayerItem(Client, "weapon_pistol");
				}

				if(g_iPrestige[Client] > 120 && g_iPrestige[Client] < 240)
				{
					SetEntityHealth(Client, 110);
				}
				else if(g_iPrestige[Client] > 239 && g_iPrestige[Client] < 480)
				{
					SetEntityHealth(Client, 130);
				}
				else if(g_iPrestige[Client] > 479 && g_iPrestige[Client] < 960)
				{
					SetEntityHealth(Client, 160);
				}
			}

			new Handle:hJob = CreateKeyValues("Vault");
			FileToKeyValues(hJob, g_sJobSetupPath);

			decl String:sModel[64];
			new iHealth = LoadInteger(hJob, g_sTempJob[Client], "health", 0);
			new iArmor = LoadInteger(hJob, g_sTempJob[Client], "armor", 0);
			LoadString(hJob, g_sTempJob[Client], "model", "models/humans/Group03/Male_02.mdl", sModel);
			SetEntityModel(Client, sModel);
			if(iHealth > 0) SetEntityHealth(Client, iHealth); else SetEntityHealth(Client, 100);
			if(iArmor > 0) SetEntProp(Client, Prop_Data, "m_ArmorValue", iArmor, 1);

			for(new X = 1; X < 26; X++)
			{
				new iWeapons = LoadInteger(hJob, g_sTempJob[Client], g_sWeapons[X], 0);
				if(iWeapons > 0)
				{
					for(new Y = 1; Y <= iWeapons; Y++)
					{
						GivePlayerItem(Client, g_sWeapons[X]);
					}
				}
			}

			GivePlayerItem(Client, "weapon_physcannon");
		}

		new iTeam = GetClientTeam(Client);
		switch(iTeam)
		{
			case 2:
			{
				if(!bIsCombine(Client))
				{
					ChangeClientTeam(Client, 3);
				}
			}

			case 3:
			{
				if(bIsCombine(Client))
				{
					ChangeClientTeam(Client, 2);
				}
			}
		}
	}
}

public MinuteTimer(Client)
{
	g_iIncomeTimer[Client] -= 1;

	if(g_iMinutes[Client] >= Pow(float(g_iIncome[Client]), 3.0))
	{
		g_iIncome[Client] += 1;		
		CPrintToChat(Client, "%s You have recieved a raise for spending a total of %i minutes in the server", CMDTAG, g_iMinutes[Client]);
	}
	
	if(g_iIncomeTimer[Client] == 1)
	{
		g_iIncomeTimer[Client] = WAGETIMER + 1;
		g_iCash[Client] += g_iIncome[Client];
		g_iMinutes[Client] += 1;
		if(g_iFelonyLevel[Client] > 0)
		{
			g_iFelonyLevel[Client] -= 100;
		}
		Save(Client);
	}
}

public Action:ShowHud(Handle:hTimer, any:Client)
{
	if(IsClientConnected(Client) && IsClientInGame(Client))
	{
		new iCuffLimit = GetConVarInt(g_hCV_Cuffable);
		decl Float:fCriminalOrigin[3], Float:fOrigin[3];
		GetClientAbsOrigin(Client, fOrigin);
		new aCopBeam[4] = {50, 0, 255, 200}; //3300FF 		
		//Bounty Tracer
		new aBountyBeam[4] = {255, 100, 100, 200};
		new aGPSBeam[4] = {100, 255, 100, 200};
		for(new X = 1; X <= MaxClients; X++)
		{
			if(IsClientConnected(X) && IsClientInGame(X))
			{
				if(g_iBounty[X] > 0 && !bIsCombine(Client))
				{
					if(X != Client)
					{
						GetClientAbsOrigin(X, fCriminalOrigin);
						TE_SetupBeamPoints(fOrigin, fCriminalOrigin, g_iLaser, 0, 0, 66, 0.5, 3.0, 3.0, 0, 0.0, aBountyBeam, 0);
						TE_SendToClient(Client);
					}
				}
				
				if(g_iGPSTo[Client][X] == 1)
				{
					if(X != Client)
					{
						GetClientAbsOrigin(X, fCriminalOrigin);
						TE_SetupBeamPoints(fOrigin, fCriminalOrigin, g_iLaser, 0, 0, 66, 0.5, 3.0, 3.0, 0, 0.0, aGPSBeam, 0);
						TE_SendToClient(Client);
					}
				}
				
				if(g_fPoliceScannerTime[Client] >= GetGameTime())
				{
					if(X != Client && bIsCombine(X))
					{
						GetClientAbsOrigin(X, fCriminalOrigin);
						TE_SetupBeamPoints(fOrigin, fCriminalOrigin, g_iLaser, 0, 0, 66, 0.5, 3.0, 3.0, 0, 0.0, aCopBeam, 0);
						TE_SendToClient(Client);
					}
				}
			}
		}    

		SetHudTextParams(-1.0, 0.015, 10.0, 255, 255, 255, 255, 1, 4.0, 0.1, 0.2); 
		for(new X = 1; X <= MaxClients; X++)
		{
			if(IsClientConnected(X) && IsClientInGame(X) && X != Client)
			{ 
				if(g_iBounty[Client] == 0 && g_bKnows[Client])
				{
					ShowHudText(X, -1, "The bounty for %N got revoked", Client, g_iBounty[Client]);
				}
				else
				{
					ShowHudText(X, -1, "%N has now a bounty of $%i on his head", Client, g_iBounty[Client]);
				}
			}
		} 

		if(bIsCombine(Client) || g_iFelonyLevel[Client] > iCuffLimit)
		{
			SetHudTextParams(3.050, -1.50, HUDTIMER, 255, 0, 0, 255, 0, 6.0, 0.1, 0.2);
			new String:sFormat[255] = "Crime:  \n";
			
			for(new X = 1; X <= MaxClients; X ++)
			{
				if(IsClientConnected(X) && IsClientInGame(X))
				{
					if(g_iFelonyLevel[X] > iCuffLimit)
					{
						GetClientAbsOrigin(X, fCriminalOrigin);
						fCriminalOrigin[2] += 40.0;
						
						if(bIsCombine(Client) && g_fPoliceJammerTime[X] < GetGameTime())
						{
							aCopBeam = {50, 0, 255, 200}; //3300FF Blue
							if(g_iFelonyLevel[X] < 100)
							{
								aCopBeam[2] = g_iFelonyLevel[X]; 
								aCopBeam[0] = g_iFelonyLevel[X] / 4;
								aCopBeam[1] = 100 - g_iFelonyLevel[X];    
							} 
							else
							{
								aCopBeam[1] = g_iFelonyLevel[X] / 10;
								if(aCopBeam[1] > 250) aCopBeam[1] = 250;
							}

							TE_SetupBeamPoints(fOrigin, fCriminalOrigin, g_iLaser, 0, 0, 66, 0.5, 3.0, 3.0, 0, 0.0, aCopBeam, 0);
							TE_SendToClient(Client);
						} 
					}
					if(g_iFelonyLevel[X] > GetConVarInt(g_hCV_CrimeMenuAmount))
					{
						decl String:sTempSave[255];	
						sTempSave = sFormat;
						
						Format(sFormat, sizeof(sFormat), "%s %N (%i)\n", sTempSave, X, g_iFelonyLevel[X]);
					}
				}
			}
			
			ShowHudText(Client, -1, "%s ", sFormat);  
		}

		if(g_iFelonyLevel[Client] >= GetConVarInt(g_hCV_CrimeForBounty)) g_iBounty[Client] = RoundToFloor(g_iFelonyLevel[Client] / 15.5);
		if(g_iPrestige[Client] < 0) g_iPrestige[Client] = 0;
		if(g_iNotoriety[Client] < 0) g_iNotoriety[Client] = 0;
		if(g_bOnDrugs[Client]) SetSpeed(Client, g_fSpeed[Client]);

		if(!IsPlayerAlive(Client))
		{
			CreateTimer(HUDTIMER, ShowHud, Client);
			return Plugin_Handled;
		}

		new iEnt = GetClientAimTarget(Client, false);
	
		if(iEnt > 0 && IsValidEntity(iEnt))
		{
			if(GetEntityDistance(Client, iEnt) <= 400)
			{
				if(!StrEqual(g_sNotice[iEnt], "NaN", false))
				{
					SetHudTextParams(-1.0, 0.555, HUDTIMER, 200, 150, 0, 255, 0, 6.0, 0.1, 0.2);
					ShowHudText(Client, -1, "%s", g_sNotice[iEnt]);
				}
			}
		}

		MinuteTimer(Client);
		decl String:sFormatHud[10000];
		decl String:sExtraInfo[255];
		new len = 0;
		new Raise = RoundToCeil(Pow(float(g_iIncome[Client]), 3.0));
		new iMinutes = g_iMinutes[Client];
		new iDays = iMinutes/1440;
		new iHours = (iMinutes/60) - (iDays*24);
		new iMinutes2 = iMinutes - (iDays*1440) - (iHours*60);
		new iServerMinutes = g_iUptime;
		new iDays2 = iServerMinutes/1440;
		new iHours2 = (iServerMinutes/60) - (iDays2*24);
		new iServerMinutes2 = iServerMinutes - (iDays2*1440) - (iHours2*60);

		len += Format(sFormatHud[len], sizeof(sFormatHud)-len, "%N\nCash: $%i\nBank: $%i\nIncome: $%i in %i\nNext Promotion: %i Minutes\nEmployment: %s", Client, g_iCash[Client], g_iBank[Client], g_iIncome[Client], g_iIncomeTimer[Client], Raise, g_sTempJob[Client]);
		
		if(g_iFelonyLevel[Client] > 0)
		{
			len += Format(sFormatHud[len], sizeof(sFormatHud)-len, "\nFelony: %i", g_iFelonyLevel[Client]);
		}

		if(g_iBounty[Client] > 0)
		{
			len += Format(sFormatHud[len], sizeof(sFormatHud)-len, "\nBounty: %i", g_iBounty[Client]);
		}

		if(g_bCuffed[Client])
		{
			len += Format(sFormatHud[len], sizeof(sFormatHud)-len, "\n[IN JAIL]\nRelease: %is", g_iJailProgress[Client]);
		}

		SetHudTextParams(0.015, 0.015, HUDTIMER, 87, 179, 255, 255, 0, 6.0, 0.1, 0.2);
		ShowHudText(Client, 0, sFormatHud);
		
		Format(sExtraInfo, sizeof(sExtraInfo), "[Time Played: %id:%ih:%im]\n[Server Uptime: %id:%ih:%im]", iDays, iHours, iMinutes2, iDays2, iHours2, iServerMinutes2);
		SetHudTextParams(-1.0, 0.865, HUDTIMER, 87, 179, 255, 255, 0, 6.0, 0.1, 0.2);
		ShowHudText(Client, 1, sExtraInfo);
		
		CreateTimer(HUDTIMER, ShowHud, Client);
		return Plugin_Handled;
	}
	
	return Plugin_Handled;
}

public Action:Command_setDoorPrice(Client, iArgs)
{
	if(Client == 0) return Plugin_Handled;

	if(iArgs != 1)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_hDoorprice <price>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_hDoorprice <price>", CMDTAG);
		return Plugin_Handled;
	}

	decl String:sArg[32];
	GetCmdArg(1, sArg, sizeof(sArg));
	new iArg = StringToInt(sArg);
	
	new iEnt = GetClientAimTarget(Client, false);
	if(!IsValidEdict(iEnt))
	{
		PrintToConsole(Client, "%s Invalid Door", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Door", CMDTAG);
		return Plugin_Handled;	
	}
	
	decl String:sClassName[64];
	GetEdictClassname(iEnt, sClassName, sizeof(sClassName));
	if(!(StrEqual(sClassName, "func_door") || StrEqual(sClassName, "func_door_rotating") || StrEqual(sClassName, "prop_door_rotating")))
	{
		PrintToConsole(Client, "%s Invalid Door", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Door", CMDTAG);
		return Plugin_Handled;
	}
	
	decl String:sDoor[32];
	IntToString(iEnt, sDoor, sizeof(sDoor));
	
	new Handle:hDoor = CreateKeyValues("Vault");
	FileToKeyValues(hDoor, g_sDoorPath);
	KvJumpToKey(hDoor, sDoor, true);

	new iTest = KvGetNum(hDoor, "Buyable", 1);
	if(iTest != 1 && iTest < 10)
	{
		CPrintToChat(Client, "%s This door already has a price", CMDTAG);
		PrintToConsole(Client, "%s This door already has a price", CONSOLETAG);
		KvRewind(hDoor);
		CloseHandle(hDoor);
		return Plugin_Handled;
	}
	KvSetNum(hDoor, "Buyable", BUYABLE);
	KvSetString(hDoor, "Price", sArg);
	KvRewind(hDoor);
	KeyValuesToFile(hDoor, g_sDoorPath);
	CloseHandle(hDoor);
	
	CPrintToChat(Client, "%s Door #%d is now buyable. Cost set to $%s", CMDTAG, iEnt, sArg);
	
	decl String:sSale[255];
	Format(sSale, sizeof(sSale), "For Sale: $%i", iArg);

	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sNoticePath);
	KvJumpToKey(hKV, "Owner", true);
	KvSetString(hKV, sDoor, sSale);
	KvRewind(hKV);
	KeyValuesToFile(hKV, g_sNoticePath);
	CloseHandle(hKV);
	ApplyMessage(iEnt, sSale);
	return Plugin_Handled;
}

public Action:Command_resetDoor(Client, Args)
{
	if(Client == 0) return Plugin_Handled;
	if(Args > 0)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_resetdoor <NO ARGS>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_resetdoor <NO ARGS>", CMDTAG);
		
		return Plugin_Handled;
	}

	new iEnt = GetClientAimTarget(Client, false);
	if(iEnt <= 1)
	{
		PrintToConsole(Client, "[RP] Invalid Door.");
		return Plugin_Handled;	
	}

	decl String:sDoor[32];
	IntToString(iEnt, sDoor, 32);
	
	new Handle:hDoor = CreateKeyValues("Vault");
	FileToKeyValues(hDoor, g_sDoorPath);
	KvJumpToKey(hDoor, sDoor, false);
	new iBuyable = KvGetNum(hDoor, "Buyable", 99);

	if(iBuyable == 99 || iBuyable == 1)
	{
		CPrintToChat(Client, "%s Found no owner of this door.", CMDTAG);
		KvRewind(hDoor);
		CloseHandle(hDoor);
		return Plugin_Handled;
	}

	KvSetNum(hDoor, "Buyable", BUYABLE);
	decl String:sOwner[32], String:sWorth[32];
	KvGetString(hDoor, "Owner", sOwner, 32, NOBODY);
	KvGetString(hDoor, "Price", sWorth, 32, NOBODY);

	new iWorth = StringToInt(sWorth);
	
	decl String:sSale[255];
	Format(sSale, sizeof(sSale), "For Sale: $%i", iWorth);
	new Handle:hMess = CreateKeyValues("Vault");
	FileToKeyValues(hMess, g_sNoticePath);
	KvJumpToKey(hMess, "Owner", true);
	KvSetString(hMess, sDoor, sSale);
	KvRewind(hMess);
	KeyValuesToFile(hMess, g_sNoticePath);
	CloseHandle(hMess);
	ApplyMessage(iEnt, sSale);
	
	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sDoorPath);
	KvJumpToKey(hKV, sDoor, true);
	KvDeleteKey(hKV, sOwner);
	
	KvSetString(hDoor, "Owner", "12345");
	
	decl String:sSteamID[32];
	for(new X = 1; X <= 50; X++)
	{
		decl String:sKey[10];
		IntToString(X, sKey, sizeof(sKey));
		KvDeleteKey(hDoor, sKey);
		KvGetString(hDoor, sKey, sSteamID, sizeof(sSteamID), NOBODY);
		if(!StrEqual(sSteamID, "Nobody", false))
		{
			KvDeleteKey(hKV, sSteamID);
		}
	}
	
	KvRewind(hKV);
	KeyValuesToFile(hKV, g_sDoorPath);
	CloseHandle(hKV);
	
	KvRewind(hDoor);
	KeyValuesToFile(hDoor, g_sDoorPath);
	CloseHandle(hDoor);
	
	new iDoor = StringToInt(sDoor);

	refreshDoors();
	CPrintToChat(Client, "%s Owner [%s] of door #%i has lost their keys. This door is now back up for sale at: $%i", CMDTAG, sOwner, iDoor, iWorth);
	PrintToConsole(Client, "%s Owner [%s] of door #%i has lost their keys. This door is now back up for sale at: $%i", CONSOLETAG, sOwner, iDoor, iWorth);
	return Plugin_Handled;
}

public Action:Command_deleteDoor(Client, Args)
{
	if(Client == 0) return Plugin_Handled;
	if(Args != 0)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_deletedoor <NO ARGS>");
		CPrintToChat(Client, "%s Invalid Syntax: sm_deletedoor <NO ARGS>");	
		return Plugin_Handled;
	}

	new iEnt = GetClientAimTarget(Client, false);
	if(!IsValidEdict(iEnt))
	{
		PrintToConsole(Client, "%s Invalid Door", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Door", CMDTAG);
		return Plugin_Handled;	
	}
	decl String:sDoor[32];
	IntToString(iEnt, sDoor, 32);
	
	new Handle:hDoor = CreateKeyValues("Vault");
	FileToKeyValues(hDoor, g_sDoorPath);
	KvJumpToKey(hDoor, sDoor, false);
	new iBuyable = KvGetNum(hDoor, "Buyable", 99);
	if(iBuyable == 99)
	{
		CPrintToChat(Client, "%s This door is not buyable or sellable at this moment", CMDTAG);
		PrintToConsole(Client, "%s This door is not buyable or sellable at this moment", CONSOLETAG);
		KvRewind(hDoor);
		CloseHandle(hDoor);
		return Plugin_Handled;
	}

	if(iBuyable != 1)
	{
		decl String:sOwner[32];
		KvGetString(hDoor, "Owner", sOwner, 32, NOBODY);
		new Handle:hKV = CreateKeyValues("Vault");
		FileToKeyValues(hKV, g_sDoorPath);
		KvJumpToKey(hKV, sDoor, true);
		KvDeleteKey(hKV, sOwner);
		
		decl String:sSteamID[32];
		for(new X = 1; X <= 50; X++)
		{
			decl String:sKey[10];
			IntToString(X, sKey, 10);
			KvGetString(hDoor, sKey, sSteamID, 32, NOBODY);
			if(!StrEqual(sSteamID, "Nobody", false))
			{
				KvDeleteKey(hKV, sSteamID);
			}
		}
		KvRewind(hKV);
		KeyValuesToFile(hKV, g_sDoorPath);
		CloseHandle(hKV);
	}

	decl String:sSale[255];
	Format(sSale, 255, " ");
	new Handle:hMess = CreateKeyValues("Vault");
	FileToKeyValues(hMess, g_sNoticePath);
	KvJumpToKey(hMess, "Owner", true);
	KvSetString(hMess, sDoor, sSale);
	KvRewind(hMess);
	KeyValuesToFile(hMess, g_sNoticePath);
	CloseHandle(hMess);
	ApplyMessage(iEnt, sSale);
	
	KvDeleteThis(hDoor);
	KvRewind(hDoor);
	KeyValuesToFile(hDoor, g_sDoorPath);
	CloseHandle(hDoor);
	CPrintToChat(Client, "%s This door has been deleted from the database", CMDTAG);
	PrintToConsole(Client, "%s This door has been deleted from the database", CONSOLETAG);
	
	refreshDoors();
	return Plugin_Handled;
}

public Action:Command_setDoorMessage(Client, Args)
{
	if(Client == 0) return Plugin_Handled;
	if(Args != 1)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_doorname <message>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_doorname <message>", CMDTAG);
		return Plugin_Handled;
	}
	
	decl String:sArg[255];
	
	GetCmdArgString(sArg, sizeof(sArg));
	
	if(StrContains(sArg, "\\", false) != -1)
	{
		CPrintToChat(Client, "%s You cannot put a backslash in a notice", CMDTAG);
		PrintToConsole(Client, "%s You cannot put a backslash in a notice", CONSOLETAG);
		return Plugin_Handled;
	}
	
	new iEnt = GetClientAimTarget(Client, false);
	if(!IsValidEdict(iEnt))
	{
		PrintToConsole(Client, "%s Invalid Door", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Door", CMDTAG);
		return Plugin_Handled;
	}

	decl String:sDoor[255];
	IntToString(iEnt, sDoor, 255);
	
	new Handle:hDoor = CreateKeyValues("Vault");
	FileToKeyValues(hDoor, g_sDoorPath);
	KvJumpToKey(hDoor, sDoor, false);

	new iBuyable = KvGetNum(hDoor, "Buyable", 99);
	if(iBuyable == 99 || iBuyable == 2)
	{
		CPrintToChat(Client, "%s Access Denied", CMDTAG);
		PrintToConsole(Client, "%s Access Denied", CONSOLETAG);
		KvRewind(hDoor);
		CloseHandle(hDoor);
		return Plugin_Handled;
	}

	decl String:sOwner[32];
	KvGetString(hDoor, "Owner", sOwner, sizeof(sOwner), NOBODY);

	if(StrEqual(g_sSteamID[Client], sOwner, false))
	{
		decl String:sSale[255];
		Format(sSale, sizeof(sSale), "%s", sArg);
		new Handle:hMess = CreateKeyValues("Vault");
		FileToKeyValues(hMess, g_sNoticePath);
		KvJumpToKey(hMess, "Owner", true);
		KvSetString(hMess, sDoor, sSale);
		KvRewind(hMess);
		KeyValuesToFile(hMess, g_sNoticePath);
		CloseHandle(hMess);
		ApplyMessage(iEnt, sSale);
		CPrintToChat(Client, "%s Door Message Changed To:", CMDTAG);
		CPrintToChat(Client, "%s %s", CMDTAG, sArg);
		KvRewind(hDoor);
		CloseHandle(hDoor);
		return Plugin_Handled;
	}

	CPrintToChat(Client, "%s Access Denied", CMDTAG);
	PrintToConsole(Client, "%s Access Denied", CONSOLETAG);
	KvRewind(hDoor);
	CloseHandle(hDoor);
	return Plugin_Handled;
}

public Action:Command_giveKeys(Client, iArgs)
{
	if(Client == 0) return Plugin_Handled;
	if(iArgs != 2)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_givekeys <1 - 50> <name>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_givekeys <1 - 50> <name>", CMDTAG);
		return Plugin_Handled;
	}
	decl String:sArgs[2][32];
	GetCmdArg(1, sArgs[0], sizeof(sArgs[]));
	GetCmdArg(2, sArgs[1], sizeof(sArgs[]));
	
	new iVar = StringToInt(sArgs[0]);
	if(iVar > 50 || iVar < 1)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_givekeys <1 - 50> <name>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_givekeys <1 - 50> <name>", CMDTAG);
		return Plugin_Handled;
	}

	new iEnt = GetClientAimTarget(Client, false);
	if(!IsValidEdict(iEnt))
	{
		PrintToConsole(Client, "%s Invalid Door", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Door", CMDTAG);
		return Plugin_Handled;	
	}

	decl String:sDoor[32];
	IntToString(iEnt, sDoor, 32);
	
	new Handle:hDoor = CreateKeyValues("Vault");
	FileToKeyValues(hDoor, g_sDoorPath);
	KvJumpToKey(hDoor, sDoor, false);
	decl String:sOwner[32];
	KvGetString(hDoor, "Owner", sOwner, 32, NOBODY);

	if(!StrEqual(g_sSteamID[Client], sOwner, false))
	{
		CPrintToChat(Client, "%s Access Denied", CMDTAG);
		PrintToConsole(Client, "%s Access Denied", CONSOLETAG);
		KvRewind(hDoor);
		CloseHandle(hDoor);
		return Plugin_Handled;
	}

	decl String:sKeys[32];
	KvGetString(hDoor, sArgs[0], sKeys, 32, NOBODY);
	if(!StrEqual(sKeys, "Nobody", false))
	{
		CPrintToChat(Client, "%s Key #%i has been used. Use a different number or delete the Key (sm_deletekeys)", CMDTAG, iVar);
		PrintToConsole(Client, "%s Key #%i has been used. Use a different number or delete the Key (sm_deletekeys)", CONSOLETAG, iVar);
		KvRewind(hDoor);
		CloseHandle(hDoor);
		return Plugin_Handled;
	}
	
	new iTarget = FindTarget(Client, sArgs[1]);
	if(iTarget == -1)
	{
		PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArgs[1]);
		CPrintToChat(Client, "%s Invalid Target: %s", CONSOLETAG, sArgs[1]);
		KvRewind(hDoor);
		CloseHandle(hDoor);
		return Plugin_Handled;
	}

	decl String:sSteamID[32];
	GetClientAuthString(iTarget, sSteamID, 32);
	if(StrEqual(g_sSteamID[Client], sSteamID, false))
	{
		CPrintToChat(Client, "%s You already have keys", CMDTAG);
		PrintToConsole(Client, "%s You already have keys", CONSOLETAG);
		KvRewind(hDoor);
		CloseHandle(hDoor);
		return Plugin_Handled;
	}

	KvSetString(hDoor, sArgs[0], sSteamID);
	KvRewind(hDoor);
	KeyValuesToFile(hDoor, g_sDoorPath);
	CloseHandle(hDoor);
	
	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sDoorPath);
	KvJumpToKey(hKV, sDoor, true);
	KvSetNum(hKV, sSteamID, 1);
	KvRewind(hKV);
	KeyValuesToFile(hKV, g_sDoorPath);
	CloseHandle(hKV);
	
	refreshDoors();
	CPrintToChat(iTarget, "%s You have been given keys by %N", CMDTAG, Client);
	CPrintToChat(Client, "%s You given keys to %N", CMDTAG, iTarget);	
	return Plugin_Handled;
}

public Action:Command_deleteKeys(Client, iArgs)
{
	if(Client == 0) return Plugin_Handled;

	if(iArgs != 1)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_deletekeys <1 - 50>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_deletekeys <1 - 50>", CMDTAG);
		return Plugin_Handled;
	}
	
	new iEnt = GetClientAimTarget(Client, false);
	if(!IsValidEdict(iEnt))
	{
		PrintToConsole(Client, "%s Invalid Door", CMDTAG);
		CPrintToChat(Client, "%s Invalid Door", CMDTAG);
		return Plugin_Handled;
	}

	decl String:sDoor[32];
	IntToString(iEnt, sDoor, 32);
	
	decl String:sArg1[32];
	GetCmdArg(1, sArg1, sizeof(sArg1));
	
	new iVar = StringToInt(sArg1);
	if(iVar > 50 || iVar < 1)
	{
		CPrintToChat(Client, "%s Invalid Syntax: sm_deletekeys <1 - 50>", CMDTAG);
		PrintToConsole(Client, "%s Invalid Syntax: sm_deletekeys <1 - 50>", CONSOLETAG);
		return Plugin_Handled;
	}
	
	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sDoorPath);
	KvJumpToKey(hKV, sDoor, false);
	decl String:sOwner[32];
	KvGetString(hKV, "Owner", sOwner, 32, NOBODY);

	if(!StrEqual(g_sSteamID[Client], sOwner, false))
	{
		CPrintToChat(Client, "%s Access Denied", CMDTAG);
		PrintToConsole(Client, "%s Access Denied", CONSOLETAG);
		KvRewind(hKV);
		CloseHandle(hKV);
		return Plugin_Handled;
	}
	
	decl String:sSteamID[32];
	KvGetString(hKV, sArg1, sSteamID, 32, NOBODY);
	if(StrEqual(sSteamID, "Nobody", false))
	{
		CPrintToChat(Client, "%s Nobody has keys with that SteamID", CMDTAG);	
		PrintToConsole(Client, "%s Nobody has keys with that SteamID", CONSOLETAG);	
		KvRewind(hKV);
		CloseHandle(hKV);
		return Plugin_Handled;
	}

	KvDeleteKey(hKV, sArg1);
	
	new Handle:hDoor = CreateKeyValues("Vault");
	FileToKeyValues(hDoor, g_sDoorPath);
	KvJumpToKey(hDoor, sDoor, true);
	KvDeleteKey(hDoor, sSteamID);
	KvRewind(hDoor);
	KeyValuesToFile(hDoor, g_sDoorPath);
	CloseHandle(hDoor);
	
	KvRewind(hKV);
	KeyValuesToFile(hKV, g_sDoorPath);
	CloseHandle(hKV);
	
	CPrintToChat(Client, "%s SteamID [%s] has lost keys to this house", CMDTAG, sSteamID);
	refreshDoors();
	return Plugin_Handled;
}

public Action:Command_getClientLocation(Client, iArgs)
{
	decl Float:fClientOrigin[3];
	GetClientAbsOrigin(Client, fClientOrigin); 
	CPrintToChat(Client, "%s Your Coordinates\n%s X:%f\n%s Y:%f\n%s Z:%f", CMDTAG, fClientOrigin[0], CMDTAG, fClientOrigin[1], CMDTAG, fClientOrigin[2]);         
	return Plugin_Handled;
}

public Action:Command_edictInfo(Client, iArgs)
{
    decl String:sModelName[64];
    decl String:sClassName[64];
    new iEnt = GetClientAimTarget(Client, false);
    GetEdictClassname(iEnt, sClassName, sizeof(sClassName));
    GetEntPropString(iEnt, Prop_Data, "m_ModelName", sModelName, sizeof(sClassName));
    CPrintToChat(Client, "%s Model:%s\n%s Classname:%s\n%s Edict Index:%i", CMDTAG, sModelName, CMDTAG, sClassName, CMDTAG, iEnt);         
    return Plugin_Handled;
}

public Action:Command_dupeEdict(Client, iArgs)
{
    decl String:sModelName[128];

    new iEnt = GetClientAimTarget(Client, false);
    if(IsValidEdict(iEnt))
    {
    	GetEntPropString(iEnt, Prop_Data, "m_ModelName", sModelName, sizeof(sModelName));
    	PrecacheModel(sModelName, true);
    }
    else return Plugin_Handled;
    
    new iEnt2 = CreateEntityByName("prop_physics_override");   	 
    DispatchKeyValue(iEnt2, "physdamagescale", "0.0");
    DispatchKeyValue(iEnt2, "model", sModelName);
    DispatchSpawn(iEnt2);
   
    decl Float:fFurnitureOrigin[3];  
    decl Float:fClientOrigin[3];
    decl Float:fEyeAngles[3];

    GetClientEyeAngles(Client, fEyeAngles);
    GetClientAbsOrigin(Client, fClientOrigin); 

    fFurnitureOrigin[0] = (fClientOrigin[0] + (50 * Cosine(DegToRad(fEyeAngles[1]))));
    fFurnitureOrigin[1] = (fClientOrigin[1] + (50 * Sine(DegToRad(fEyeAngles[1]))));
    fFurnitureOrigin[2] = (fClientOrigin[2] + 100);

    TeleportEntity(iEnt2, fFurnitureOrigin, NULL_VECTOR, NULL_VECTOR);
    SetEntityMoveType(iEnt2, MOVETYPE_VPHYSICS);    
    return Plugin_Handled;
}

public Action:Command_throwEdict(Client, iArgs)
{
	if(Client == 0)
	{
		PrintToConsole(Client, "%s You can't use this command in the Server Console", CONSOLETAG);
		return Plugin_Handled;
	}

	if(iArgs != 1)
	{
	    PrintToConsole(Client, "%s Invalid Syntax: sm_throw <model>", CONSOLETAG);
	    CPrintToChat(Client, "%s Invalid Syntax: sm_throw <model>", CMDTAG);
	    return Plugin_Handled;
	}

	decl String:sModelName[64];
	GetCmdArg(1, sModelName, sizeof(sModelName));

	new iEnt = CreateEntityByName("prop_physics_override"); 
	DispatchKeyValue(iEnt, "physdamagescale", "0.0");
	DispatchKeyValue(iEnt, "model", sModelName);
	DispatchSpawn(iEnt);

	decl Float:fFurnitureOrigin[3];  
	decl Float:fClientOrigin[3];
	decl Float:fEyeAngles[3];
	decl Float:fPush[3];

	GetClientEyeAngles(Client, fEyeAngles);
	GetClientAbsOrigin(Client, fClientOrigin); 

	fPush[0] = (5000.0 * Cosine(DegToRad(fEyeAngles[1])));
	fPush[1] = (5000.0 * Sine(DegToRad(fEyeAngles[1])));
	fPush[2] = (-12000.0 * Sine(DegToRad(fEyeAngles[0])));

	fFurnitureOrigin[0] = (fClientOrigin[0] + (50 * Cosine(DegToRad(fEyeAngles[1]))));
	fFurnitureOrigin[1] = (fClientOrigin[1] + (50 * Sine(DegToRad(fEyeAngles[1]))));
	fFurnitureOrigin[2] = (fClientOrigin[2]);

	new aBeam[4] = {255, 255, 0, 255}; 
	TE_SetupBeamFollow(iEnt, g_iLaser, g_iHalo, 1.0, 8.0, 8.0, 1000, aBeam);
	TE_SendToAll();

	CPrintToChat(Client,"%s Throw Edict: %f %f %f", CMDTAG, fPush[0], fPush[1], fPush[2]);            
	TeleportEntity(iEnt, fFurnitureOrigin, NULL_VECTOR, fPush);
	IgniteEntity(iEnt, 5.0);
	SetEntityMoveType(iEnt, MOVETYPE_VPHYSICS); 
	return Plugin_Handled;
}

public Action:Command_createEdict(Client, iArgs)
{
	if(Client == 0)
	{
		PrintToConsole(Client, "%s You can't use this command in the Server Console", CONSOLETAG);
		return Plugin_Handled;
	}

	if(iArgs != 1)
	{
	    PrintToConsole(Client, "%s Invalid Syntax: sm_create <model>", CONSOLETAG);
	    CPrintToChat(Client, "%s Invalid Syntax: sm_create <model>", CMDTAG);
	    return Plugin_Handled;
	}

	decl String:sModelName[64];
	GetCmdArg(1, sModelName, sizeof(sModelName));
	PrecacheModel(sModelName, true);

	new iEnt = CreateEntityByName("prop_physics_override");

	DispatchKeyValue(iEnt, "physdamagescale", "0.0");
	DispatchKeyValue(iEnt, "model", sModelName);
	DispatchSpawn(iEnt);

	decl Float:fFurnitureOrigin[3];  
	decl Float:fClientOrigin[3];
	decl Float:fEyeAngles[3];

	GetClientEyeAngles(Client, fEyeAngles);
	GetClientAbsOrigin(Client, fClientOrigin);

	fFurnitureOrigin[0] = (fClientOrigin[0] + (50 * Cosine(DegToRad(fEyeAngles[1]))));
	fFurnitureOrigin[1] = (fClientOrigin[1] + (50 * Sine(DegToRad(fEyeAngles[1]))));
	fFurnitureOrigin[2] = (fClientOrigin[2] + 100);
	             
	TeleportEntity(iEnt, fFurnitureOrigin, NULL_VECTOR, NULL_VECTOR);
	SetEntityMoveType(iEnt, MOVETYPE_VPHYSICS);   
	return Plugin_Handled;
}

public Action:Command_removeEdict(Client, iArgs)
{
    new iEnt = GetClientAimTarget(Client, false); 

    if(IsValidEntity(iEnt))
    {
        RemoveEdict(iEnt);
    }
    return Plugin_Handled;
}

public Action:Command_createJob(Client, iArgs)
{
	if(iArgs != 3)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Syntax: sm_createjob <name> <id> <level: 0 = Public | 1 = Admin | 2 = Cop", CONSOLETAG);
			return Plugin_Handled;
		}
		CPrintToChat(Client, "%s View Console for Information", CMDTAG);
		PrintToConsole(Client, "%s Invalid Syntax: sm_createjob <name> <id> <level: 0 = Public | 1 = Admin | 2 = Cop", CONSOLETAG);
		return Plugin_Handled;
	}

	decl String:sArgs[3][32];

	GetCmdArg(1, sArgs[0], sizeof(sArgs[]));
	GetCmdArg(2, sArgs[1], sizeof(sArgs[]));
	GetCmdArg(3, sArgs[2], sizeof(sArgs[]));

	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sJobPath);

	if(KvJumpToKey(hKV, sArgs[0], false))
	{
		CPrintToChat(Client, "%s Job Name %s is already in use", CMDTAG, sArgs[0]);
		PrintToConsole(Client, "%s Job Name %s is already in use", CMDTAG, sArgs[0]);
		return Plugin_Handled;
	}

	SaveString(hKV, sArgs[2], sArgs[1], sArgs[0]);
	KeyValuesToFile(hKV, g_sJobPath);
	CloseHandle(hKV);

	CPrintToChat(Client, "%s Created Job: %s, ID: %i, Type: %i", CMDTAG, sArgs[0], StringToInt(sArgs[1]), StringToInt(sArgs[2]));
	PrintToConsole(Client, "%s Created Job: %s, ID: %i, Type: %i", CONSOLETAG, sArgs[0], StringToInt(sArgs[1]), StringToInt(sArgs[2]));
	return Plugin_Handled;
}

public Action:Command_deleteJob(Client, iArgs)
{
	if(iArgs != 2)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Syntax: sm_deletejob <id> <level: 0 = Public | 1 = Admin | 2 = Cop", CONSOLETAG);
			return Plugin_Handled;
		}
		CPrintToChat(Client, "%s View Console for Information", CMDTAG);
		PrintToConsole(Client, "%s Invalid Syntax: sm_createjob <name> <level: 0 = Public | 1 = Admin | 2 = Cop", CONSOLETAG);
		return Plugin_Handled;
	}

	decl String:sArgs[2][32];

	GetCmdArg(1, sArgs[0], sizeof(sArgs[]));
	GetCmdArg(2, sArgs[1], sizeof(sArgs[]));

	new iJob = StringToInt(sArgs[0]);
	new iFlag = StringToInt(sArgs[1]);
	if(iFlag > 2 || iFlag < 0)
	{
		CPrintToChat(Client, "%s Invalid Level: %i", iFlag);
		PrintToConsole(Client, "%s Invalid Level: %i", iFlag);
		return Plugin_Handled;
	}

	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sJobPath);

	KvJumpToKey(hKV, sArgs[1], false);
	if(!KvDeleteKey(hKV, sArgs[0]))
	{
		CPrintToChat(Client, "%s Job ID: %i @ Type: %i does not exist", CMDTAG, iJob, iFlag);
		PrintToConsole(Client, "%s Job ID: %i @ Type: %i does not exist", CMDTAG, iJob, iFlag);
		return Plugin_Handled;
	}

	KvDeleteKey(hKV, sArgs[0]);
	KvRewind(hKV);
	KeyValuesToFile(hKV, g_sJobPath);
	CloseHandle(hKV);

	CPrintToChat(Client, "%s Job ID: %i @ Level: %i Deleted", iJob, iFlag);
	PrintToConsole(Client, "%s Job ID: %i @ Level: %i Deleted", iJob, iFlag);
	return Plugin_Handled;
}

public Action:Command_listJobs(Client, iArgs)
{
	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sJobPath);

	PrintToConsole(Client, "[BlueRP] Current Jobs:");
	PrintJob(Client, hKV, "-0: (Public)", "0", MAXJOBS);
	PrintJob(Client, hKV, "-1: (Admin)", "1", MAXJOBS);
	PrintJob(Client, hKV, "-2: (Police)", "2", MAXJOBS);

	KeyValuesToFile(hKV, g_sJobPath);
	CloseHandle(hKV);

	CPrintToChat(Client, "%s View console for output", CMDTAG);
	return Plugin_Handled;
}

public Action:Command_setExit(Client, iArgs)
{
	if(iArgs != 2)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s You can't use this command from the Server Console", CONSOLETAG);
			return Plugin_Handled;
		}

		CPrintToChat(Client, "%s Invalid Syntax: sm_setexit <no arguments>", CMDTAG);
		PrintToConsole(Client, "%s Invalid Syntax: sm_setexit <no arguments>", CONSOLETAG);
		return Plugin_Handled;
	}

	decl Float:fOrigin[3];
	GetClientAbsOrigin(Client, fOrigin);

	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sConfigPath);

	KvJumpToKey(hKV, "Jails", false);
	KvSetVector(hKV, "Exit-Zone", fOrigin);
	KvRewind(hKV);

	KeyValuesToFile(hKV, g_sConfigPath);
	CloseHandle(hKV);

	CPrintToChat(Client, "%s Exit Zone has been set to <%f, %f, %f>", CMDTAG, fOrigin[0], fOrigin[1], fOrigin[2]);
	PrintToConsole(Client, "%s Exit Zone has been set to <%f, %f, %f>", CONSOLETAG, fOrigin[0], fOrigin[1], fOrigin[2]);

	reloadMapConfig();

	return Plugin_Handled;
}

public Action:Command_setJail(Client, iArgs)
{
	if(iArgs != 1)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s You can't use this command from the Server Console", CONSOLETAG);
			return Plugin_Handled;
		}

		CPrintToChat(Client, "%s Invalid Syntax: sm_setjail <id 1-10>", CMDTAG);
		PrintToConsole(Client, "%s Invalid Syntax: sm_setjail <id 1-10>", CONSOLETAG);
		return Plugin_Handled;
	}

	decl String:sArg[32], Float:fOrigin[3];

	GetCmdArg(1, sArg, sizeof(sArg)); 
	GetClientAbsOrigin(Client, fOrigin);
	new iArg = StringToInt(sArg);

	if(iArg < 1 || iArg > 10)
	{
		CPrintToChat(Client, "%s Invalid Syntax: sm_setjail <id 1-10>", CMDTAG);
		PrintToConsole(Client, "%s Invalid Syntax: sm_setjail <id 1-10>", CONSOLETAG);
		return Plugin_Handled;
	}

	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sConfigPath);

	KvJumpToKey(hKV, "Jails", false);
	KvSetVector(hKV, sArg, fOrigin);
	KvRewind(hKV);

	KeyValuesToFile(hKV, g_sConfigPath);
	CloseHandle(hKV);

	CPrintToChat(Client, "%s Jail #%i has been set to <%f, %f, %f>", CMDTAG, iArg, fOrigin[0], fOrigin[1], fOrigin[2]);
	PrintToConsole(Client, "%s Jail #%i has been set to <%f, %f, %f>", CONSOLETAG, iArg, fOrigin[0], fOrigin[1], fOrigin[2]);

	reloadMapConfig();

	return Plugin_Handled;
}

public Action:Command_doorInfo(Client, iArgs)
{
	new iEnt = GetClientAimTarget(Client, false);
	if(iEnt <= 1)
	{
		CPrintToChat(Client, "%s No Door selected", CMDTAG);
		return Plugin_Handled;
	}

	decl String:sClassName[32];
	GetEdictClassname(iEnt, sClassName, sizeof(sClassName));
	if(!(StrEqual(sClassName, "func_door") || StrEqual(sClassName, "func_door_rotating") || StrEqual(sClassName, "prop_door_rotating")))
	{
		CPrintToChat(Client, "%s No Door selected", CMDTAG);
		return Plugin_Handled;
	}
	
	CPrintToChat(Client, "%s Online Owner of door #%i:", CMDTAG, iEnt);      
	
	for(new X = 1; X < MaxClients; X++)
	{
		if(IsClientConnected(X) && IsClientInGame(X))
		{
			if(g_iAccessDoor[X][iEnt] == 1) 
			{
				CPrintToChat(Client, "%s %N", CMDTAG, X);
			}
		}
	}

	return Plugin_Handled;
}

public Action:Command_buyDoor(Client, iArgs)
{
	if(Client == 0) return Plugin_Handled;

	if(iArgs != 0)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_buydoor <NO ARGS>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_buydoor <NO ARGS>", CMDTAG);
		return Plugin_Handled;	
	}

	new iEnt = GetClientAimTarget(Client, false);
	if(!IsValidEdict(iEnt))
	{
		CPrintToChat(Client, "%s Invalid Door", CMDTAG);
		PrintToConsole(Client, "%s Invalid Door", CONSOLETAG);
		return Plugin_Handled;
	}

	decl String:sDoor[32];
	IntToString(iEnt, sDoor, sizeof(sDoor));
	
	new Handle:hDoor = CreateKeyValues("Vault");
	FileToKeyValues(hDoor, g_sDoorPath);
	KvJumpToKey(hDoor, sDoor, false);
	new iBuyable = KvGetNum(hDoor, "Buyable", 0);
	
	if(iBuyable == 0 || iBuyable == 99)
	{
		CPrintToChat(Client, "%s This door is not buyable or sellable, try again later", CMDTAG);		
		KvRewind(hDoor);
		CloseHandle(hDoor);
		return Plugin_Handled;
	}
	
	decl String:sAmount[32], String:sSteamID[32];
	KvGetString(hDoor, "Price", sAmount, sizeof(sAmount), NOBODY);
	GetClientAuthString(Client, sSteamID, sizeof(sSteamID));
	
	new iPrice = StringToInt(sAmount);
	
	if(g_iBank[Client] >= iPrice && iPrice != 0)
	{	
		g_iBank[Client] -= iPrice;
		KvSetNum(hDoor, "Buyable", NOTBUYABLE);
		KvSetString(hDoor, "Owner", sSteamID);
		for(new X = 1; X <= 50; X++)
		{
			decl String:sKey[10];
			IntToString(X, sKey, 10);
			KvDeleteKey(hDoor, sKey);
		}
		KvRewind(hDoor);
		KeyValuesToFile(hDoor, g_sDoorPath);
		CloseHandle(hDoor);

		CPrintToChat(Client, "%s You successfully brought it for: $%i", CMDTAG, iPrice);
		CPrintToChat(Client, "%s To change the message, use sm_doorname to change it up", CMDTAG);
		
		decl String:sOwnerMess[255];
		Format(sOwnerMess, sizeof(sOwnerMess), "%N's House", Client);
		new Handle:hMess = CreateKeyValues("Vault");
		FileToKeyValues(hMess, g_sNoticePath);
		KvJumpToKey(hMess, "Owner", true);
		KvSetString(hMess, sDoor, sOwnerMess);
		KvRewind(hMess);
		KeyValuesToFile(hMess, g_sNoticePath);
		CloseHandle(hMess);
		ApplyMessage(iEnt, sOwnerMess);
		
		new Handle:hSetup = CreateKeyValues("Vault");
		FileToKeyValues(hSetup, g_sDoorPath);
		KvJumpToKey(hSetup, sDoor, true);
		KvSetNum(hSetup, g_sSteamID[Client], 1);
		KvRewind(hSetup);
		KeyValuesToFile(hSetup, g_sDoorPath);
		CloseHandle(hSetup);

		refreshDoors();
		return Plugin_Handled;
	}

	CPrintToChat(Client, "%s You cannot afford to buy this location", CMDTAG);
	return Plugin_Handled;
}

public Action:Command_sellDoor(Client, iArgs)
{
	decl String:sWorth[32];
	
	if(Client == 0) return Plugin_Handled;

	if(iArgs != 0)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_buydoor <NO ARGS>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_buydoor <NO ARGS>", CMDTAG);
		return Plugin_Handled;	
	}

	new iEnt = GetClientAimTarget(Client, false);
	if(!IsValidEdict(iEnt))
	{
		CPrintToChat(Client, "%s Invalid Door", CMDTAG);
		PrintToConsole(Client, "%s Invalid Door", CONSOLETAG);
		return Plugin_Handled;
	}

	decl String:sDoor[32];
	IntToString(iEnt, sDoor, 32);
	
	new Handle:hSell = CreateKeyValues("Vault");
	FileToKeyValues(hSell, g_sDoorPath);
	KvJumpToKey(hSell, sDoor, false);
	new iCheck = KvGetNum(hSell, g_sSteamID[Client], 0);
	if(iCheck == 0)
	{
		CPrintToChat(Client, "%s You must own the door to sell it", CMDTAG);
		KvRewind(hSell);
		CloseHandle(hSell);
		return Plugin_Handled;
	}
	
	new Handle:hDoor = CreateKeyValues("Vault");
	FileToKeyValues(hDoor, g_sDoorPath);
	KvJumpToKey(hDoor, sDoor, false);
	new iCheck2 = KvGetNum(hDoor, "Buyable", 99);
	if(iCheck2 == 99)
	{
		CPrintToChat(Client, "%s This door is not buyable or sellable", CMDTAG);
		KvRewind(hDoor);
		KeyValuesToFile(hDoor, g_sDoorPath);
		CloseHandle(hDoor);
		KvRewind(hSell);
		CloseHandle(hSell);
		return Plugin_Handled;
	}

	decl String:sOwner[32];
	KvGetString(hDoor, "Owner", sOwner, sizeof(sOwner), NOBODY);
	if(!StrEqual(g_sSteamID[Client], sOwner, false))
	{
		CPrintToChat(Client, "%s Access Denied", CMDTAG);
		KvRewind(hDoor);
		CloseHandle(hDoor);
		KvRewind(hSell);
		CloseHandle(hSell);
		return Plugin_Handled;
	}

	decl String:sSteamID[32];
	for(new X = 1; X <= 50; X++)
	{
		decl String:sKey[10];
		IntToString(X, sKey, sizeof(sKey));
		KvGetString(hDoor, sKey, sSteamID, sizeof(sSteamID), NOBODY);
		if(!StrEqual(sSteamID, "Nobody", false))
		{
			KvDeleteKey(hSell, sSteamID);
		}
	}
	
	KvDeleteKey(hSell, g_sSteamID[Client]);
	KvRewind(hSell);
	KeyValuesToFile(hSell, g_sDoorPath);
	CloseHandle(hSell);
	
	KvSetNum(hDoor, "Buyable", BUYABLE);
	KvDeleteKey(hDoor, "Owner");
	KvGetString(hDoor, "Price", sWorth, sizeof(sWorth), NOBODY);
	KvRewind(hDoor);
	KeyValuesToFile(hDoor, g_sDoorPath);
	CloseHandle(hDoor);
	
	decl String:sSale[255];
	Format(sSale, sizeof(sSale), "For Sale: $%s", sWorth);
	new Handle:hMess = CreateKeyValues("Vault");
	FileToKeyValues(hMess, g_sNoticePath);
	KvJumpToKey(hMess, "Owner", true);
	KvSetString(hMess, sDoor, sSale);
	KvRewind(hMess);
	KeyValuesToFile(hMess, g_sNoticePath);
	CloseHandle(hMess);
	ApplyMessage(iEnt, sSale);
	
	new Float:fCashBack = float(StringToInt(sWorth));
	fCashBack = fCashBack - fCashBack / 100 * 15;
	
	g_iBank[Client] += RoundFloat(fCashBack);
	g_iItem[Client][65] += g_iLocks[iEnt];
	SaveItem(Client, 65, g_iItem[Client][65]); 
	g_iLocks[iEnt] = 0;
	
	CPrintToChat(Client, "%s You successfully sold your to the market", CMDTAG);
	CPrintToChat(Client, "%s You've received $%i (15% deduction off original price of house)", CMDTAG, RoundFloat(fCashBack));
	refreshDoors();
	
	return Plugin_Handled;
}

public Action:Command_items(Client, iArgs)
{
	g_bGivingItems[Client] = false;
	ShowItems(Client);
	return Plugin_Handled;
}

public Action:Command_switchJob(Client, iArgs)
{
	if(!StrEqual(g_sPermJob[Client], "Unemployed", false))
	{
		if(StrEqual(g_sTempJob[Client], "Unemployed", false) || StrEqual(g_sTempJob[Client], "Off Duty Cop", false))
		{
			strcopy(g_sTempJob[Client], sizeof(g_sTempJob[]), g_sPermJob[Client]);
			ForcePlayerSuicide(Client);
			CPrintToChat(Client, "%s Switching back to %s", CMDTAG, g_sPermJob[Client]);
			return Plugin_Handled;
		}
		else if(!StrEqual(g_sTempJob[Client], "Unemployed", false) || !StrEqual(g_sTempJob[Client], "Off Duty Cop", false))
		{
			if(StrContains(g_sPermJob[Client], "Police", false) != -1)
			{
				Format(g_sTempJob[Client], sizeof(g_sTempJob[]), "Off Duty Cop");
				CPrintToChat(Client, "%s Switching to Off Duty Cop", CMDTAG);
				ForcePlayerSuicide(Client);
				return Plugin_Handled;
			}
			else
			{
				Format(g_sTempJob[Client], sizeof(g_sTempJob[]), "Unemployed");
				CPrintToChat(Client, "%s Switching to Civillian", CMDTAG);
				ForcePlayerSuicide(Client);
				return Plugin_Handled;
			}
		}
	}
	return Plugin_Handled;
}

public Action:Command_employPlayer(Client, iArgs)
{
	if(iArgs != 3)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Syntax: sm_employ <player> <id> <level>", CONSOLETAG);
			CPrintToChat(Client, "%s Invalid Syntax: sm_employ <player> <id> <level>", CMDTAG);
			return Plugin_Handled;
		}

		PrintToConsole(Client, "%s Invalid Syntax: sm_employ <player> <id> <level>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_employ <player> <id> <level>", CMDTAG);
		return Plugin_Handled;
	}

	decl String:sArgs[3][32];

	GetCmdArg(1, sArgs[0], sizeof(sArgs[]));
	GetCmdArg(2, sArgs[1], sizeof(sArgs[]));
	GetCmdArg(3, sArgs[2], sizeof(sArgs[]));

	new iLevel = StringToInt(sArgs[2]);
	new iJob = StringToInt(sArgs[1]);
	decl String:sJob[32];
	new String:sTargetName[MAX_TARGET_LENGTH];
	new iTargetList[MAXPLAYERS], iTargetCount;
	new bool:bMultiLang;
 
	if ((iTargetCount = ProcessTargetString(
			sArgs[0],
			Client,
			iTargetList,
			MAXPLAYERS,
			COMMAND_FILTER_CONNECTED,
			sTargetName,
			sizeof(sTargetName),
			bMultiLang)) <= 0)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArgs[0]);
			return Plugin_Handled;
		}
		PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArgs[0]);
		CPrintToChat(Client, "%s Invalid Target: %s", CMDTAG, sArgs[0]);
		return Plugin_Handled;
	}

	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sJobPath);
 
 	LoadString(hKV, sArgs[2], sArgs[1], "NaN", sJob);

	for(new i = 0; i < iTargetCount; i++)
	{
		if(!StrEqual(sJob, "NaN", false))
		{
			strcopy(g_sTempJob[iTargetList[i]], sizeof(g_sTempJob[]), sJob);
			strcopy(g_sPermJob[iTargetList[i]], sizeof(g_sPermJob[]), sJob);
			CPrintToChat(Client, "%s %N Employed you to %s", CMDTAG, Client, sJob);
			ForcePlayerSuicide(iTargetList[i]);
			Save(iTargetList[i]);
		}
		else
		{
			CPrintToChat(Client, "%s Invalid Job ID: %i @ Level: %i", CMDTAG, iJob, iLevel);
			PrintToConsole(Client, "%s Invalid Job ID: %i @ Level: %i", CONSOLETAG, iJob, iLevel);
		}
		
	}
 
	if(bMultiLang)
	{
		CPrintToChat(Client, "%s Employed %t to %s", CMDTAG, sTargetName, sJob);
		PrintToConsole(Client, "%s Employed %t to %s", CONSOLETAG, sTargetName, sJob);
	}
	else
	{
		CPrintToChat(Client, "%s Employed %s to %s", CMDTAG, sTargetName, sJob);
		PrintToConsole(Client, "%s Employed %s to %s", CONSOLETAG, sTargetName, sJob);
	}

	return Plugin_Handled;
}

public Action:Command_giveItem(Client, iArgs)
{
	if(Client == 0)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_giveitem <name> <id> <amount>", CONSOLETAG);
		return Plugin_Handled;
	}
	if(iArgs < 3)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_giveitem <name> <id> <amount>", CONSOLETAG);
		return Plugin_Handled;
	}
	
	new String:sTargetName[MAX_TARGET_LENGTH], String:sArgs[3][32];
	new iTargetList[MAXPLAYERS], iTargetCount;
	new bool:bMultiLang;

	GetCmdArg(1, sArgs[0], sizeof(sArgs[]));
	GetCmdArg(2, sArgs[1], sizeof(sArgs[]));
	GetCmdArg(3, sArgs[2], sizeof(sArgs[]));
 
	if ((iTargetCount = ProcessTargetString(
			sArgs[0],
			Client,
			iTargetList,
			MAXPLAYERS,
			COMMAND_FILTER_CONNECTED,
			sTargetName,
			sizeof(sTargetName),
			bMultiLang)) <= 0)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArgs[0]);
			return Plugin_Handled;
		}
		PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArgs[0]);
		CPrintToChat(Client, "%s Invalid Target: %s", CMDTAG, sArgs[0]);
		return Plugin_Handled;
	}
 
	for(new i = 0; i < iTargetCount; i++)
	{
		g_iItem[iTargetList[i]][StringToInt(sArgs[1])] += StringToInt(sArgs[2]);
		CPrintToChat(iTargetList[i], "%s %N Gave you %i of %s", CMDTAG, Client, StringToInt(sArgs[2]), g_sItemName[StringToInt(sArgs[1])]);
		SaveItem(iTargetList[i], StringToInt(sArgs[2]), g_iItem[iTargetList[i]][StringToInt(sArgs[1])]);
	}
 
	if(bMultiLang)
	{
		CPrintToChat(Client, "%s Gave %t %i of %s", CMDTAG, sTargetName, StringToInt(sArgs[2]), g_sItemName[StringToInt(sArgs[1])]);
	}
	else
	{
		CPrintToChat(Client, "%s Gave %s %i of %s", CMDTAG, sTargetName, StringToInt(sArgs[2]), g_sItemName[StringToInt(sArgs[1])]);
	}

	return Plugin_Handled;
}

public Action:Command_takeItem(Client, iArgs)
{
	
	if(iArgs != 3)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Syntax: sm_takeitem <name> <id> <amount>", CONSOLETAG);
			return Plugin_Handled;
		}
		
		CPrintToChat(Client, "%s Invalid Syntax: sm_takeitem <name> <id> <amount>", CMDTAG);
		PrintToConsole(Client, "%s Invalid Syntax: sm_takeitem <name> <id> <amount>", CONSOLETAG);
		return Plugin_Handled;
	}
	
	new String:sTargetName[MAX_TARGET_LENGTH], String:sArgs[3][32];
	new iTargetList[MAXPLAYERS], iTargetCount;
	new bool:bMultiLang;

	GetCmdArg(1, sArgs[0], sizeof(sArgs[]));
	GetCmdArg(2, sArgs[1], sizeof(sArgs[]));
	GetCmdArg(3, sArgs[2], sizeof(sArgs[]));

	new iItem = StringToInt(sArgs[1]);
 	new iAmount = StringToInt(sArgs[2]);

	if ((iTargetCount = ProcessTargetString(
			sArgs[0],
			Client,
			iTargetList,
			MAXPLAYERS,
			COMMAND_FILTER_CONNECTED,
			sTargetName,
			sizeof(sTargetName),
			bMultiLang)) <= 0)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArgs[0]);
			return Plugin_Handled;
		}
		PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArgs[0]);
		CPrintToChat(Client, "%s Invalid Target: %s", CMDTAG, sArgs[0]);
		return Plugin_Handled;
	}
 
	for(new i = 0; i < iTargetCount; i++)
	{
		g_iItem[iTargetList[i]][iItem] -= iAmount;
		CPrintToChat(iTargetList[i], "%s %N Took %i of %s from you", CMDTAG, Client, iAmount, g_sItemName[iItem]);
		SaveItem(iTargetList[i], iItem, g_iItem[iTargetList[i]][iItem]);
	}
 
	if(bMultiLang)
	{
		CPrintToChat(Client, "%s Took %i of %s from %t", CMDTAG, iAmount, g_sItemName[iItem], sTargetName);
	}
	else
	{
		CPrintToChat(Client, "%s Took %i of %s from %s", CMDTAG, iAmount, g_sItemName[iItem], sTargetName);
	}

	return Plugin_Handled;
}

public Action:Command_createItem(Client, iArgs)
{
	if(iArgs != 5)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_createitem <id> <name> <type> <variable> <cost>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_createitem <id> <name> <type> <variable> <cost>", CMDTAG);
		return Plugin_Handled;
	}

	decl String:sArgs[4][128];
	decl String:sBuffer[255], String:sItem[255];
	
	GetCmdArg(1, sItem, sizeof(sItem));
	GetCmdArg(2, sArgs[0], 128);
	GetCmdArg(3, sArgs[1], 128);
	GetCmdArg(4, sArgs[2], 128);
	GetCmdArg(5, sArgs[3], 128);
	
	ImplodeStrings(sArgs, 4, "^", sBuffer, 255);
	
	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sItemPath);
	
	SaveString(hVault, "Items", sItem, sBuffer);
	KeyValuesToFile(hVault, g_sItemPath);
	CloseHandle(hVault);
	
	PrintToConsole(Client, "%s Created Item #%s - %s, Type: %s @ %s", CONSOLETAG, sItem, sArgs[0], sArgs[1], sArgs[2]);
	CPrintToChat(Client, "%s Created Item #%s - %s, Type: %s @ %s", CMDTAG, sItem, sArgs[0], sArgs[1], sArgs[2]);
	reloadItems();
	return Plugin_Handled;
}

public Action:Command_deleteItem(Client, iArgs)
{
	if(iArgs != 1)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_deleteitem <id>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_deleteitem <id>", CMDTAG);
		return Plugin_Handled;
	}

	decl String:sItem[255];
	GetCmdArg(1, sItem, sizeof(sItem));

	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sItemPath);
	
	KvJumpToKey(hVault, "Items", false);
	new bool:bDelete = KvDeleteKey(hVault, sItem); 
	KvRewind(hVault);

	KeyValuesToFile(hVault, g_sItemPath);

	if(Client > 0)
	{
		(!bDelete) ? PrintToConsole(Client, "%s Failed to remove Item %s from the database", CONSOLETAG, sItem) : PrintToConsole(Client, "%s Removed Item %s from the database", CONSOLETAG, sItem);
		(!bDelete) ? CPrintToChat(Client, "%s Failed to remove Item %s from the database", CMDTAG, sItem) : CPrintToChat(Client, "%s Removed Item %s from the database", CMDTAG, sItem);
	}
	else (!bDelete) ? PrintToConsole(Client, "%s Failed to remove Item %s from the database", CONSOLETAG, sItem) : PrintToConsole(Client, "%s Removed Item %s from the database", CONSOLETAG, sItem);


	CloseHandle(hVault);
	reloadItems();
	return Plugin_Handled;
}

public Action:Command_itemList(Client, iArgs)
{
	if(iArgs < 1)
	{	
		PrintToConsole(Client, "%s Invalid Syntax: sm_itemlist <page> <sort by>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_itemlist <page> <sort by>", CMDTAG);
		return Plugin_Handled;
	}
	
	decl String:sPage[10], String:sCategory[25];
	decl String:sRef[255], String:sItem[255];

	GetCmdArg(1, sPage, sizeof(sPage));
	GetCmdArg(2, sCategory, sizeof(sCategory));
	new iPageInt = StringToInt(sPage);

	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sItemPath);
	
	PrintToConsole(Client, "Items:");
	PrintToConsole(Client, "Printing Itemlist Page %i", iPageInt);
	
	new iPageInt2 = iPageInt * 100;

	if(strlen(sCategory) == 0)
	{
		for(new X = iPageInt2 - 100; X < iPageInt2; X++)
		{
			IntToString(X, sItem, sizeof(sItem));
			LoadString(hVault, "Items", sItem, "NaN", sRef);

			if(!StrEqual(sRef, "NaN"))
			{
				ReplaceString(sRef, 255, "^", " "); 
				PrintToConsole(Client, "%i - %s", X, sRef);
			}		
		}

		CloseHandle(hVault);
		return Plugin_Handled;
	}

	for(new X = iPageInt2 - 100; X < iPageInt2; X++)
	{
		IntToString(X, sItem, sizeof(sItem));
		LoadString(hVault, "Items", sItem, "NaN", sRef);

		if(!StrEqual(sRef, "NaN"))
		{
			decl String:sFunc[4][128];
			ExplodeString(sRef, "^", sFunc, 4, 32);
			
			if(StrContains(sFunc[1], sCategory, false) != -1)
			{
				ReplaceString(sRef, 255, "^", " "); 
				PrintToConsole(Client, "%i - %s", X, sRef);
			}		
		}
	}

	CloseHandle(hVault);
	return Plugin_Handled;
}

public Action:Command_addVendorItems(Client, iArgs)
{
	if(iArgs < 2)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_addvendoritem <vendor id> <item id>", CONSOLETAG);

		return Plugin_Handled;
	}
	decl String:sBuffer[255], String:sArgs[2][255];
	
	GetCmdArg(1, sArgs[0], sizeof(sArgs[])); //Vendor ID
	GetCmdArg(2, sArgs[1], sizeof(sArgs[])); //Item ID

	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sNPCPath);
	LoadString(hVault, "Vendor_Items", sArgs[0], "NaN", sBuffer);
	
	if(StrContains(sBuffer, "NaN", false) == -1)
	{
		decl String:sAddString[2][255], String:sOut[255];
		
		sAddString[0] = sBuffer;
		sAddString[1] = sArgs[1];
		ImplodeStrings(sAddString, 2, " ", sOut, 255);
		SaveString(hVault, "Vendor_Items", sArgs[0], sOut);
	}
	else
	{
		SaveString(hVault, "Vendor_Items", sArgs[0], sArgs[1]);
	}
	
	
	KeyValuesToFile(hVault, g_sNPCPath);
	PrintToConsole(Client, "%s Added Item %s to Vendor #%i", CONSOLETAG, g_sItemName[StringToInt(sArgs[1])], StringToInt(sArgs[0]));
	CloseHandle(hVault);
	return Plugin_Handled;
}

public Action:Command_createNPC(Client, iArgs)
{
	if(Client == 0) 
	{
		CPrintToChat(Client, "%s You can't use this command from the server console", CONSOLETAG);
		return Plugin_Handled;
	}
	
	if(iArgs < 3)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_createnpc <id> <NPC> <type> <opt model>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_createnpc <id> <NPC> <type> <opt model>", CMDTAG);
		return Plugin_Handled;
	}
	
	decl String:sBuffers[7][32];
	decl Float:fOrigin[3], Float:fAngles[3];
	decl String:sSave[255], String:sNPC[255];
	
	GetCmdArg(1, sNPC, 32);
	GetCmdArg(2, sBuffers[0], 32);
	GetCmdArg(3, sBuffers[6], 32);
	GetCmdArg(4, sBuffers[5], 32);
	GetClientAbsOrigin(Client, fOrigin);
	GetClientAbsAngles(Client, fAngles);
	IntToString(RoundFloat(fOrigin[0]), sBuffers[1], 32);
	IntToString(RoundFloat(fOrigin[1]), sBuffers[2], 32);
	IntToString(RoundFloat(fOrigin[2]), sBuffers[3], 32);
	IntToString(RoundFloat(fAngles[1]), sBuffers[4], 32);
	ImplodeStrings(sBuffers, 6, " ", sSave, 255);
	
	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sNPCPath);
	SaveString(hVault, sBuffers[6], sNPC, sSave);

	KeyValuesToFile(hVault, g_sNPCPath);
	PrintToConsole(Client, "%s Added NPC %s, npc_%s <%s, %s, %s> ZAxis: %s", CONSOLETAG, sNPC, sBuffers[0], sBuffers[1], sBuffers[2], sBuffers[3], sBuffers[4]);
	CPrintToChat(Client, "%s Added NPC %s, npc_%s <%s, %s, %s> ZAxis: %s", CMDTAG, sNPC, sBuffers[0], sBuffers[1], sBuffers[2], sBuffers[3], sBuffers[4]);
	CloseHandle(hVault);
	
	decl String:sClassName[32], String:sAngles[32], String:sPrecache[255];
	
	Format(sAngles, 32, "0 %f 0", sAngles[1]);
	Format(sClassName, 32, "npc_%s", sBuffers[0]);
	if(strlen(sBuffers[5][0]) > 0)
	{
		Format(sPrecache, 256, "models/%s.mdl", sBuffers[5]);
	}
	else
	{
		Format(sPrecache, 256, "models/%s.mdl", sBuffers[0]);
	}
	
	PrecacheModel(sPrecache, true);
	new iNPC = CreateEntityByName(sClassName);
	DispatchKeyValue(iNPC, "angles", sAngles);
	DispatchSpawn(iNPC);	
	SetEntProp(iNPC, Prop_Data, "m_takedamage", 0, 1);
	
	if(sBuffers[5][0]) SetEntityModel(iNPC, sPrecache);
	new iX = StringToInt(sNPC);
	new iY = StringToInt(sBuffers[6]); 
	g_iNPCList[iX] = iNPC;
	g_iNPCListInverse[iNPC] = iX;
	g_iNPCLiveUpdate[iY][iX] = iNPC;	
	TeleportEntity(iNPC, fOrigin, NULL_VECTOR, NULL_VECTOR);
	return Plugin_Handled;
}

public Action:Command_npcWho(Client, iArgs)
{
	if(Client == 0) 
	{
		CPrintToChat(Client, "%s You can't use this command from the server console", CONSOLETAG);
		return Plugin_Handled;
	}
	
	new iEnt = GetClientAimTarget(Client, false);
	if(iEnt > 0)
	{
		CPrintToChat(Client, "%s NPCID: #%i", CMDTAG, g_iNPCListInverse[iEnt]);
		PrintToConsole(Client, "%s NPCID: #%i", CONSOLETAG, g_iNPCListInverse[iEnt]);
	}
	return Plugin_Handled;
}

public Action:Command_removeNPC(Client, iArgs)
{
	if(iArgs < 2)
	{
		PrintToConsole(Client, "%s Invalid Syntax: sm_removenpc <type> <id>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_removenpc <type> <id>", CMDTAG);
		return Plugin_Handled;
	}
	
	decl String:sNPC[255], String:sType[255];
	GetCmdArg(1, sType, sizeof(sType));
	GetCmdArg(2, sNPC, sizeof(sNPC));

	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sNPCPath);
	KvJumpToKey(hVault, sType, false);
	KvDeleteKey(hVault, sNPC); 
	KvRewind(hVault);
	KvJumpToKey(hVault, "Notice", false);
	KvDeleteKey(hVault, sNPC);
	KvRewind(hVault);
	KeyValuesToFile(hVault, g_sNPCPath);
	
	new iD = StringToInt(sNPC);
	new iE = StringToInt(sType);
	
	if(g_iNPCLiveUpdate[iE][iD] > 0)
	{
		AcceptEntityInput(g_iNPCLiveUpdate[iE][iD], "Kill");
		g_iNPCLiveUpdate[iE][iD] = 0;
		g_iNPCList[iD] = 0;
	}
	
	PrintToConsole(Client, "%s Removed NPC %s (Type: %s) from the database", CONSOLETAG, sNPC, sType);
	CPrintToChat(Client, "%s Removed NPC %s (Type: %s) from the database", CMDTAG, sNPC, sType);
	CloseHandle(hVault);
	return Plugin_Handled;
}

public Action:Command_npcList(Client, iArgs)
{
	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sNPCPath);
	PrintToConsole(Client, "NPC List:");
	
	PrintNPC(Client, hVault, "-0: (Employer)", "0", MAXNPCS);
	PrintNPC(Client, hVault, "-1: (Bankers)", "1", MAXNPCS);
	PrintNPC(Client, hVault, "-2: (Vendors)", "2", MAXNPCS);
	
	KeyValuesToFile(hVault, g_sNPCPath);
	CloseHandle(hVault);
	return Plugin_Handled;
}

public Action:Command_setFelony(Client, iArgs)
{
	if(iArgs != 2)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Syntax: sm_setfelony <player> <level>", CONSOLETAG);
			return Plugin_Handled;
		}
		
		PrintToConsole(Client, "%s Invalid Syntax: sm_setfelony <player> <level>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_setfelony <player> <level>", CMDTAG);
		return Plugin_Handled;
	}

	decl String:sArg1[32], String:sArg2[32];

	GetCmdArg(1, sArg1, sizeof(sArg1));
	GetCmdArg(2, sArg2, sizeof(sArg2));

	new String:sTargetName[MAX_TARGET_LENGTH];
	new iTargetList[MAXPLAYERS], iTargetCount;
	new bool:bMultiLang;
 
	if ((iTargetCount = ProcessTargetString(
			sArg1,
			Client,
			iTargetList,
			MAXPLAYERS,
			COMMAND_FILTER_CONNECTED,
			sTargetName,
			sizeof(sTargetName),
			bMultiLang)) <= 0)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArg1);
			return Plugin_Handled;
		}
		PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArg1);
		CPrintToChat(Client, "%s Invalid Target: %s", CMDTAG, sArg1);
		return Plugin_Handled;
	}
 
	for(new i = 0; i < iTargetCount; i++)
	{
		g_iFelonyLevel[iTargetList[i]] = StringToInt(sArg2);
		CPrintToChat(iTargetList[i], "%s %N set your Felony Level to %i", CMDTAG, Client, StringToInt(sArg2));
		Save(iTargetList[i]);
	}
 
	if(bMultiLang)
	{
		CPrintToChat(Client, "%s Set %t's Felony Level to %i", CMDTAG, sTargetName, StringToInt(sArg2));
	}
	else
	{
		CPrintToChat(Client, "%s Set %s's Felony Level to %i", CMDTAG, sTargetName, StringToInt(sArg2));
	}

	return Plugin_Handled;
}

public Action:Command_addMoney(Client, iArgs)
{
	if(iArgs != 2)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Syntax: sm_addmoney <player> <amount>", CONSOLETAG);
			return Plugin_Handled;
		}
		
		PrintToConsole(Client, "%s Invalid Syntax: sm_setmoney <player> <amount>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_addmoney <player> <amount>", CMDTAG);
		return Plugin_Handled;
	}

	decl String:sArg1[32], String:sArg2[32];

	GetCmdArg(1, sArg1, sizeof(sArg1));
	GetCmdArg(2, sArg2, sizeof(sArg2));

	new String:sTargetName[MAX_TARGET_LENGTH];
	new iTargetList[MAXPLAYERS], iTargetCount;
	new bool:bMultiLang;
 
	if ((iTargetCount = ProcessTargetString(
			sArg1,
			Client,
			iTargetList,
			MAXPLAYERS,
			COMMAND_FILTER_CONNECTED,
			sTargetName,
			sizeof(sTargetName),
			bMultiLang)) <= 0)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArg1);
			return Plugin_Handled;
		}
		PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArg1);
		CPrintToChat(Client, "%s Invalid Target: %s", CMDTAG, sArg1);
		return Plugin_Handled;
	}
 
	for(new i = 0; i < iTargetCount; i++)
	{
		g_iCash[iTargetList[i]] += StringToInt(sArg2);
		CPrintToChat(iTargetList[i], "%s %N Added %i to your Wallet", CMDTAG, Client, StringToInt(sArg2));
		Save(iTargetList[i]);
	}
 
	if(bMultiLang)
	{
		CPrintToChat(Client, "%s Added %i to %t's Wallet", CMDTAG, StringToInt(sArg2), sTargetName);
	}
	else
	{
		CPrintToChat(Client, "%s Added %i to %s's Wallet", CMDTAG, StringToInt(sArg2), sTargetName);
	}
 
	return Plugin_Handled;
}

public Action:Command_setMoney(Client, iArgs)
{
	if(iArgs != 2)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Syntax: sm_setmoney <player> <amount>", CONSOLETAG);
			return Plugin_Handled;
		}
		
		PrintToConsole(Client, "%s Invalid Syntax: sm_setmoney <player> <amount>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_setmoney <player> <amount>", CMDTAG);
		return Plugin_Handled;
	}

	decl String:sArg1[32], String:sArg2[32];

	GetCmdArg(1, sArg1, sizeof(sArg1));
	GetCmdArg(2, sArg2, sizeof(sArg2));

	new String:sTargetName[MAX_TARGET_LENGTH];
	new iTargetList[MAXPLAYERS], iTargetCount;
	new bool:bMultiLang;
 
	if ((iTargetCount = ProcessTargetString(
			sArg1,
			Client,
			iTargetList,
			MAXPLAYERS,
			COMMAND_FILTER_CONNECTED,
			sTargetName,
			sizeof(sTargetName),
			bMultiLang)) <= 0)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArg1);
			return Plugin_Handled;
		}
		PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArg1);
		CPrintToChat(Client, "%s Invalid Target: %s", CMDTAG, sArg1);
		return Plugin_Handled;
	}
 
	for(new i = 0; i < iTargetCount; i++)
	{
		g_iCash[iTargetList[i]] += StringToInt(sArg2);
		CPrintToChat(iTargetList[i], "%s %N Set your Wallet to %i", CMDTAG, Client, StringToInt(sArg2));
		Save(iTargetList[i]);
	}
 
	if(bMultiLang)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Set %s's Wallet to %i", CONSOLETAG, sTargetName, StringToInt(sArg2));
			return Plugin_Handled;
		}
		else CPrintToChat(Client, "%s Set %s's Wallet to %i", CMDTAG, sTargetName, StringToInt(sArg2));
	}
	else
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Set %s's Wallet to %i", CONSOLETAG, sTargetName, StringToInt(sArg2));
			return Plugin_Handled;
		}
		else CPrintToChat(Client, "%s Set %s's Wallet to %i", CMDTAG, sTargetName, StringToInt(sArg2));
	}
 
	return Plugin_Handled;
}

public Action:Command_cuffPlayer(Client, iArgs)
{
	if(iArgs != 1)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Syntax: sm_cuff <player>", CONSOLETAG);
			return Plugin_Handled;
		}
		
		PrintToConsole(Client, "%s Invalid Syntax: sm_cuff <player>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_cuff <player>", CMDTAG);
		return Plugin_Handled;
	}	
	
	decl String:sArg1[16];
	GetCmdArg(1, sArg1, sizeof(sArg1));
	
	new String:sTargetName[MAX_TARGET_LENGTH];
	new iTargetList[MAXPLAYERS], iTargetCount;
	new bool:bMultiLang;
 
	if ((iTargetCount = ProcessTargetString(
			sArg1,
			Client,
			iTargetList,
			MAXPLAYERS,
			COMMAND_FILTER_CONNECTED,
			sTargetName,
			sizeof(sTargetName),
			bMultiLang)) <= 0)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArg1);
			return Plugin_Handled;
		}
		PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArg1);
		CPrintToChat(Client, "%s Invalid Target: %s", CMDTAG, sArg1);
		return Plugin_Handled;
	}
 
	for(new i = 0; i < iTargetCount; i++)
	{
		SetEntityRenderMode(iTargetList[i], RENDER_GLOW);
		SetEntityRenderColor(iTargetList[i], 255, 0, 0, 180);
		g_iJailTime[iTargetList[i]] = g_iFelonyLevel[iTargetList[i]] / 60 * 2;
		g_iJailProgress[iTargetList[i]] = g_iJailTime[iTargetList[i]];
		CreateTimer(1.0, ProgressJail, iTargetList[i], TIMER_REPEAT);
		g_iFelonyLevel[iTargetList[i]] = 0;
		g_iBounty[iTargetList[i]] = 0;
		g_bCuffed[iTargetList[i]] = true;
		g_iCuffedBy[iTargetList[i]] = Client;
		CreateTimer(0.2, removeWeapons, iTargetList[i]);
		CPrintToChat(iTargetList[i], "%s You have been cuffed by %N via command", CMDTAG, Client);
		Save(iTargetList[i]);
	}
 
	if(bMultiLang)
	{
		PrintToConsole(Client, "%s You cuffed %t via command", CONSOLETAG, sTargetName);
		CPrintToChat(Client, "%s You cuffed %t via command", CMDTAG, sTargetName);
	}
	else
	{
		PrintToConsole(Client, "%s You cuffed %s via command", CONSOLETAG, sTargetName);
		CPrintToChat(Client, "%s You cuffed %s via command", CMDTAG, sTargetName);
	}
	
	return Plugin_Handled;
}

public Action:Command_uncuffPlayer(Client, iArgs)
{
	if(iArgs != 1)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Syntax: sm_uncuff <player>", CONSOLETAG);
			return Plugin_Handled;
		}
		
		PrintToConsole(Client, "%s Invalid Syntax: sm_uncuff <player>", CONSOLETAG);
		CPrintToChat(Client, "%s Invalid Syntax: sm_uncuff <player>", CMDTAG);
		return Plugin_Handled;
	}	
	
	decl String:sArg1[16];
	GetCmdArg(1, sArg1, sizeof(sArg1));
	
	new String:sTargetName[MAX_TARGET_LENGTH];
	new iTargetList[MAXPLAYERS], iTargetCount;
	new bool:bMultiLang;
 
	if ((iTargetCount = ProcessTargetString(
			sArg1,
			Client,
			iTargetList,
			MAXPLAYERS,
			COMMAND_FILTER_CONNECTED,
			sTargetName,
			sizeof(sTargetName),
			bMultiLang)) <= 0)
	{
		if(Client == 0)
		{
			PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArg1);
			return Plugin_Handled;
		}
		PrintToConsole(Client, "%s Invalid Target: %s", CONSOLETAG, sArg1);
		CPrintToChat(Client, "%s Invalid Target: %s", CMDTAG, sArg1);
		return Plugin_Handled;
	}
 
	for(new i = 0; i < iTargetCount; i++)
	{
		SetEntityRenderMode(iTargetList[i], RENDER_NORMAL);
		SetEntityRenderColor(iTargetList[i], 255, 255, 255, 255);
		g_iCuffedBy[iTargetList[i]] = -1;
		g_bCuffed[iTargetList[i]] = false;
		CPrintToChat(iTargetList[i], "%s You have been uncuffed by %N via command", CMDTAG, Client);
		CreateTimer(0.2, SetupSpawn, iTargetList[i]);
		Save(iTargetList[i]);
	}
 
	if(bMultiLang)
	{
		PrintToConsole(Client, "%s You uncuffed %t via command", CONSOLETAG, sTargetName);
		CPrintToChat(Client, "%s You uncuffed %t via command", CMDTAG, sTargetName);
	}
	else
	{
		PrintToConsole(Client, "%s You uncuffed %s via command", CONSOLETAG, sTargetName);
		CPrintToChat(Client, "%s You uncuffed %s via command", CMDTAG, sTargetName);
	}
	
	return Plugin_Handled;
}

public Action:Listener_MenuSelect(Client, const String:sCommand[], argc)
{
	if(g_fMenuTime[Client] >= GetGameTime() - 0.1)
	{
		return Plugin_Handled;
	}
	else g_fMenuTime[Client] = GetGameTime();
	return Plugin_Continue;
}

public Action:ProgressJail(Handle:hTimer, any:Client)
{
	g_iJailProgress[Client] -= 1;
	if(g_iJailTime[Client] <= 1 || g_iJailProgress[Client] <= 0)
	{
		KillTimer(hTimer);
		Release(Client);
		return Plugin_Handled;
	}
	if(!IsPlayerAlive(Client) || !IsClientInGame(Client) && !IsClientConnected(Client))
	{
		KillTimer(hTimer);
	}
	return Plugin_Changed;
}

Release(Client)
{
	g_iCuffedBy[Client] = -1;
	g_bCuffed[Client] = false;
	SetEntityRenderMode(Client, RENDER_NORMAL);
	SetEntityRenderColor(Client, 255, 255, 255, 255);	
	CPrintToChat(Client, "%s You have served your sentence, and have been released", CMDTAG);
	CreateTimer(0.2, SetupSpawn, Client);
	(g_bHasExit) ? TeleportEntity(Client, g_fExitLoc, NULL_VECTOR, NULL_VECTOR) : CPrintToChat(Client, "%s Server has not created an exit zone", CMDTAG);
	Save(Client);
}

Cuff(Client, Cop)
{
	if(g_iBounty[Client] > 0)
	{
		g_iCash[Cop] += RoundToCeil(g_iBounty[Client] * 1.6);
	}
	SetEntityRenderMode(Client, RENDER_GLOW);
	SetEntityRenderColor(Client, 255, 0, 0, 180);
	
	CPrintToChat(Client, "%s You are cuffed with %i Felony by %N", CMDTAG, g_iFelonyLevel[Client], Cop);
	CPrintToChat(Cop, "%s You cuff %N with %i Felony", CMDTAG, Client, g_iFelonyLevel[Client]);
	
	g_iJailTime[Client] = g_iFelonyLevel[Client] / 60 * 2;
	g_iJailProgress[Client] = g_iJailTime[Client];
	CreateTimer(1.0, ProgressJail, Client, TIMER_REPEAT);
	g_iFelonyLevel[Client] = 0;
	g_iBounty[Client] = 0;
	g_bCuffed[Client] = true;
	g_iCuffedBy[Client] = Cop;
	CreateTimer(0.2, removeWeapons, Client);
	Save(Client);
}

Uncuff(Client, Cop)
{
	SetEntityRenderMode(Client, RENDER_NORMAL);
	SetEntityRenderColor(Client, 255, 255, 255, 255);
	new iCop = g_iCuffedBy[Client];
	if(Cop > 0 && Cop < MaxClients && IsClientConnected(Cop) && IsClientInGame(Cop))
	{
		CPrintToChat(Client, "%s You are uncuffed by %N", CMDTAG, Cop);
	}

	//Check if another cop is uncuffing cuffs: Avoid abuse situations
	if(g_iCuffedBy[Client] != Cop || g_iCuffedBy[Client] == Client)
	{
		CPrintToChat(iCop, "%s %N was uncuffed by %N", CMDTAG, Client, Cop);
	}
	else
	{
		CPrintToChat(Cop, "%s, You uncuffed %N", CMDTAG, Client);
	}
	g_iCuffedBy[Client] = -1;
	g_bCuffed[Client] = false;
	CreateTimer(0.2, SetupSpawn, Client);
	Save(Client);
}

reloadItems()
{

	decl String:sBuffer[4][32];
	decl String:sReference[255], String:sItem[255];

	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sItemPath);

	for(new X = 0; X < MAXITEMS; X++)
	{
		IntToString(X, sItem, sizeof(sItem));
		
		LoadString(hVault, "Items", sItem, "Null", sReference);
		
		if(!StrEqual(sReference, "Null"))
		{	
			ExplodeString(sReference, "^", sBuffer, 4, 255);
			
			decl String:sAction[2][32];
			ExplodeString(sBuffer[1], "-", sAction, 2, 32);
			new iAction = StringToInt(sAction[0]);
			new iCost = StringToInt(sBuffer[3]);
			
			g_sItemName[X] = sBuffer[0];
			g_iItemAction[X] = iAction;
			g_sItemVar[X] = sBuffer[2];
			g_iItemCost[X] = iCost;
			
			switch(g_iItemAction[X])
			{
				case 7: //Furniture
				{
					PrecacheModel(g_sItemVar[X]);
				}

				case 16: //Player Models
				{
					PrecacheModel(g_sItemVar[X]);
				}
			}
		}
	}
	
	KeyValuesToFile(hVault, g_sItemPath);
	CloseHandle(hVault);
}

dropMoney(Client, iAmount)
{
	decl Float:fLoc[3];
	new Float:fAngles[3] = {0.0, 0.0, 0.0};
	GetClientAbsOrigin(Client, fLoc);
	fLoc[2] += 30.0;
	
	while(iAmount > 0)
	{
		new iEnt = CreateEntityByName("prop_physics_override");

		if(iAmount > 5000)
		{
			g_iValue[iEnt] = 1000;
			DispatchKeyValue(iEnt, "model", "models/money/goldbar.mdl");
			iAmount -= 1000; 
		}	
		//Values:
		else if(iAmount > 2500)
		{
			g_iValue[iEnt] = 500;
			DispatchKeyValue(iEnt, "model", "models/money/note2.mdl");
			iAmount -= 500; 
		}
		else if(iAmount > 1000)
		{
			g_iValue[iEnt] = 200;
			DispatchKeyValue(iEnt, "model", "models/money/note2.mdl");
			iAmount -= 200; 
		}
		else if(iAmount > 500)
		{
			g_iValue[iEnt] = 100;
			DispatchKeyValue(iEnt, "model", "models/money/note.mdl");
			iAmount -= 100;
		}
		else if(iAmount > 250)
		{
			g_iValue[iEnt] = 200;
			DispatchKeyValue(iEnt, "model", "models/money/note2.mdl");
		}
		else if(iAmount > 200)
		{
			g_iValue[iEnt] = 200;
			DispatchKeyValue(iEnt, "model", "models/money/note2.mdl");
		}
		else if(iAmount > 100)
		{
			g_iValue[iEnt] = 200;
			DispatchKeyValue(iEnt, "model", "models/money/note2.mdl");
		}	
		else if(iAmount > 50)
		{
			g_iValue[iEnt] = 50;
			DispatchKeyValue(iEnt, "model", "models/money/note3.mdl");
			iAmount -= 50; 
		}
		else if(iAmount > 20)
		{
			g_iValue[iEnt] = 20;
			DispatchKeyValue(iEnt, "model", "models/money/silvcoin.mdl");
			iAmount -= 20; 
		}
		else if(iAmount > 10)
		{
			g_iValue[iEnt] = 10;
			DispatchKeyValue(iEnt, "model", "models/money/silvcoin.mdl");
			iAmount -= 10;  
		}
		else if(iAmount > 5)
		{
			g_iValue[iEnt] = 5;
			DispatchKeyValue(iEnt, "model", "models/money/silvcoin.mdl");
			iAmount -= 5;  
		}
		else
		{
			g_iValue[iEnt] = 1;
			DispatchKeyValue(iEnt, "model", "models/money/broncoin.mdl");
			iAmount -= 1; 
		}

		DispatchSpawn(iEnt);
		fAngles[1] = GetRandomFloat(0.0, 360.0);
		fLoc[0] += GetRandomFloat(-50.0, 50.0);
		fLoc[1] += GetRandomFloat(-50.0, 50.0);
		new iCollision = GetEntSendPropOffs(iEnt, "m_CollisionGroup");
		if(IsValidEntity(iEnt)) SetEntData(iEnt, iCollision, 1, 1, true);
		TeleportEntity(iEnt, fLoc, fAngles, NULL_VECTOR);
	}

	return 1;
}

reloadMapConfig()
{
	new Handle:hKV = CreateKeyValues("Vault");
	FileToKeyValues(hKV, g_sConfigPath);
	decl String:sJail[32];

	for(new X = 1; X <= MAXJAILS; X++)
	{
		IntToString(X, sJail, sizeof(sJail));
		KvJumpToKey(hKV, "Jails", false);
		KvGetVector(hKV, sJail, g_fJailLoc[X], NULLZONE);

		if(g_fJailLoc[X][0] != -1.0)
		{
			g_bHasJail = true;
		}
		g_iValidJails++;
	}

	KvGetVector(hKV, "Exit-Zone", g_fExitLoc, NULLZONE);
	if(g_fExitLoc[0] != -1.0)
	{
		g_bHasExit = true;
	}

	KvRewind(hKV);
	KeyValuesToFile(hKV, g_sConfigPath);
	CloseHandle(hKV);
}

ApplyMessage(iDoor, String:Message[255])
{
	g_sNotice[iDoor] = Message;
}

DoorLoad(Client)
{
	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sDoorPath);

	if(Client != 0)
	{
		decl String:sKey[32];
		for(new X = 0; X <= MAXDOORS; X++)
		{
			IntToString(X, sKey, sizeof(sKey));
			g_iAccessDoor[Client][X] = LoadInteger(hVault, sKey, g_sSteamID[Client], 0);

		}
	}

	if(Client == 0)
	{
		decl String:sDoor[32];
		for(new X = 0; X <= MAXDOORS; X++)
		{
			IntToString(X, sDoor, sizeof(sDoor));
			g_bDoorLocked[X] = IntToBool(LoadInteger(hVault, "Locked", sDoor, 0));

			if(g_bDoorLocked[X] == true)
			{
				decl String:sClassName[255];
				GetEdictClassname(X, sClassName, 255);
				if(StrEqual(sClassName, "func_door_rotating") || StrEqual(sClassName, "prop_door_rotating")) AcceptEntityInput(X, "Lock", 0);
				if(StrEqual(sClassName, "func_door")) AcceptEntityInput(X, "Unlock", 0);
			}
		}
	}

	CloseHandle(hVault);
}

Action:RobNPC(Client, iCash, iType, iNPC)
{
	if(bIsCombine(Client))
	{
		CPrintToChat(Client, "%s You cannot rob as a cop", CMDTAG);
		return Plugin_Handled;
	}
	
	//Check if Cops are on
	new bool:bCombineInGame = false;
	for(new X = 1; X <= MaxClients; X++) if(StrContains(g_sTempJob[X], "Police", false) != -1 || StrContains(g_sTempJob[X], "Admin", false) != -1) bCombineInGame = true;
	
	//No Combine Online
	if(GetConVarInt(g_hCV_RobMode) == 1 && !bCombineInGame)
	{
		CPrintToChat(Client, "%s Try again later, There are no police online to stop you!", CMDTAG);
		return Plugin_Handled;
	}

	if(g_fRobCooldown[iType][iNPC] >= GetGameTime() - 600)
	{
		new iTimeLeft = RoundToCeil(g_fRobCooldown[iType][iNPC] - GetGameTime() + 600);
		CPrintToChat(Client, "%s This NPC has been robbed too recently! (%is left)", CMDTAG, iTimeLeft);
		return Plugin_Handled;
	}

	if(g_bCuffed[Client]) return Plugin_Handled;
	g_fRobCooldown[iType][iNPC] = GetGameTime();

	decl String:sType[16];
	if(iType == 1)
	{
		sType = "Banker";
	}
	else
	{
		sType = "Vendor";
	}

	for(new Y = 1; Y <= MaxClients; Y++)
	{
		if(IsClientConnected(Y) && IsClientInGame(Y))
		{
			SetHudTextParams(-1.0, 0.015, 10.0, 255, 255, 255, 255, 0, 6.0, 0.1, 0.2);
			ShowHudText(Y, -1, "ATTENTION: %N is robbing a %s!", Client, sType);
		}
	}

	g_iNPCCash[Client] = iCash;
	CreateTimer(1.0, Robbing, Client, TIMER_REPEAT);
	CPrintToChat(Client, "%s You have started the robbery, stay close to continue getting money", CMDTAG);
	g_iFelonyLevel[Client] += 700;
	return Plugin_Handled;
}

public Action:Robbing(Handle:hTimer, any:Client)
{
	
	if(g_iNPCCash[Client] <= 0)
	{
		CPrintToChat(Client, "%s You have taken all of the money, run!", CMDTAG);
		KillTimer(hTimer);		

		for(new Y = 1; Y < MaxClients; Y++)
		{
			if(IsClientConnected(Y) && IsClientInGame(Y))
			{
				SetHudTextParams(-1.0, 0.015, 10.0, 255, 255, 255, 255, 0, 6.0, 0.1, 0.2);
				ShowHudText(Y, -1, "\n\nATTENTION: %N is getting away!", Client);
			}
		}

		return Plugin_Handled;
	}
	
	if(!IsClientInGame(Client) || !IsClientConnected(Client) || !IsPlayerAlive(Client)) return Plugin_Handled;
	
	decl Float:fClientOrigin[3];
	GetClientAbsOrigin(Client, fClientOrigin);
	new Float:fDist = GetVectorDistance(g_fNPCRobOrigin[Client], fClientOrigin);
	
	if(fDist >= 150)
	{
		CPrintToChat(Client, "%s You have moved too far from the register! Robbery aborted", CMDTAG);	
		g_iNPCCash[Client] = 0;
		KillTimer(hTimer);
		return Plugin_Handled;
	}

	new iRandMoney = GetRandomInt(2, 9);
	g_iCash[Client] += iRandMoney;
	g_iNPCCash[Client] -= iRandMoney;
	return Plugin_Handled;
}


Action:sendToJail(Client)
{
	if(g_bCuffed[Client])
	{
		new iRandom = GetRandomInt(1, g_iValidJails);
		if(g_fJailLoc[iRandom][0] != NULLZONE[0])
		{
			TeleportEntity(Client, g_fJailLoc[iRandom], NULL_VECTOR, NULL_VECTOR);
		}
		else
		{
			sendToJail(Client);
		}
	}
}

Action:refreshDoors()
{
	new Handle:hVault = CreateKeyValues("Vault");
	FileToKeyValues(hVault, g_sConfigPath);
	new String:sEntity[32];
	for(new X = 0; X < 64; X++)
	{
		IntToString(X, sEntity, 32);	
		g_iPoliceDoor[X] = LoadInteger(hVault, "PoliceDoor", sEntity, 0);
	}
	KvRewind(hVault);
	CloseHandle(hVault);

	for(new i = 1; i <= MaxClients; i++)
	{
		if(IsClientConnected(i) && IsClientInGame(i))
		{
			for(new X = 0; X < MAXDOORS; X++)
			{
				g_iAccessDoor[i][X] = 0;
			}

			for(new D = 0; D < 64; D++)
			{
				if(bIsCombine(i))
				{
					if(g_iPoliceDoor[D] != 0) g_iAccessDoor[i][D] = 1;		
				}
				else
				{
					if(g_iPoliceDoor[D] != 0) g_iAccessDoor[i][D] = 0;
				}
			}
			DoorLoad(i);
		}
	}
	return Plugin_Handled;
}

bool:bIsMoneyModel(Entity)
{
	decl String:sModel[64];
	GetEntPropString(Entity, Prop_Data, "m_ModelName", sModel, sizeof(sModel));
	if(StrContains(sModel, "models/money/", false) != -1) return true;
	else return false;
}

bool:bIsCombine(Client)
{
	if(StrContains(g_sTempJob[Client], "Police", false) != -1) return true;
	else return false;
}